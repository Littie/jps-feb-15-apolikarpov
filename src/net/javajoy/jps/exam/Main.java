package net.javajoy.jps.exam;

import net.javajoy.jps.exam.controller.FileManagerMainController;
import net.javajoy.jps.exam.view.MainFrame;

/**
 * Entry point
 */

public class Main {
    public static void main(String[] args) {
        new FileManagerMainController(new MainFrame());
    }
}

