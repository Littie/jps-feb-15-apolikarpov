package net.javajoy.jps.exam.view.components;

import javax.swing.*;
import java.io.File;

public class FileManagerDiskBar extends JToolBar {
    public static final String BACK_SLASHES = ":\\";
    private static final String SPACES = "  ";
    private static final String SELECTED_DISK = SPACES + "C" + SPACES;

    private ButtonGroup buttonGroup = new ButtonGroup();

    public FileManagerDiskBar() {
        init();
    }

    private void init() {
        addDiskButtons();
        setFloatable(false);
    }

    private void addDiskButtons() {
        for (File diskName : File.listRoots()) {
            add(makeButton(formatDiskName(diskName)));
        }
    }

    private String formatDiskName(File diskName) {
        return SPACES + diskName.toString().replace(BACK_SLASHES, SPACES);
    }

    private JToggleButton makeButton(String name) {
        JToggleButton button = new JToggleButton(name);

        if (name.equals(SELECTED_DISK)) {
            button.setSelected(true);
        }

        buttonGroup.add(button);
        button.setToolTipText(name);
        return button;
    }
}
