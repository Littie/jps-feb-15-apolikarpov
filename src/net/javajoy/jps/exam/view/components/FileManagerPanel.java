package net.javajoy.jps.exam.view.components;

import net.javajoy.jps.exam.controller.FileManagerPanelController;
import net.javajoy.jps.exam.om.CompositeFileManagerModel;
import net.javajoy.jps.exam.om.FileManagerDataModel;

import javax.swing.*;
import java.awt.*;

public class FileManagerPanel extends JPanel {
    public static final String LEFT_LOCATION = "left";
    public static final String RIGHT_LOCATION = "right";

    private CompositeFileManagerModel compositeFileManagerModel;

    private JTable fileTable;
    private JToolBar diskBar;
    private JLabel pathPanel;
    private JScrollPane scrollPane;
    private String panelLocation;

    public FileManagerPanel(String panelLocation, CompositeFileManagerModel compositeFileManagerModel) {
        this.compositeFileManagerModel = compositeFileManagerModel;
        this.panelLocation = panelLocation;
        init();
    }

    private void init() {
        initFileTable();
        addController();
        addComponents();
    }

    private void initFileTable() {
        this.fileTable = new JTable();
        this.fileTable.setShowGrid(false);
        this.fileTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.fileTable.setModel(getCompositeFileManagerModel().getFileManagerTableModel());
        this.scrollPane = new JScrollPane(this.fileTable);
        this.fileTable.setFocusable(false);
    }

    private void addController() {
        new FileManagerPanelController(this);
    }

    private void addComponents() {
        setLayout(new GridBagLayout());

        add(diskBar,
                new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(pathPanel,
                new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(scrollPane,
                new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public void setDiskBar(JToolBar diskBar) {
        this.diskBar = diskBar;
    }

    public void setPathPanel(JLabel pathPanel) {
        this.pathPanel = pathPanel;
    }

    public String getPanelLocation() {
        return panelLocation;
    }

    public JTable getFileTable() {
        return fileTable;
    }

    public JLabel getPathPanel() {
        return pathPanel;
    }

    public JToolBar getDiskBar() {
        return diskBar;
    }

    public CompositeFileManagerModel getCompositeFileManagerModel() {
        return compositeFileManagerModel;
    }

    public FileManagerDataModel getFileMangerDataModel() {
        return compositeFileManagerModel.getFileManagerDataModel();
    }

    //------------------------------------------------------------------------------------------------------------------
}
