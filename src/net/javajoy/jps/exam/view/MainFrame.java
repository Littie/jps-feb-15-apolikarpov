package net.javajoy.jps.exam.view;

import net.javajoy.jps.exam.controller.action.*;
import net.javajoy.jps.exam.om.Menu;
import net.javajoy.jps.exam.om.ModelProvider;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class MainFrame extends JFrame {
    private static final String WINDOW_TITLE = "File manager";
    private static final int DIVIDER_SIZE = 2;
    private static final double RESIZE_WEIGHT = 0.5;
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 600;

    private JSplitPane splitPane;
    private JToolBar toolBar;
    private FileManagerPanel leftPanel;
    private FileManagerPanel rightPanel;

    public MainFrame() {
        init();
    }

    private void init() {
        setTitle(WINDOW_TITLE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        initComponents();
        addComponents();
        pack();
        setMinimumSize(new Dimension(WIDTH, HEIGHT));
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        initSplitPane();
        initPanels();
        initToolBar();
        initMainMenu();
    }

    private void initPanels() {
        setLeftPanel(new FileManagerPanel(FileManagerPanel.LEFT_LOCATION, ModelProvider.getLeftModel()));
        setRightPanel(new FileManagerPanel(FileManagerPanel.RIGHT_LOCATION, ModelProvider.getRightModel()));
    }

    private void initSplitPane() {
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setResizeWeight(RESIZE_WEIGHT);
        splitPane.setDividerSize(DIVIDER_SIZE);
    }

    private void initToolBar() {
        toolBar = new JToolBar();
        toolBar.setFloatable(false);

        toolBar.add(new CopyAction(this, Menu.COPY.getName(), new ImageIcon(getClass().getResource(Menu.COPY.getImagePath())),
                "Copy file", KeyEvent.VK_F5));
        toolBar.add(new CutAction(this, Menu.CUT.getName(), new ImageIcon(getClass().getResource(Menu.CUT.getImagePath())),
                "Cut file", KeyEvent.VK_F6));
        toolBar.add(new DeleteAction(this, Menu.DELETE.getName(), new ImageIcon(getClass().getResource(Menu.DELETE.getImagePath())),
                "Delete file", KeyEvent.VK_F8));
        toolBar.add(new PasteAction(this, Menu.PASTE.getName(), new ImageIcon(getClass().getResource(Menu.PASTE.getImagePath())),
                "Paste file", KeyEvent.VK_F7));
    }

    private void initMainMenu() {
        JMenuBar mainFrameMenuBar = new JMenuBar();

        JMenuItem copy = new JMenuItem(new CopyAction(this, Menu.COPY.getName(), null, "Copy fileMenu", KeyEvent.VK_F5));
        JMenuItem cut = new JMenuItem(new CutAction(this, Menu.CUT.getName(), null, "Cut fileMenu", KeyEvent.VK_F6));
        JMenuItem delete = new JMenuItem(new DeleteAction(this, Menu.DELETE.getName(), null, "Delete fileMenu", KeyEvent.VK_F8));
        JMenuItem paste = new JMenuItem(new PasteAction(this, Menu.PASTE.getName(), null, "Paste fileMenu", KeyEvent.VK_F7));
        JMenuItem exit = new JMenuItem(new ExitAction(Menu.EXIT.getName(), null, "Exit program", KeyEvent.VK_Q));

        JMenu fileMenu = new JMenu("File");
        fileMenu.add(copy);
        fileMenu.add(cut);
        fileMenu.add(delete);
        fileMenu.add(paste);
        fileMenu.addSeparator();
        fileMenu.add(exit);

        mainFrameMenuBar.add(fileMenu);
        setJMenuBar(mainFrameMenuBar);
    }

    private void addComponents() {
        setLayout(new GridBagLayout());

        add(toolBar,
                new GridBagConstraints(0, 0, 2, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(splitPane,
                new GridBagConstraints(0, 1, 2, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    private void setLeftPanel(FileManagerPanel leftPanel) {
        this.leftPanel = leftPanel;
        splitPane.setLeftComponent(this.leftPanel);
        this.leftPanel.grabFocus();
        revalidate();
    }

    private void setRightPanel(FileManagerPanel rightPanel) {
        this.rightPanel = rightPanel;
        splitPane.setRightComponent(this.rightPanel);
        revalidate();
    }

    public FileManagerPanel getLeftPanel() {
        return leftPanel;
    }

    public FileManagerPanel getRightPanel() {
        return rightPanel;
    }

    public JTable getLeftFilePanel() {
        return this.leftPanel.getFileTable();
    }

    public JTable getRightFilePanel() {
        return this.rightPanel.getFileTable();
    }

    //------------------------------------------------------------------------------------------------------------------
}
