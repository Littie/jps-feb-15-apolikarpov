package net.javajoy.jps.exam.view.renderer;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Date;

public abstract class AbstractRenderer extends JLabel implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        setOpaque(false);
        String valueAsString;

        if ((valueAsString = toString(value)) != null) {
            setText(valueAsString);
        }

        if (isSelected) {
            setBorder(getSelectedBorder());
        } else {
            setBorder(null);
        }

        return this;
    }

    private String toString(Object value) {
        if (value instanceof Long) {
            return Long.toString((Long) value);
        } else if (value instanceof String) {
            return (String) value;
        } else if (value instanceof Date) {
            return value.toString();
        }

        return null;
    }

    protected abstract MatteBorder getSelectedBorder();
}
