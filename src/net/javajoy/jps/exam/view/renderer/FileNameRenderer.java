package net.javajoy.jps.exam.view.renderer;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

public class FileNameRenderer extends AbstractRenderer {

    @Override
    protected MatteBorder getSelectedBorder() {
        return BorderFactory.createMatteBorder(1, 1, 1, 0, Color.RED);
    }
}
