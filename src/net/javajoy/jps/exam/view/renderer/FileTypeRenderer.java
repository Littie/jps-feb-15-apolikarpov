package net.javajoy.jps.exam.view.renderer;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

public class FileTypeRenderer extends AbstractRenderer {
    @Override
    protected MatteBorder getSelectedBorder() {
        return BorderFactory.createMatteBorder(1, 0, 1, 0, Color.RED);
    }
}
