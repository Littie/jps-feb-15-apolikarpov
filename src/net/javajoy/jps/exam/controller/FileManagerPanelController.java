package net.javajoy.jps.exam.controller;

import net.javajoy.jps.exam.controller.action.DiskBarAction;
import net.javajoy.jps.exam.om.Title;
import net.javajoy.jps.exam.view.components.FileManagerDiskBar;
import net.javajoy.jps.exam.view.components.FileManagerPanel;
import net.javajoy.jps.exam.view.renderer.FileDateRenderer;
import net.javajoy.jps.exam.view.renderer.FileNameRenderer;
import net.javajoy.jps.exam.view.renderer.FileSizeRenderer;
import net.javajoy.jps.exam.view.renderer.FileTypeRenderer;

import javax.swing.*;
import javax.swing.table.TableColumnModel;

public class FileManagerPanelController {
    private FileManagerPanel fileManagerPanel;

    public FileManagerPanelController(FileManagerPanel fileManagerPanel) {
        this.fileManagerPanel = fileManagerPanel;
        init();
    }

    private void init() {
        setCurrentPath();
        addActions();
        addRenderers();
    }

    private void setCurrentPath() {
        fileManagerPanel.setDiskBar(new FileManagerDiskBar());
        fileManagerPanel.setPathPanel(new JLabel(fileManagerPanel.getCompositeFileManagerModel().getSelectedDisk().toString()));
    }

    private void addActions() {
        addDiskBarAction();
    }

    private void addRenderers() {
        TableColumnModel columnModel = fileManagerPanel.getFileTable().getColumnModel();
        columnModel.getColumn(Title.NAME.ordinal()).setCellRenderer(new FileNameRenderer());
        columnModel.getColumn(Title.TYPE.ordinal()).setCellRenderer(new FileTypeRenderer());
        columnModel.getColumn(Title.SIZE.ordinal()).setCellRenderer(new FileSizeRenderer());
        columnModel.getColumn(Title.DATE.ordinal()).setCellRenderer(new FileDateRenderer());
    }

    private void addDiskBarAction() {
        JToolBar toolBar = fileManagerPanel.getDiskBar();

        for (int i = 0; i < toolBar.getComponentCount(); i++) {
            ((JToggleButton) toolBar.getComponentAtIndex(i)).addActionListener(new DiskBarAction(fileManagerPanel));
        }
    }
}
