package net.javajoy.jps.exam.controller.action;

import net.javajoy.jps.exam.om.CompositeFileManagerModel;
import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;

public class CutAction extends FileManagerAbstractAction {
    public CutAction(MainFrame mainFrame, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(mainFrame, text, icon, dsc, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (leftPanel.getFileTable().isFocusable()) {
            filePanelCut(leftPanel);
        } else {
            filePanelCut(rightPanel);
        }
    }

    private void filePanelCut(FileManagerPanel panel) {
        CompositeFileManagerModel fileManagerModel = panel.getCompositeFileManagerModel();
        int rowNum = fileManagerModel.getSelectedRow();
        String path = fileManagerModel.getFullPathToFile(rowNum);

        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(new StringSelection(path), null);
    }
}
