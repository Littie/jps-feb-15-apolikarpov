package net.javajoy.jps.exam.controller.action;

import net.javajoy.jps.exam.view.components.FileManagerDiskBar;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class DiskBarAction implements ActionListener {
    private FileManagerPanel fileManagerPanel;

    public DiskBarAction(FileManagerPanel fileManagerPanel) {
        this.fileManagerPanel = fileManagerPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand().trim().concat(FileManagerDiskBar.BACK_SLASHES);

        File path = new File(command);
        fileManagerPanel.getCompositeFileManagerModel().showFileList(path);

        if (path.list() != null) {
            fileManagerPanel.getCompositeFileManagerModel().setSelectedDisk(path);
            fileManagerPanel.getPathPanel().setText(path.toString());
        }
    }
}
