package net.javajoy.jps.exam.controller.action.listener;

import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class FilePanelMouseListener extends MouseAdapter {
    private static final int DOUBLE_CLICK = 2;

    private FileManagerPanel fileManagerPanel;
    private JTable leftFilePanel;
    private JTable rightFilePanel;
    private JTable filePanel;

    public FilePanelMouseListener(MainFrame mainFrame, FileManagerPanel fileManagerPanel) {
        this.fileManagerPanel = fileManagerPanel;
        this.leftFilePanel = mainFrame.getLeftPanel().getFileTable();
        this.rightFilePanel = mainFrame.getRightPanel().getFileTable();
        this.filePanel = fileManagerPanel.getFileTable();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (fileManagerPanel.getPanelLocation().equals(FileManagerPanel.LEFT_LOCATION)) {
            if (!filePanel.isFocusable()) {
                setPanelFocusable(rightFilePanel, leftFilePanel);
            }

            setSelectedRow();
        } else {
            if (!filePanel.isFocusable()) {
                setPanelFocusable(leftFilePanel, rightFilePanel);
            }

            setSelectedRow();
        }

        if (e.getClickCount() == DOUBLE_CLICK && e.getButton() == MouseEvent.BUTTON1) {
            int num = filePanel.getSelectedRow();
            File path = new File(fileManagerPanel.getFileMangerDataModel().getFullPathToFile(num));

            if (path.isDirectory()) {
                fileManagerPanel.getCompositeFileManagerModel().showFileList(path);
                filePanel.setRowSelectionInterval(0, 0);
                fileManagerPanel.getPathPanel().setText(path.toString());
            }
        }
    }

    private void setPanelFocusable(JTable removeFocusPanel, JTable setFocusPanel) {
        removeFocusPanel.clearSelection();
        removeFocusPanel.setFocusable(false);
        setFocusPanel.setFocusable(true);
        setFocusPanel.grabFocus();
    }

    private void setSelectedRow() {
        fileManagerPanel.getCompositeFileManagerModel().setSelectedRow(filePanel.getSelectedRow());
    }
}
