package net.javajoy.jps.exam.controller.action.listener;

import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class FilePanelTabListener extends KeyAdapter {
    private FileManagerPanel fileManagerPanel;
    private JTable leftFilePanel;
    private JTable rightFilePanel;

    public FilePanelTabListener(MainFrame mainFrame, FileManagerPanel fileManagerPanel) {
        this.fileManagerPanel = fileManagerPanel;
        this.leftFilePanel = mainFrame.getLeftPanel().getFileTable();
        this.rightFilePanel = mainFrame.getRightPanel().getFileTable();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_TAB) {
            if (fileManagerPanel.getPanelLocation().equals(FileManagerPanel.LEFT_LOCATION)) {
                setPanelFocusable(leftFilePanel, rightFilePanel);
            } else {
                setPanelFocusable(rightFilePanel, leftFilePanel);
            }
        }
    }

    private void setPanelFocusable(JTable removeFocusPanel, JTable setFocusPanel) {
        setSelectedRow();
        removeFocusPanel.clearSelection();
        removeFocusPanel.setFocusable(false);
        setFocusPanel.setFocusable(true);
        setFocusPanel.grabFocus();
    }

    private void setSelectedRow() {
        fileManagerPanel.getCompositeFileManagerModel().setSelectedRow(fileManagerPanel.getFileTable().getSelectedRow());
    }
}
