package net.javajoy.jps.exam.controller.action;

import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;

abstract public class FileManagerAbstractAction extends AbstractAction {
    protected FileManagerPanel leftPanel;
    protected FileManagerPanel rightPanel;

    public FileManagerAbstractAction(MainFrame mainFrame, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
        initFields(mainFrame);
    }

    private void initFields(MainFrame mainFrame) {
        leftPanel = mainFrame.getLeftPanel();
        rightPanel = mainFrame.getRightPanel();
    }
}
