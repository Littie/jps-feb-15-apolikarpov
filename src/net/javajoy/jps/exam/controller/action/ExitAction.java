package net.javajoy.jps.exam.controller.action;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ExitAction extends AbstractAction {
    public ExitAction(String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
