package net.javajoy.jps.exam.controller.action;

import net.javajoy.jps.exam.om.CompositeFileManagerModel;
import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

public class DeleteAction extends FileManagerAbstractAction {
    public DeleteAction(MainFrame mainFrame, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(mainFrame, text, icon, dsc, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (leftPanel.getFileTable().isFocusable()) {
            filePanelDelete(leftPanel);
        } else {
            filePanelDelete(rightPanel);
        }
    }

    private void filePanelDelete(FileManagerPanel panel) {
        CompositeFileManagerModel fileManagerModel = panel.getCompositeFileManagerModel();
        int rowNum = fileManagerModel.getSelectedRow();
        File path = new File(fileManagerModel.getFullPathToFile(rowNum));

        delete(path);
        fileManagerModel.updatePanelData(path.getParentFile());
    }

    private void delete(File inputFile) {
        int confirmation = JOptionPane.showConfirmDialog(null, "Are you sure?");

        if (confirmation == JOptionPane.YES_OPTION) {
            deleteDir(inputFile);
        }
    }

    private void deleteDir(File inputFile) {
        if (!inputFile.delete()) {
            String[] fileList = inputFile.list();
            if (fileList == null) {
                JOptionPane.showMessageDialog(null, "No selected file");
            } else {
                for (String dirFile : fileList) {
                    File f = new File(inputFile, dirFile);
                    deleteDir(f);
                }
                inputFile.delete();
            }
        }
    }
}
