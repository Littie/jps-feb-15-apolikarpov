package net.javajoy.jps.exam.controller.action;

import net.javajoy.jps.exam.om.CompositeFileManagerModel;
import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;


public class CopyAction extends FileManagerAbstractAction {
    public CopyAction(MainFrame mainFrame, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(mainFrame, text, icon, dsc, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (leftPanel.getFileTable().isFocusable()) {
            filePanelCopy(leftPanel, rightPanel);
        } else {
            filePanelCopy(rightPanel, leftPanel);
        }
    }

    private void filePanelCopy(FileManagerPanel sourcePanel, FileManagerPanel targetPanel) {
        CompositeFileManagerModel sourcePanelModel = sourcePanel.getCompositeFileManagerModel();
        CompositeFileManagerModel targetPanelModel = targetPanel.getCompositeFileManagerModel();

        int sourceSelectedRow = sourcePanelModel.getSelectedRow();
        String sourceName = new File(sourcePanelModel.getFullPathToFile(sourceSelectedRow)).getName();

        File sourceFile = new File(sourcePanelModel.getFullPathToFile(sourceSelectedRow));
        File targetFile = new File(targetPanel.getPathPanel().getText(), sourceName);

        copyDirs(sourceFile, targetFile);
        targetPanelModel.updatePanelData(targetFile.getParentFile());
    }

    private void copyDirs(File sourceFile, File targetFile) {
        if (sourceFile.isDirectory()) {
            if (!targetFile.exists()) {
                targetFile.mkdir();
            }

            for (String newFile : sourceFile.list()) {
                copyDirs(new File(sourceFile, newFile), new File(targetFile, newFile));
            }
        } else {
            try {
                if (targetFile.isDirectory()) {
                    targetFile = new File(targetFile, sourceFile.getName());
                }

                Files.copy(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
