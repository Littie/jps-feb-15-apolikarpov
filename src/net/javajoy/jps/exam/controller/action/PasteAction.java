package net.javajoy.jps.exam.controller.action;

import net.javajoy.jps.exam.om.CompositeFileManagerModel;
import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class PasteAction extends FileManagerAbstractAction {
    public PasteAction(MainFrame mainFrame, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(mainFrame, text, icon, dsc, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (leftPanel.getFileTable().isFocusable()) {
            filePanelPaste(leftPanel, rightPanel);
        } else {
            filePanelPaste(rightPanel, leftPanel);
        }
    }

    private void filePanelPaste(FileManagerPanel targetPanel, FileManagerPanel sourcePanel) {
        CompositeFileManagerModel sourcePanelModel = sourcePanel.getCompositeFileManagerModel();
        CompositeFileManagerModel targetPanelModel = targetPanel.getCompositeFileManagerModel();

        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        String sourcePath;

        try {
            sourcePath = (String) clipboard.getData(DataFlavor.stringFlavor);
            File sourceFile = new File(sourcePath);
            File targetFile = new File(targetPanel.getPathPanel().getText(), sourceFile.getName());

            moveDirs(sourceFile, targetFile);
            updatePanels(sourcePanelModel, targetPanelModel, sourceFile, targetFile);
        } catch (UnsupportedFlavorException | IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Paste error");
        }
    }

    private void updatePanels(CompositeFileManagerModel sourcePanelModel, CompositeFileManagerModel targetPanelModel,
                              File sourceFile, File targetFile) {
        targetPanelModel.updatePanelData(targetFile.getParentFile());
        sourcePanelModel.updatePanelData(sourceFile.getParentFile());
    }

    private void moveDirs(File sourceFile, File targetFile) {
        if (sourceFile.isDirectory()) {
            if (!targetFile.exists()) {
                targetFile.mkdir();
            }

            for (String newFile : sourceFile.list()) {
                moveDirs(new File(sourceFile, newFile), new File(targetFile, newFile));
            }
        } else {
            try {
                if (targetFile.isDirectory()) {
                    targetFile = new File(targetFile, sourceFile.getName());
                }

                Files.move(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        sourceFile.delete();
    }
}
