package net.javajoy.jps.exam.controller.action;

import net.javajoy.jps.exam.om.CompositeFileManagerModel;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

public class EnterAction extends KeyAdapter {
    private FileManagerPanel fileManagerPanel;

    public EnterAction(FileManagerPanel fileManagerPanel) {
        this.fileManagerPanel = fileManagerPanel;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        CompositeFileManagerModel compositeFileManagerModel = fileManagerPanel.getCompositeFileManagerModel();

        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            int selectedRow = fileManagerPanel.getFileTable().getSelectedRow();
            File path = new File(compositeFileManagerModel.getFullPathToFile(selectedRow));

            if (path.isDirectory()) {
                compositeFileManagerModel.showFileList(path);
                fileManagerPanel.getPathPanel().setText(path.toString());
            }
        }
    }
}
