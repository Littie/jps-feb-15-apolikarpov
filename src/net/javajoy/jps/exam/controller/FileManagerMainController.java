package net.javajoy.jps.exam.controller;

import net.javajoy.jps.exam.controller.action.EnterAction;
import net.javajoy.jps.exam.controller.action.listener.FilePanelMouseListener;
import net.javajoy.jps.exam.controller.action.listener.FilePanelTabListener;
import net.javajoy.jps.exam.view.MainFrame;
import net.javajoy.jps.exam.view.components.FileManagerPanel;

import javax.swing.*;

public class FileManagerMainController {
    private MainFrame mainFrame;

    public FileManagerMainController(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        init();
    }

    private void init() {
        addListeners();
    }

    private void addListeners() {
        addFilePanelListener(mainFrame.getLeftFilePanel(), mainFrame.getLeftPanel());
        addFilePanelListener(mainFrame.getRightFilePanel(), mainFrame.getRightPanel());
    }

    private void addFilePanelListener(JTable table, FileManagerPanel panel) {
        table.addMouseListener(new FilePanelMouseListener(mainFrame, panel));
        table.addKeyListener(new FilePanelTabListener(mainFrame, panel));
        table.addKeyListener(new EnterAction(panel));
    }
}
