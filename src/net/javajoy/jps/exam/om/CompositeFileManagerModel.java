package net.javajoy.jps.exam.om;

import java.io.File;

public class CompositeFileManagerModel {
    private FileManagerDataModel fileManagerDataModel;
    private FileManagerTableModel fileManagerTableModel;

    public CompositeFileManagerModel() {
        fileManagerDataModel = new FileManagerDataModel();
        fileManagerTableModel = new FileManagerTableModel(fileManagerDataModel);
    }

    public String getFullPathToFile(int selectedRow) {
        return fileManagerDataModel.getFullPathToFile(selectedRow);
    }

    public void updatePanelData(File path) {
        fileManagerTableModel.updateData(fileManagerDataModel, path);
    }

    public FileManagerDataModel getFileManagerDataModel() {
        return fileManagerDataModel;
    }

    public FileManagerTableModel getFileManagerTableModel() {
        return fileManagerTableModel;
    }

    public void showFileList(File path) {
        fileManagerDataModel.prepareFileList(path);
        fileManagerTableModel.setData(fileManagerDataModel.getFileList());
    }

    public int getSelectedRow() {
        return fileManagerDataModel.getSelectedRow();
    }

    public void setSelectedRow(int row) {
        fileManagerDataModel.setSelectedRow(row);
    }

    public File getSelectedDisk() {
        return this.fileManagerDataModel.getSelectedDisk();
    }

    public void setSelectedDisk(File disk) {
        this.fileManagerDataModel.setSelectedDisk(disk);
    }
}
