package net.javajoy.jps.exam.om;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import java.io.File;
import java.util.List;

public class FileManagerTableModel extends AbstractTableModel {
    private FileManagerDataModel dataModel;

    public FileManagerTableModel(FileManagerDataModel dataModel) {
        this.dataModel = dataModel;
    }

    @Override
    public int getRowCount() {
        return dataModel.getFileList().size();
    }

    @Override
    public int getColumnCount() {
        return Title.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return dataModel.getFileAt(rowIndex, columnIndex);
    }

    @Override
    public String getColumnName(int column) {
        return Title.values()[column].getName();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        dataModel.getFileList().get(rowIndex).set(columnIndex, aValue);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public void setData(List<List<Object>> data) {
        dataModel.setFileList(data);
        fireTableChanged(new TableModelEvent(this));
    }

    public void updateData(FileManagerDataModel model, File path) {
        model.prepareFileList(path);
        this.setData(model.getFileList());
    }
}
