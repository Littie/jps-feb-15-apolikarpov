package net.javajoy.jps.exam.om;

public class ModelProvider {
    private static CompositeFileManagerModel leftModel;
    private static CompositeFileManagerModel rightModel;

    public static CompositeFileManagerModel getLeftModel() {
        if (leftModel == null) {
            leftModel = new CompositeFileManagerModel();
        }
        return leftModel;
    }

    public static CompositeFileManagerModel getRightModel() {
        if (rightModel == null) {
            rightModel = new CompositeFileManagerModel();
        }
        return rightModel;
    }
}
