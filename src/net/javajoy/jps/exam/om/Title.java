package net.javajoy.jps.exam.om;

public enum Title {
    NAME("Name"),
    TYPE("Type"),
    SIZE("Size"),
    DATE("Date");

    private String name;

    Title(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
