package net.javajoy.jps.exam.om;

public enum Menu {

    COPY("Copy"),
    CUT("Cut"),
    DELETE("Delete"),
    PASTE("Paste"),
    EXIT("Exit");

    public static final String ICON_PATTERN_PATH = "/resources/exam/images/%s.png";

    private String name;

    Menu(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getImagePath() {
        return String.format(ICON_PATTERN_PATH, name.toLowerCase());
    }
}
