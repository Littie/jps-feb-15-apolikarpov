package net.javajoy.jps.exam.om;

import javax.swing.*;
import java.io.File;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class FileManagerDataModel {
    private static final String PARENT_DIRECTORY = "[...]";
    private static final String SET_TYPE_IF_FILE = "";
    private static final String SET_TYPE_IF_DIRECTORY = "folder";
    private static final String DOT = ".";
    private static final int DEFAULT_DISK = 0;
    private static final int FULL_PATH_TO_FILE = 4;
    public static final String NO_EXTENSION = "";

    private List<List<Object>> fileList;
    private File selectedDisk;
    private int selectedRow;

    public FileManagerDataModel() {
        File[] systemDisks = File.listRoots();
        this.selectedDisk = systemDisks[DEFAULT_DISK];
        this.fileList = new LinkedList<>();
        init();
    }

    private void init() {
        prepareFileList(selectedDisk);
    }

    private void insertCells(File file, List<List<Object>> list, boolean parent) {
        List<Object> row = new LinkedList<>();

        if (parent) {
            row.add(PARENT_DIRECTORY);
        } else {
            row.add(getFileName(file));
        }

        if (file.isFile()) {
            row.add(getExtension(file));
            row.add(file.length());
        } else {
            row.add(SET_TYPE_IF_FILE);
            row.add(SET_TYPE_IF_DIRECTORY);
        }

        row.add(new Date(file.lastModified()));
        row.add(file);

        list.add(row);
    }

    private String getExtension(File file) {
        String fileName = file.toString();
        if (file.isFile() && fileName.contains(DOT)) {
            int pos = fileName.lastIndexOf(DOT) + 1;
            return fileName.substring(pos);
        }

        return NO_EXTENSION;
    }

    private String getFileName(File file) {
        String fileName = file.getName();
        if (file.isFile() && fileName.contains(DOT)) {
            int pos = fileName.lastIndexOf(DOT);
            return fileName.substring(0, pos);
        }

        return fileName;
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public void prepareFileList(File path) {
        fileList.clear();

        if (path.getParentFile() != null) {
            insertCells(path.getParentFile(), fileList, true);
        }

        if (path.list() == null) {
            JOptionPane.showMessageDialog(null, "Insert disk in drive " + path);
            prepareFileList(selectedDisk);
        } else {
            for (String dirFile : path.list()) {
                File file = new File(path, dirFile);

                if (file.isFile() && file.isHidden()) {
                    continue;
                }

                insertCells(file, fileList, false);
            }
        }
    }

    public List<List<Object>> getFileList() {
        return fileList;
    }

    public File getSelectedDisk() {
        return selectedDisk;
    }

    public String getFullPathToFile(int row) {
        return fileList.get(row).get(FULL_PATH_TO_FILE).toString();
    }

    public int getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(int selectedRow) {
        this.selectedRow = selectedRow;
    }

    public void setFileList(List<List<Object>> data) {
        this.fileList = data;
    }

    public Object getFileAt(int rowIndex, int columnIndex) {
        return this.fileList.get(rowIndex).get(columnIndex);
    }

    public void setSelectedDisk(File disk) {
        this.selectedDisk = disk;
    }

    //------------------------------------------------------------------------------------------------------------------
}
