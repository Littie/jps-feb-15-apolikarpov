package net.javajoy.jps.homework.w18;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 1. Напишите приложение, создающее крупные объекты по мере ввода данныхи проследите за использованием памяти с помощью jvisualvm.
 * Поэкспериментируйте с количеством объектов, ссылки на которые удаляются.
 * Как влияет их количество назаполнение сегментов Survivor и Old Generation?
 *
 * 2. Добейтесь заполнения всего пространства heap. Что будет, если продолжить создавать объекты?
 *
 * 3. Запакуйте все классы, созданные на предыдущем занятии в один .jar-архив.
 *
 */

public class TestMemory {
    private static final int BLOCK_OF_MEMORY = 1024;

    private static List<byte[]> bytes = new ArrayList<>(); //AN: здесь лучше LinkedList выбрать
    private static boolean exit = false;
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws Exception {
        System.out.println("Welcome to Memory Tool!");

        while (!exit) {
            System.out.println(
                    "\n\nI have " + bytes.size() + " bytes in use, about " +
                            (bytes.size() * 10) + " MB." +
                            "\nWhat would you like me to do?\n" +
                            "1. Create some bytes\n" +
                            "2. Remove some bytes\n" +
                            "0. Quit");

            String input = in.readLine();
            if ((input != null) && (input.length() >= 1)) {
                if (input.startsWith("0")) exit = true;
                else if (input.startsWith("1")) createObjects();
                else if (input.startsWith("2")) removeObjects();
            }
        }

        System.out.println("Bye!");
    }

    private static void createObjects() {
        System.out.println("Creating bytes...");

        bytes.add(new byte[BLOCK_OF_MEMORY * BLOCK_OF_MEMORY * 10]);
    }

    private static void removeObjects() {
        System.out.println("Removing bytes...");
        int i;

        if ((i = bytes.size() - 1) >= 0) {
            bytes.remove(i);
        } else {
            System.out.println("Can not delete. Byte size is " + (i + 1));
        }
    }
}
