package net.javajoy.jps.homework.w10.ui;

import net.javajoy.jps.homework.w10.DataBase;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EntryForm extends JFrame {
    private JPanel entryPanel;
    private JTextField loginTextField;
    private JTextField passwordTextField;
    private JButton loginButton;
    private JButton clearButton;
    private JLabel loginLabel;
    private JLabel passwordLabel;
    private JLabel messageLabel;
    private DataBase base;

    public EntryForm() {
        setSize(230, 110);
        getContentPane().add($$$getRootComponent$$$());
        setVisible(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loginTextField.setText("");
                passwordTextField.setText("");
            }
        });

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String login = loginTextField.getText();
                String password = passwordTextField.getText();

                if (checkLoginAndPassword(login, password)) {
                    messageLabel.setEnabled(true);
                    messageLabel.setForeground(Color.GREEN);
                    messageLabel.setText("Logged");
                } else {
                    messageLabel.setEnabled(true);
                    messageLabel.setForeground(Color.RED);
                    messageLabel.setText("Incorrect login or password");
                }
            }
        });
    }

    public void getBase(DataBase base) {
        this.base = base;
    }

    private boolean checkLoginAndPassword(String login, String password) {
        return base.verifyLoginAndPassword(login.toLowerCase(), password);
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        entryPanel = new JPanel();
        entryPanel.setLayout(new GridBagLayout());
        loginLabel = new JLabel();
        loginLabel.setText("Login:");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        entryPanel.add(loginLabel, gbc);
        passwordLabel = new JLabel();
        passwordLabel.setText("Password:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        entryPanel.add(passwordLabel, gbc);
        loginTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        entryPanel.add(loginTextField, gbc);
        passwordTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        entryPanel.add(passwordTextField, gbc);
        loginButton = new JButton();
        loginButton.setText("Login");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        entryPanel.add(loginButton, gbc);
        clearButton = new JButton();
        clearButton.setText("Clear");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        entryPanel.add(clearButton, gbc);
        messageLabel = new JLabel();
        messageLabel.setEnabled(false);
        messageLabel.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        entryPanel.add(messageLabel, gbc);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return entryPanel;
    }
}
