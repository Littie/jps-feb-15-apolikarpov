package net.javajoy.jps.homework.w10;


import net.javajoy.jps.homework.w10.ui.EntryForm;

/**
 * Main testing class
 */
public class Main {
    public static void main(String[] args) {
        run();
    }

    /**
     * Main method
     */
    public static void run() {
        DataBase base = new DataBase();
        EntryForm form = new EntryForm();

        fillUsersBase(base);
        form.getBase(base);
    }

    /**
     * Fill base
     *
     * @param base base of data
     */
    public static void fillUsersBase(DataBase base) {
        base.add("Artem", "artem");
        base.add("Vasja", "vasja");
        base.add("Petja", "petja");
    }
}
