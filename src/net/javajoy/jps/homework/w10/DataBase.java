package net.javajoy.jps.homework.w10;

import java.util.HashMap;
import java.util.Map;

/**
 * Data base. Contents login and passwords
 */
public class DataBase {
    private Map<String, String> base;

    /**
     * Initial constructor
     */
    public DataBase() {
        base = new HashMap<>();
    }

    /**
     * Add login and password in base
     *
     * @param name     user name
     * @param password user password
     */
    public void add(String name, String password) {
        base.put(name.toLowerCase(), password);
    }

    /**
     * Verify login and password
     *
     * @param user     user name
     * @param password user password
     * @return true if verify is Ok else return false
     */
    public boolean verifyLoginAndPassword(String user, String password) {
        return base.get(user) != null && base.get(user).equals(password);

    }
}
