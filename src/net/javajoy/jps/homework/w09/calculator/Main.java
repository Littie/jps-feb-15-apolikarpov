package net.javajoy.jps.homework.w09.calculator;

public class Main {
    public static void main(String[] args) {
        Stack stack = new Stack();

        stack.push("String 1");
        stack.push("String 2");
        stack.push("String 3");


        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        System.out.println(stack.pop());
        System.out.println(stack.getSize());
    }
}
