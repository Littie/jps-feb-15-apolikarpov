package net.javajoy.jps.homework.w09.calculator;

import java.util.ArrayList;
import java.util.List;

/**
 * Stack of string
 */
public class Stack {
    private List<String> stack;
    private int top;

    /**
     * Initial constructor
     */
    public Stack() {
        stack = new ArrayList<>();
        top = stack.size() - 1;
    }

    /**
     * Add element in last position of stack
     *
     * @param element element of String type
     */
    public void push(String element) {
        stack.add(element);
        top++;
    }

    /**
     * Returns the last element and removes it from the stack
     */
    public String pop() {
        String topElement = stack.get(top);
        stack.remove(top--);

        return topElement;
    }

    /**
     * Returns the last element from the stack
     */
    public String peek() {
        return stack.get(top);
    }

    /**
     * Return size of stack
     *
     * @return size of stack
     */
    public int getSize() {
        return top + 1;
    }

    /**
     * Check whether the stack is empty
     *
     * @return true if stack is not empty
     * false if stack is empty
     */
    public boolean isEmpty() {
        return top < 0;
    }
}
