package net.javajoy.jps.homework.w09.diary;

import net.javajoy.jps.homework.w02.Time;

import java.util.Calendar;

/**
 * Class Date extends class Time. Add three additional fields
 * day, month, year.
 */
public final class Date extends Time {
    private int day;
    private int month;
    private int year;

    /**
     * Initialization constructor. Hours, minutes, seconds is sets to zero
     *
     * @param day   number of day from 1 to 31
     * @param month number of month from 1 to 12
     */
    public Date(int day, int month) {
        super(0, 0, 0);

        if (day > 0 && day <= 31 && month > 0 && month <= 12) {
            this.day = day;
            this.month = month;
            this.year = Calendar.getInstance().get(Calendar.YEAR);
        } else {
            throw new IllegalArgumentException("Illegal input data: " + day + " " + month);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Date date = (Date) o;

        return day == date.day && month == date.month && year == date.year;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + day;
        result = 31 * result + month;
        result = 31 * result + year;
        return result;
    }

    @Override
    public String toString() {
        return String.format("%02d.%02d.%02d", this.day, this.month, this.year);
    }
}
