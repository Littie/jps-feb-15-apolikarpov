package net.javajoy.jps.homework.w09.diary;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Main test class
 */
public class Main {
    public static final String ADD_RECORD = "1";
    public static final String DELETE_RECORD = "2";
    public static final String SEARCH_RECORD = "3";
    public static final String SHOW_ALL_RECORDS = "4";
    public static final String EXIT = "5";

    public static void main(String[] args) {
        run();
    }

    /**
     * Main method for testing
     */
    public static void run() {
        Diary diary = new Diary();
        Scanner inputData = new Scanner(System.in);

        while (true) {
            menu();

            String num = inputData.nextLine();
            switch (num) {
                case ADD_RECORD:
                    addRecords(diary);
                    break;
                case DELETE_RECORD:
                    deleteRecord(diary);
                    break;
                case SEARCH_RECORD:
                    searchRecord(diary);
                    break;
                case SHOW_ALL_RECORDS:
                    getAllRecords(diary);
                    break;
                case EXIT:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Incorrect input data. Please try again");
                    break;

            }
        }
    }

    /**
     * Add record in diary
     *
     * @param diary diary
     */
    private static void addRecords(Diary diary) {
        Scanner inputData = new Scanner(System.in);

        System.out.println("Please, enter your record:");
        System.out.println("Add time: " + diary.addRecord(inputData.nextLine()));
    }

    /**
     * Delete record from diary
     *
     * @param diary diary
     */
    private static void deleteRecord(Diary diary) {
        System.out.println("Please, enter day in dd.mm format");

        Date date = ConsoleDateProvider.enterDate();

        List<String> records = diary.getListRecordsByDate(date);

        System.out.println("Available records on " + date + ":");

        int num = 1;

        for (String record : records) {
            System.out.println(num + ". " + record);
            num++;
        }

        System.out.println("Enter the number you want to delete: ");

        try {
            if (diary.deleteRecord(date, new Scanner(System.in).nextInt())) {
                System.out.println("Record was successfully deleted");
            } else {
                System.out.println("Such records do not exist");
            }
        } catch (InputMismatchException ex) {
            System.out.println("Incorrect input data");
        }
    }


    /**
     * Search records in diary
     *
     * @param diary diary
     */
    private static void searchRecord(Diary diary) {
        Scanner inputData = new Scanner(System.in);

        System.out.println("Please, enter string for search");

        System.out.println(diary.getRecords(inputData.nextLine()));
    }

    /**
     * Print all records from diary
     *
     * @param diary diary
     */
    private static void getAllRecords(Diary diary) {
        System.out.println(diary.getAllRecords());
    }

    /**
     * Man menu
     */
    private static void menu() {
        System.out.println("1. Add record");
        System.out.println("2. Delete record");
        System.out.println("3. Search records");
        System.out.println("4. Show all records");
        System.out.println("5. Exit");
    }
}
