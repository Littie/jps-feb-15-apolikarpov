package net.javajoy.jps.homework.w09.diary;

import net.javajoy.jps.homework.w02.Time;

import java.util.*;

/**
 * Class Diary. Contain records and his creation time
 */
public class Diary {
    private Map<Date, List<String>> diary;

    /**
     * Initial constructor
     */
    public Diary() {
        diary = new HashMap<>();
    }

    /**
     * Add record in diary
     *
     * @param record record of String type
     * @return instance of Time class
     */
    public Time addRecord(String record) {
        Calendar calendar = Calendar.getInstance();
        Date date = new Date(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1);

        if (diary.containsKey(date)) {
            diary.get(date).add(record);
        } else {
            List<String> records = new ArrayList<>();
            records.add(record);
            diary.put(date, records);
        }

        return date;
    }

    /**
     * Delete record from diary
     *
     * @return true if record was deleted
     * false if record delete failed
     */
    public boolean deleteRecord(Date date, int numberOfRecord) {
        List<String> records = diary.get(date);

        if (numberOfRecord < 0 || numberOfRecord > records.size()) {
            return false;
        } else {
            records.remove(numberOfRecord - 1);
            return true;
        }
    }

    /**
     * Get records from diary by input value
     *
     * @param recordPart time of record
     * @return record
     */
    public String getRecords(String recordPart) {
        String outputString = "";

        for (Date date : diary.keySet()) {
            List<String> records = diary.get(date);

            for (String record : records) {
                if (recordPart != null) {
                    if (record.contains(recordPart)) {
                        outputString += "Date: " + date + " " + "Text: " + record + "\n";
                    }
                } else {
                    outputString += "Date: " + date + " " + "Text: " + record + "\n";
                }
            }
        }

        return getOrDefault(outputString);
    }

    /**
     * Get all record from diary
     *
     * @return string that contains all records
     */
    public String getAllRecords() {
        return getOrDefault(getRecords(null));
    }

    public List<String> getListRecordsByDate(Date date) {
        return diary.get(date);
    }

    /**
     * Method checks whether the string is empty and if is empty, return information message,
     * else return input string
     *
     * @param outputString string
     * @return resulting string
     */
    private static String getOrDefault(String outputString) {
        return outputString.isEmpty() ? "There are no records" : outputString;
    }
}
