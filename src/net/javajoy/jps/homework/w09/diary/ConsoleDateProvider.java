package net.javajoy.jps.homework.w09.diary;

import java.util.Scanner;

public class ConsoleDateProvider {
    public static final String PATTERN = "(([0-2][0-9])|(3[0-1]))\\.([0][0-9]|(1[0-2]))";

    /**
     * Создает объект Date на основании введенных пользователем данных
     * Формат данных dd.mm
     *
     * @return объект Date. NULL никогда не возвращает
     */
    public static Date enterDate() {
        Scanner in = new Scanner(System.in);

        while (in.hasNext()) {
            String date = in.nextLine();
            if (checkInputData(date)) {
                String[] arr = date.split("\\.");
                return new Date(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
            } else {
                System.out.println("Incorrect input format. Try again");
            }
        }

        return null;
    }

    /**
     * Проверка корректности введенных c консоли данных
     *
     * @param inputData входящие данные
     * @return результат
     */

    private static boolean checkInputData(String inputData) {
        return inputData.matches(PATTERN);
    }
}
