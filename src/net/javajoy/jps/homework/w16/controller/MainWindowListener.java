package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.model.PersonModel;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class MainWindowListener extends WindowAdapter {
    private PersonModel personModel; //AP: Ok, понял

    public MainWindowListener(PersonModel personModel) {
        this.personModel = personModel;
    }

    @Override
    public void windowClosing(WindowEvent event) {
        File path = new File(PersonModel.PATH_TO_FILE.replace("save.dat", ""));

        if (!path.exists()) {
            path.mkdirs();
        }

        try (ObjectOutputStream outData = new ObjectOutputStream(new FileOutputStream(PersonModel.PATH_TO_FILE))) {
            outData.writeObject(personModel);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
