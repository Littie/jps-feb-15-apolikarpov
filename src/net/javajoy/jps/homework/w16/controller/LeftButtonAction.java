package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.model.PersonModel;
import net.javajoy.jps.homework.w16.model.ui.LeftListModel;
import net.javajoy.jps.homework.w16.model.ui.RightListModel;
import net.javajoy.jps.homework.w16.view.MainWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class LeftButtonAction extends AbstractAction {
    private MainWindow mainWindow;
    private PersonModel personModel;

    public LeftButtonAction(MainWindow mainWindow, PersonModel personModel) {
        this.mainWindow = mainWindow;
        this.personModel = personModel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JList<String> list = mainWindow.getRightList();

        for (String value : list.getSelectedValuesList()) {
            personModel.addAcquainted(value);
        }

        int i = 0;
        for (int element : list.getSelectedIndices()) {
            personModel.removeFriend(element - i);
            i++;
        }

        ((LeftListModel) mainWindow.getLeftList().getModel()).update();
        ((RightListModel) mainWindow.getRightList().getModel()).update();
    }
}
