package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.model.PersonModel;
import net.javajoy.jps.homework.w16.view.MainWindow;

public class MainController {
    private MainWindow mainWindow;
    private PersonModel personModel;

    public MainController(MainWindow mainWindow, PersonModel personModel) {
        this.mainWindow = mainWindow;
        this.personModel = personModel;
        initControllers();
    }

    private void initControllers() {
        mainWindow.getLeftButton().addActionListener(new LeftButtonAction(mainWindow, personModel));
        mainWindow.getRightButton().addActionListener(new RightButtonAction(mainWindow, personModel));
        mainWindow.addWindowListener(new MainWindowListener(personModel));
    }
}
