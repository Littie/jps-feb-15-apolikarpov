package net.javajoy.jps.homework.w16.dao;

import net.javajoy.jps.homework.w16.model.PersonModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class PersonDao {
    private PersonModel personModel;

    public PersonDao() {
        personModel = loadData();
    }

    private PersonModel loadData() {
        if (!PersonModel.getInstance().isRestored()) {
            tryRestoreOrDefault();
        }

        return personModel;
    }

    private void tryRestoreOrDefault() {
        if (new File(PersonModel.PATH_TO_FILE).exists()) {
            try (ObjectInputStream inputData = new ObjectInputStream(new FileInputStream(PersonModel.PATH_TO_FILE))) {
                PersonModel personModel = (PersonModel) inputData.readObject();
                PersonModel.setInstance(personModel);
            } catch (ClassNotFoundException | IOException ex) {
                ex.printStackTrace();
            }
        }

        personModel = PersonModel.getInstance();
    }

    public int getAcquaintedSize() {
        return personModel.getAcquaintedSize();
    }

    public int getFriendSize() {
        return personModel.getFriendSize();
    }

    public String getAcquaintedElementAt(int index) {
        return personModel.getAcquaintedAt(index);
    }

    public String getFriendElementAt(int index) {
        return personModel.getFriendAt(index);
    }


}
