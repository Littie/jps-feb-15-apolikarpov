package net.javajoy.jps.homework.w16.view;

import net.javajoy.jps.homework.w16.dao.PersonDao;
import net.javajoy.jps.homework.w16.model.ui.LeftListModel;
import net.javajoy.jps.homework.w16.model.ui.RightListModel;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    private static final int FIELD_WIDTH = 400;
    private static final int FIELD_HEIGHT = 200;

    private JLabel acquaintedLabel;
    private JLabel friendLabel;
    private JButton leftButton;
    private JButton rightButton;
    private JList<String> leftList;
    private JList<String> rightList;

    public MainWindow() {
        init();
    }

    private void init() {
        setPreferredSize(new Dimension(FIELD_WIDTH, FIELD_HEIGHT));
        initComponents();
        addComponents();
        setResizable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        acquaintedLabel = new JLabel("Acquainted");
        friendLabel = new JLabel("Friends");
        leftButton = new JButton("<<<");
        rightButton = new JButton(">>>");
        initJList();
    }

    private void initJList() {
        PersonDao personDao = new PersonDao();

        leftList = new JList<>();
        leftList.setModel(new LeftListModel(personDao));

        rightList = new JList<>();
        rightList.setModel(new RightListModel(personDao));
    }

    private void addComponents() {
        setLayout(new GridBagLayout());

        getContentPane().add(acquaintedLabel,
                new GridBagConstraints(0, 0, 1, 1, 0.5, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(6, 6, 0, 6), 0, 0));
        getContentPane().add(friendLabel,
                new GridBagConstraints(2, 0, 1, 1, 0.5, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(6, 6, 0, 6), 0, 0));
        getContentPane().add(leftButton,
                new GridBagConstraints(1, 1, 1, 1, 0.2, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(6, 6, 6, 6), 0, 0));
        getContentPane().add(rightButton,
                new GridBagConstraints(1, 2, 1, 0, 0.2, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(6, 6, 6, 6), 0, 0));
        getContentPane().add(leftList,
                new GridBagConstraints(0, 1, 1, 2, 1, 0.9, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 6, 6, 6), 0, 0));
        getContentPane().add(rightList,
                new GridBagConstraints(2, 1, 1, 2, 1, 0.9, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 6, 6, 6), 0, 0));
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------
    public JButton getLeftButton() {
        return leftButton;
    }

    public JButton getRightButton() {
        return rightButton;
    }

    public JList<String> getRightList() {
        return rightList;
    }

    public JList<String> getLeftList() {
        return leftList;
    }


    //------------------------------------------------------------------------------------------------------------------
}
