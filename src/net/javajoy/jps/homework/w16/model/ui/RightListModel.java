package net.javajoy.jps.homework.w16.model.ui;

import net.javajoy.jps.homework.w16.dao.PersonDao;

import javax.swing.*;

public class RightListModel extends AbstractListModel<String> {
    private PersonDao personDao;

    public RightListModel(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public int getSize() {
        return personDao.getFriendSize();
    }

    @Override
    public String getElementAt(int index) {
        return personDao.getFriendElementAt(index);
    }

    public void update() {
        fireContentsChanged(this, 0 , personDao.getFriendSize() - 1);
    }
}
