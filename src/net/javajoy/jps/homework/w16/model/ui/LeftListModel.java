package net.javajoy.jps.homework.w16.model.ui;

import net.javajoy.jps.homework.w16.dao.PersonDao;

import javax.swing.*;

public class LeftListModel extends AbstractListModel<String> {
    private PersonDao personDao;

    public LeftListModel(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public int getSize() {
        return personDao.getAcquaintedSize();
    }

    @Override
    public String getElementAt(int index) {
        return personDao.getAcquaintedElementAt(index);
    }

    public void update() {
        fireContentsChanged(this, 0 , personDao.getAcquaintedSize() - 1);
    }
}
