package net.javajoy.jps.homework.w16.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PersonModel implements Serializable {
    public static final String PATH_TO_FILE = "resources/w16/save.dat";
    private static PersonModel instance;

    private List<String> acquainted;
    private List<String> friend;
    private boolean restored = false;

    private PersonModel() {
        acquainted = new ArrayList<>();
        friend = new ArrayList<>();
        fillData();
    }

    public static PersonModel getInstance() {
        if (instance == null) {
            instance = new PersonModel();
        }

        return instance;
    }

    private void fillData() {
        acquainted.add("Вася");
        acquainted.add("Петя");
        acquainted.add("Ира");
        acquainted.add("Настя");
        acquainted.add("Сергей");

        friend.add("Маша");
        friend.add("Катя");
        friend.add("Ваня");
        friend.add("Кирилл");
        friend.add("Коля");
    }

    public int getAcquaintedSize() {
        return acquainted.size();
    }

    public int getFriendSize() {
        return friend.size();
    }

    public String getAcquaintedAt(int index) {
        return acquainted.get(index);
    }

    public String getFriendAt(int index) {
        return friend.get(index);
    }

    public void addAcquainted(String value) {
        acquainted.add(value);
    }

    public void addFriend(String value) {
        friend.add(value);
    }

    public void removeAcquainted(int index) {
        this.acquainted.remove(index);
    }

    public void removeFriend(int index) {
        this.friend.remove(index);
    }

    public static void setInstance(PersonModel personModel) {
        instance = personModel;
        instance.restored = true;
    }

    public boolean isRestored() {
        return restored;
    }
}
