package net.javajoy.jps.homework.w16;

import net.javajoy.jps.homework.w16.controller.MainController;
import net.javajoy.jps.homework.w16.model.PersonModel;
import net.javajoy.jps.homework.w16.view.MainWindow;

//AN: есть небольшая проблема с архитектурой. В целом, разделение правильное
public class Main {
    public static void main(String[] args) {
        new MainController(new MainWindow(), PersonModel.getInstance());
    }
}
