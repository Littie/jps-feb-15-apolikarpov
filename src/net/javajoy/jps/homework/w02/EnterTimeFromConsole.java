package net.javajoy.jps.homework.w02;

import java.util.Scanner;

public class EnterTimeFromConsole {
    /**
     * Создает объект Time на основании введенных пользователем данных
     * Формат данных hh:mm:ss
     *
     * @return объект Time
     */
    //AN: так как Time это класс для данных, метод для ввода данных должен быть вынесен отсюда в другой или отдельный класс
    //AP: обращал на это внимание, поленился исправить ))
    public static Time enterTime() {
        Scanner in = new Scanner(System.in);

        System.out.println("Введите время в формате hh:mm:ss");

        while (in.hasNext()) {
            String str = in.nextLine();
            if (checkInputData(str)) {
                String[] arr = str.split(":");
                return new Time(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));
            } else {
                System.out.println("Неверный формат ввода. Повторите попытку");
            }
        }

        return null;
    }

    /**
     * Проверка корректности введенных c консоли данных
     *
     * @param inputData входящие данные
     * @return результат
     */

    private static boolean checkInputData(String inputData) {
        String pattern = "(([0,1][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]";
        return inputData.matches(pattern);
    }
}
