package net.javajoy.jps.homework.w02;

public class Main {

    public static void main(String[] args) {
        // Создаем время через консоль ввода
        Time timeOne = EnterTimeFromConsole.enterTime();
        System.out.println("Значение времени введенное с клавиатуры " + timeOne);
        // Создаем время через копирующий конструктор
        Time timeTwo = new Time(timeOne);
        System.out.println("Время, созданное на базе первого времени, введенного с клавиатуры " + timeTwo);
        // Сравниваем времена timeOne и timeTwo
        compareTimes(timeOne, timeTwo);

        // Выводим разницу в секундах между временем timeOne и TimeTwo
        int difference = timeOne.differenceBetweenTimes(timeTwo);
        System.out.println("Разница в секундах " + difference);

        // Создаем время через конструктор по умолчанию
        Time timeThree = new Time();
        System.out.println("Время, созданное через конструктор по умолчанию. Все значения равны нулю " + timeThree);
        // Создаем время через конструктор с параметрами
        Time timeFour = new Time(23, 0, 15);
        System.out.println("Время, созданное через конструктор с параметрами " + timeFour);
        // Сравниваем времена timeThree и timeFour
        compareTimes(timeThree, timeFour);

        // Выводим разницу в секундах между временем timeThree и timeFour
        difference = timeThree.differenceBetweenTimes(timeFour);
        System.out.println("Разница в секундах " + difference);

        // Демонстрация метода getTimeObject
        Time timeFive = Time.getTimeObject(23, 15, 5);
        System.out.println("Время, созданное через статический метод по трем заданным числам " + timeFive);
    }

    // Сравнение времен
    private static void compareTimes(Time time1, Time time2) {
        int res = Time.compareTime(time1, time2);
        if (res < 0) {
            System.out.println("Время " + time1 + " меньше времени " + time2);
        } else if (res > 0) {
            System.out.println("Время " + time1 + " больше времени " + time2);
        } else {
            System.out.println("Время " + time1 + " равно времени " + time2);
        }
    }

}
