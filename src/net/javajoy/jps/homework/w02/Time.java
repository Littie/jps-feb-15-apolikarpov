package net.javajoy.jps.homework.w02;

/**
 * 1. Напишите класс «время» для представления времени в пределах суток. Внутренне представление - три целые числа,
 * задающие часы, минуты и секунды.  Класс должен содержать:
 * -  конструкторы (по умолчанию, с параметрами и копирующий);
 * -  статический метод создания объекта по трем заданным числам;
 * -  статический метод сравнения на больше-меньше;
 * -  не статический метод нахождения секунд, разделяющих два времени.
 * <p/>
 * <p/>
 * 2. Создать два объекта «время» заполнив их значениями из консоли, используя разные способы создания экземпляра объекта.
 * Вывести результаты сравнения двух объектов на консоль (кто больше из них, разница в секундах). Подумайте,
 * как бы вы могли обезопасить свой класс от ввода некорректных данных. Если такое происходит, следует информировать
 * об этом пользователя.
 */

public class Time implements Cloneable {
    public static final int AMOUNT_SECONDS_IN_HOUR = 3600;
    public static final int AMOUNT_SECONDS_IN_MINUTE = 60;

    private int hour;
    private int minute;
    private int second;

    /*
     * Конструктор по умолчанию
     */
    public Time() {

    }

    /*
     * Конструктор с параметрами
     */
    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    /*
     * Копирующий конструктор
     */
    public Time(Time time) {
        this.hour = time.hour;
        this.minute = time.minute;
        this.second = time.second;
    }

    /*
     * Getter of hour field
     */
    public int getHour() {
        return hour;
    }

    /*
     * Getter of minute field
     */
    public int getMinute() {
        return minute;
    }

    /*
     * Getter of second field
     */
    public int getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object otherObject) {
        // Быстрая проверка объектов на идентичность
        if (this == otherObject) return true;

        // Возвратить false, если otherObject равен null
        if (otherObject == null) return false;

        // Если классы не совпадают, они не равнозначны
        if (getClass() != otherObject.getClass()) return false;

        // Приведение типа
        Time other = (Time) otherObject;

        return this.hour == other.hour && this.minute == other.minute && this.second == other.second;
    }

    @Override
    public int hashCode() {
        int result = hour;
        result = 31 * result + minute;
        result = 31 * result + second;
        return result;
    }

    @Override
    public Time clone() throws CloneNotSupportedException {
        try {
            return (Time) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Возвращает объект Time
     *
     * @param hour   часы
     * @param minute минуты
     * @param second секунды
     * @return объект Time
     */

    public static Time getTimeObject(int hour, int minute, int second) {
        return new Time(hour, minute, second);
    }

    /**
     * Возвращает время в секундах
     *
     * @param time время в формате hh:mm:ss
     * @return время в секундах
     */

    public static int translateTimeInSeconds(Time time) {
        return time.hour * AMOUNT_SECONDS_IN_HOUR + time.minute * AMOUNT_SECONDS_IN_MINUTE + time.second;
    }

    /**
     * Сравнивает два времени на больше-меньше
     *
     * @param timeFirst  время 1
     * @param timeSecond время 2
     *                   <p/>
     *                   return -1, если timeFirst < timeSecond
     *                   1, если timeFirst > timeSecond
     *                   0, если timeFirst = timeSecond
     */

    public static int compareTime(Time timeFirst, Time timeSecond) {
        int time1 = translateTimeInSeconds(timeFirst);
        int time2 = translateTimeInSeconds(timeSecond);
        if (time1 < time2) {
            return -1;
        } else if (time1 > time2) {
            return 1;
        } else {
            return 0;
        }
    }

    public int differenceBetweenTimes(Time time) {
        return Math.abs(translateTimeInSeconds(this) - translateTimeInSeconds(time));
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
    }

}


















