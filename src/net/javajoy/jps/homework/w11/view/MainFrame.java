package net.javajoy.jps.homework.w11.view;

import net.javajoy.jps.homework.w11.view.panels.FieldsPanel;
import net.javajoy.jps.homework.w11.view.panels.InformationPanel;
import net.javajoy.jps.homework.w11.view.panels.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {
    public static final int WIDTH = 400;
    public static final int HEIGHT = 400;

    private JPanel mainPanel = new MainPanel();
    private JPanel fieldsPanel = new FieldsPanel();
    private JPanel informationPanel = new InformationPanel();


    public MainFrame() {
        addPanels();
        initFrame();
    }

    private void initFrame() {
        setSize(new Dimension(WIDTH, HEIGHT));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setJMenuBar(addJMenuBar()); // AS: помоему пока что не работает )
        setVisible(true);
    }

    private void addPanels() {
        add(mainPanel);
        mainPanel.add(informationPanel, setConstraintsForInformationPanel());
        mainPanel.add(fieldsPanel, setConstraintsForFieldsPanel());
    }

    private JMenuBar addJMenuBar() {
        JMenuBar mainMenu = new JMenuBar();
        JMenu fileMenu = new JMenu("File");

        JMenuItem exitGame = new JMenuItem("Exit");

        exitGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        fileMenu.addSeparator();
        fileMenu.add(exitGame);

        mainMenu.add(fileMenu);

        return mainMenu;
    }

    private GridBagConstraints setConstraintsForInformationPanel() {
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.BOTH;

        return constraints;
    }

    private GridBagConstraints setConstraintsForFieldsPanel() {
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.fill = GridBagConstraints.BOTH;

        return constraints;
    }

}
