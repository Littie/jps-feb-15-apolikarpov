package net.javajoy.jps.homework.w11.view.panels;

import net.javajoy.jps.homework.w11.model.Fields;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FieldsPanel extends JPanel {
    public static final int SIZE = 5; // AS: это может быть одна константа на приложение

    private Fields fields = Fields.getInstance();


    public FieldsPanel() {
        setLayout(new GridBagLayout());
        addFields();
    }

    private void addFields() {
        Color color;

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {

                if (fields.getFields()[i][j] == 0) {
                    color = new Color(255, 0, 0);
                } else {
                    color = new Color(0, 255, 0);
                }

                addButton(color, i, j);
            }
        }
    }

    private GridBagConstraints setConstraintsForButton(int gridx, int gridy) {
        return new GridBagConstraints(gridx, gridy, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0);
    }

    private void reverseLineColors() {
        Color color;
        int count = 0;

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {

                if (fields.getFields()[i][j] == 0) {
                    color = Color.RED; // AP: Можно
                } else {
                    color = Color.GREEN;
                }

                getComponent(count++).setBackground(color);
            }
        }

        if (fields.getCountOfColorFirst() == 0 || fields.getCountOfColorSecond() == 0) {
            JOptionPane.showMessageDialog(this, "Game over");
            System.exit(0);
        }
    }

    private JButton addButton(Color color, final int x, final int y) {
        JButton button = new JButton("");

        button.setBackground(color);
        add(button, setConstraintsForButton(x, y));

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fields.reverseColorLines(x, y);
                reverseLineColors();
            }
        });

        return button;
    }

}
