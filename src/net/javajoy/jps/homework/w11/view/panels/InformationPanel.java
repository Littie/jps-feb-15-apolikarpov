package net.javajoy.jps.homework.w11.view.panels;

import net.javajoy.jps.homework.w11.ifaces.Observer;
import net.javajoy.jps.homework.w11.model.Fields;

import javax.swing.*;
import java.awt.*;

public class InformationPanel extends JPanel implements Observer {
    private JLabel firstColor = new JLabel();
    private JLabel secondColor = new JLabel();
    private Fields fields = Fields.getInstance();

    public InformationPanel() {
        fields.register(this);
        initPanel();
    }

    private void initPanel() {
        setLayout(new GridBagLayout());
        initFirstColorLabel();
        initSecondColorLabel();
        setConstraintsForFirstColorLabel();
        setConstraintsForSecondColorLabel();
        addComponentsToPanel();
    }

    private void addComponentsToPanel() {
        add(firstColor, setConstraintsForFirstColorLabel());
        add(secondColor, setConstraintsForSecondColorLabel());
    }

    private void initFirstColorLabel() {
        firstColor.setText("First color: " + fields.getCountOfColorFirst());
    }

    private void initSecondColorLabel() {
        secondColor.setText("Second color: " + fields.getCountOfColorSecond());
    }

    private GridBagConstraints setConstraintsForFirstColorLabel() {
        return new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(1, 1, 1, 1), 0, 0);
    }

    private GridBagConstraints setConstraintsForSecondColorLabel() {
        return new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(1, 1, 5, 1), 0, 0);
    }

    //----------------------Observer------------------------

    @Override
    public void update() {
        int[] colors = fields.getUpdate(this);
        firstColor.setText("First color: " + Integer.toString(colors[0]));
        secondColor.setText("Second color: " + Integer.toString(colors[1]));
    }

    //------------------------------------------------------
}
