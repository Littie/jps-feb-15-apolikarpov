package net.javajoy.jps.homework.w11.model;

import net.javajoy.jps.homework.w11.ifaces.Observer;
import net.javajoy.jps.homework.w11.ifaces.Subject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Fields implements Subject {
    public static final int SIZE = 5;

    private static Fields instance;
    private List<Observer> observers;
    private int[][] fields;
    private int countOfColorFirst;
    private int countOfColorSecond;

    private Fields() {
        fields = new int[SIZE][SIZE];
        observers = new ArrayList<>();
        randomFillFields();
    }

    public static Fields getInstance() {
        if (instance == null) {
            instance = new Fields();
        }

        return instance;
    }

    private void randomFillFields() {
        Random rnd = new Random();

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                fields[i][j] = rnd.nextInt(2);
                if (fields[i][j] == 0) {
                    countOfColorFirst++;
                } else {
                    countOfColorSecond++;
                }
            }
        }
    }

    public int[][] getFields() {
        return fields;
    }

    public void reverseColorLines(int x, int y) {
        int currentField = fields[x][y];

        for (int i = 0; i < SIZE; i++) {
            if (fields[i][y] == 0) {
                fields[i][y] = 1;
            } else {
                fields[i][y] = 0;
            }

            if (fields[x][i] == 0) {
                fields[x][i] = 1;
            } else {
                fields[x][i] = 0;
            }

            if (currentField == 0) {
                currentField = 1;
                fields[x][y] = currentField;
            } else {
                currentField = 0;
                fields[x][y] = currentField;
            }
        }

        calculateColors();

        notifyObservers();
    }

    private void calculateColors() {
        countOfColorFirst = 0;
        countOfColorSecond = 0;

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (fields[i][j] == 0) {
                    countOfColorFirst++;
                } else {
                    countOfColorSecond++;
                }
            }
        }
    }

    public static void main(String[] args) {
        Fields fields1 = new Fields();

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                System.out.print(fields1.getFields()[i][j] + " ");
            }

            System.out.println();
        }

        System.out.println();
        fields1.reverseColorLines(4, 0);

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                System.out.print(fields1.getFields()[i][j] + " ");
            }

            System.out.println();
        }
    }

    public int getCountOfColorFirst() {
        return countOfColorFirst;
    }

    public int getCountOfColorSecond() {
        return countOfColorSecond;
    }

    //-----------------------Subject------------------------------

    @Override
    public void register(Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unregister(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    @Override
    public int[] getUpdate(Observer observer) {
        int[] counts = new int[2];
        counts[0] = countOfColorFirst;
        counts[1] = countOfColorSecond;

        return counts;
    }

    //--------------------------------------------------------------
}
