package net.javajoy.jps.homework.w11.ifaces;

public interface Subject {

    public void register(Observer observer);

    public void unregister(Observer observer);

    public void notifyObservers();

    public int[] getUpdate(Observer observer);
}
