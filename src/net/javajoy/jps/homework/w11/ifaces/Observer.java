package net.javajoy.jps.homework.w11.ifaces;

public interface Observer {

    // AS: public в интерфейсах можно опускать
    public void update();

}
