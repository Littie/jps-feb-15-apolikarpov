package net.javajoy.jps.homework.w11;

import net.javajoy.jps.homework.w11.view.MainFrame;

public class Main {
    public static void main(String[] args) {
        new MainFrame();
    }
    // AS: свои цвета для ячеек тоже ок
    // - не ведется автоматический подсчет ячеек каждого цвета после кликов AP: check
    // - нет проверки окончания игры. Легко проверить если заранее проинициализоваровать матрицу как нужно, чтобы после запуска сделать 1 клик AP: check
    // и вся доска окажется одного цвета

    // AS:  молодец, что сам реализовал паттерн Наблюдатель.
}
