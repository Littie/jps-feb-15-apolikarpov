package net.javajoy.jps.homework.w06;

/**
 * Parser for brackets
 * Checks whether placed brackets in the expression
 */
public class Parser {
    public static final String KEY = "{}()";

    private Stack stack;

    /**
     * Initial constructor
     */
    public Parser() {
        stack = new Stack();
    }

    /**
     * Parse string
     *
     * @param expression of String type
     */
    public boolean parse(String expression) {
        for (char c : expression.toCharArray()) {
            if (isBracket(c)) {
                if (stack.getSize() != 0 && isPairBracket(stack.peek(), String.valueOf(c))) {
                    stack.pop();
                } else {
                    stack.push(String.valueOf(c));
                }
            }
        }

        return stack.isEmpty();
    }

    /**
     * Checks whether paired brackets
     *
     * @param openBracket  open brackets
     * @param closeBracket close brackets
     * @return true if brackets are paired
     * false if brackets are not paired
     */
    private static boolean isPairBracket(String openBracket, String closeBracket) {
        return openBracket.equals("(") && closeBracket.equals(")") || openBracket.equals("{") && closeBracket.equals("}");
    }

    /**
     * Check whether character is a bracket
     *
     * @param character character
     * @return true if character is bracket
     *         false if character is not bracket
     */
    private static boolean isBracket(char character) {
        return KEY.contains(String.valueOf(character));
    }
}
