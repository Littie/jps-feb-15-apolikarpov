package net.javajoy.jps.homework.w06;

/**
 * Aleksey checked
 * Content method for work with string collections
 */
public interface IStringSequence {

    /**
     * Adds an value to the end
     *
     * @param value string
     */
    void add(String value);

    /**
     * Adds an element in the specified index position
     *
     * @param index index
     * @param value string
     */
    void addAt(int index, String value);

    /**
     * Deletes an element in the specified index position
     *
     * @param index index
     */
    void delete(int index);

    /**
     * Edits an element in the specified index position
     *
     * @param index index
     * @param value string
     */
    void set(int index, String value);

    /**
     * Search element by index
     *
     * @param index index
     * @return found string
     */
    String searchByIndex(int index);

    /**
     * Search index of string by value
     *
     * @param value string
     * @return index of position
     */
    int searchByValue(String value);
}
