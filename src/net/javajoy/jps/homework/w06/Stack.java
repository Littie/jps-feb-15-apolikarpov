package net.javajoy.jps.homework.w06;

/**
 * Stack of string
 */
public class Stack {
    private Vector stack;

    private int top = -1;

    /**
     * Initial constructor
     */
    public Stack() {
        stack = new Vector();
    }

    /**
     * Returns the last element and removes it from the stack
     *
     * @return instance of TreeNode
     */
    public String pop() {
        String topElement = stack.searchByIndex(top);
        stack.delete(top);
        top--;
        return topElement;
    }

    /**
     * Add node in last position of stack
     *
     * @param value instance of TreeNode
     */
    public void push(String value) {
        stack.add(value);
        top++;
    }

    /**
     * Returns the last element from the stack, but does not remove it
     *
     * @return instance of TreeNode
     */
    public String peek() {
        return stack.searchByIndex(top);
    }

    /**
     * Get size of stack
     *
     * @return size of stack
     */
    public int getSize() {
        return top + 1;
    }

    /**
     * Check whether the stack is empty
     *
     * @return true if stack is not empty
     * false if stack is empty
     */
    public boolean isEmpty() {
        return this.top < 0;
    }

}
