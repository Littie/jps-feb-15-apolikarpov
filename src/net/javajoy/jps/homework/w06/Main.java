package net.javajoy.jps.homework.w06;

/**
 * Test class
 */
public class Main {
    public static void main(String[] args) {
        run();
    }

    /**
     * Test method
     */
    public static void run() {
        System.out.println("=======================================================");
        System.out.println("Testing Vector");
        System.out.println("=======================================================");

        testVector();

        System.out.println("\n=======================================================");
        System.out.println("Testing List");
        System.out.println("=======================================================");

        testList();

        System.out.println("\n=======================================================");
        System.out.println("Testing Parser");
        System.out.println("=======================================================");

        testParser();

        System.out.println("\n=======================================================");
        System.out.println("Testing Tree");
        System.out.println("=======================================================");

        testNodes();
    }

    /**
     * Method for test Vector
     */
    public static void testVector() {
        //AP: Подправил
        int initialSize = 5;
        int addElements = 6;
        int addAtPosition = 5;
        String addAtString = "This string was added";
        int deleteElement = 3;
        int setIndex = 2;
        String setString = "This string was changed";
        int searchIndex = 2;
        String searchString = "This string was changed";

        Vector vector = new Vector(initialSize);
        System.out.println("1. Initial vector in " + initialSize + " elements");

        System.out.println("2. Add " + addElements + " elements in vector and print elements");
        addElementsVector(vector, addElements);
        System.out.println(vector);

        System.out.println("3. Add new string \"" + addAtString + "\" in " + addAtPosition + " index and print elements");
        addAtElementsVector(vector, addAtPosition, addAtString);
        System.out.println(vector);

        System.out.println("4. Delete element with index " + deleteElement + " and print elements");
        deleteElementsVector(vector, deleteElement);
        System.out.println(vector);

        System.out.println("5. Set element with index " + setIndex + " in \"" + setString + "\" value and print elements");
        setElementsVector(vector, setIndex, setString);
        System.out.println(vector);

        System.out.println("\n6. Search element by index " + searchIndex + " and print element");
        System.out.println("Found element \"" + searchByIndexElementsVector(vector, searchIndex) + "\"");

        System.out.println("\n7. Search element by \"" + searchString + "\" value");
        System.out.println("Found element in index " + searchByValueElementsVector(vector, searchString));

        System.out.println("\n8.Test iterator");
        for (String element : vector) {
            System.out.println(element);
        }
    }

    /**
     * Method for test List
     */
    public static void testList() {
        //AN: те же самые комментариии и для этого метода
        int addElements = 6;
        int addAtPosition = 5;
        String addAtString = "This string was added";
        int deleteElement = 3;
        int setIndex = 2;
        String setString = "This string was changed";
        int searchIndex = 2;
        String searchString = "This string was changed";

        List list = new List();
        System.out.println("1. Initial list");

        System.out.println("2. Add " + addElements + " elements in list and print elements");
        addElementsList(list, addElements);
        System.out.println(list);

        System.out.println("3. Add new string \"" + addAtString + "\" in " + addAtPosition + " index and print elements");
        addAtElementsList(list, addAtPosition, addAtString);
        System.out.println(list);

        System.out.println("4. Delete element with index " + deleteElement + " and print elements");
        deleteElementsList(list, deleteElement);
        System.out.println(list);

        System.out.println("5. Set element with index " + setIndex + " in \"" + setString + "\" value and print elements");
        setElementsList(list, setIndex, setString);
        System.out.println(list);

        System.out.println("\n6. Search element by index " + searchIndex + " and print element");
        System.out.println("Found element \"" + searchByIndexElementsList(list, searchIndex) + "\"");

        System.out.println("\n7. Search element by \"" + searchString + "\" value");
        System.out.println("Found element in index " + searchByValueElementsList(list, searchString));

        System.out.println("\n8.Test iterator");
        for (String element : list) {
            System.out.println(element);
        }
    }

    /**
     * Method for test Parser
     */
    public static void testParser() {
        String expression = "if (stack == 0) {return null}";

        System.out.println("String for parse is \"if (stack == 0) {return null}}\"");

        Parser parser = new Parser();

        if (parser.parse(expression)) {
            System.out.println("Expression contain all pair bracket");
        } else {
            System.out.println("Expression does not contain all pair bracket");
        }

    }

    /**
     * Method for test Tree
     */
    public static void testNodes() {
        StackForNodes stack = new StackForNodes();

        // Adding root node
        TreeNode root = new TreeNode("1");

        // Adding left child and his two children
        TreeNode left = new TreeNode("2");
        left.setLeft(new TreeNode("4"));
        left.setRight(new TreeNode("5"));
        root.setLeft(left);

        // Adding right child and his two children
        TreeNode right = new TreeNode("3");
        right.setLeft(new TreeNode("6"));
        right.setRight(new TreeNode("7"));
        root.setRight(right);

        System.out.println("          1          ");
        System.out.println("       2     3       ");
        System.out.println("     4   5 6   7     ");

        System.out.println("\n");

        // Adding root node in stack
        stack.push(root);

        System.out.print("Result DFS is: ");

        // Depth first search
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            System.out.print(node.getValue() + " ");

            if (node.getLeft() != null) {
                stack.push(node.getLeft());
            }

            if (node.getRight() != null) {
                stack.push(node.getRight());
            }
        }
    }

    /*
     ************************** Methods for testing Vector and List*****************************************************
     */

    /**
     * Fill vector
     *
     * @param vector vector
     * @param count  count elements
     */
    public static void addElementsVector(Vector vector, int count) {
        for (int i = 0; i < count; i++) {
            vector.add("Vector string number " + i);
        }
    }

    /**
     * Test addAt for Vector
     *
     * @param vector   vector
     * @param position index
     * @param string   value
     */
    public static void addAtElementsVector(Vector vector, int position, String string) {
        vector.addAt(position, string);
    }

    /**
     * Test delete for vector
     *
     * @param vector   vector
     * @param position index
     */
    public static void deleteElementsVector(Vector vector, int position) {
        vector.delete(position);
    }

    /**
     * Test set for Vector
     *
     * @param vector   vector
     * @param position index
     * @param string   value
     */
    public static void setElementsVector(Vector vector, int position, String string) {
        vector.set(position, string);
    }

    /**
     * Test searchByIndex for vector
     *
     * @param vector vector
     * @param index  index
     * @return found string
     */
    public static String searchByIndexElementsVector(Vector vector, int index) {
        return vector.searchByIndex(index);
    }

    /**
     * Test searchByValue for vector
     *
     * @param vector vector
     * @param value  value
     * @return index of found element
     */
    public static int searchByValueElementsVector(Vector vector, String value) {
        return vector.searchByValue(value);
    }

    /**
     * Fill List
     *
     * @param list  list
     * @param count count elements
     */
    public static void addElementsList(List list, int count) {
        for (int i = 0; i < count; i++) {
            list.add("List string number " + i);
        }
    }

    /**
     * Test addAt for List
     *
     * @param list     list
     * @param position index
     * @param string   value
     */
    public static void addAtElementsList(List list, int position, String string) {
        list.addAt(position, string);
    }

    /**
     * Test delete for List
     *
     * @param list     list
     * @param position index
     */
    public static void deleteElementsList(List list, int position) {
        list.delete(position);
    }

    /**
     * Test set for List
     *
     * @param list     list
     * @param position index
     * @param string   value
     */
    public static void setElementsList(List list, int position, String string) {
        list.set(position, string);
    }

    /**
     * Test searchByIndex for List
     *
     * @param list  list
     * @param index index
     * @return found string
     */
    public static String searchByIndexElementsList(List list, int index) {
        return list.searchByIndex(index);
    }

    /**
     * Test searchByValue for List
     *
     * @param list  list
     * @param value value
     * @return index of found element
     */
    public static int searchByValueElementsList(List list, String value) {
        return list.searchByValue(value);
    }
}
