package net.javajoy.jps.homework.w06;

import java.util.Iterator;

/**
 * Aleksey checked
 * Classic vector
 */
//AN: Ok, почти все хорошо.
public class Vector implements IStringSequence, Iterable<String> {
    public static final int DEFAULT_CAPACITY = 10;

    private String[] elementData;
    private int capacity;
    private int size;

    /**
     * Constructor with DEFAULT_CAPACITY
     */
    public Vector() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Constructor with initial capacity
     *
     * @param capacity initial capacity
     */
    public Vector(int capacity) {
        if (capacity > 0) {
            this.capacity = capacity;
            this.elementData = new String[capacity];
        } else if (capacity == 0) {
            this.capacity = capacity;
            this.elementData = new String[]{};
        } else {
            throw new IllegalArgumentException("Incorrect capacity");
        }
    }

    /**
     * Iterator
     *
     * @return VectorIterator()
     */
    @Override
    public Iterator<String> iterator() {
        return new VectorIterator();
    }

    /**
     * Inner class-iterator
     */
    private class VectorIterator implements Iterator<String> {
        int index;

        /**
         * Return true if if the iterator has more elements
         *
         * @return true if if the iterator has more elements
         */
        @Override
        public boolean hasNext() {
            return index < size;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return next element
         */
        @Override
        public String next() {
            return elementData[index++];
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator
         */
        @Override
        public void remove() {
            if (index == size) {
                size--;
            } else {
                System.arraycopy(elementData, index + 1, elementData, index, capacity - index - 1);
                size--;
            }
        }
    }

    /**
     * Adds an value to the end
     *
     * @param value string
     */
    @Override
    public void add(String value) {
        if (size < capacity) {
            elementData[size] = value;
        } else {
            capacity = capacity * 3 / 2 + 1;
            String[] newData = new String[capacity];
            System.arraycopy(elementData, 0, newData, 0, size);
            elementData = newData;
            elementData[size] = value;
        }

        size++;
    }

    /**
     * Adds an element in the specified index position
     *
     * @param index index
     * @param value string
     */
    @Override
    public void addAt(int index, String value) {
        if (index >= size || index < 0) {
            throw new IllegalArgumentException("Illegal argument " + index + " size = " + size);
        } else if (index < size && size < capacity) {
            System.arraycopy(elementData, index, elementData, index + 1, capacity - index - 1);
            elementData[index] = value;
        } else {
            capacity = capacity * 3 / 2 + 1;
            String[] newData = new String[capacity];
            System.arraycopy(elementData, 0, newData, 0, size);
            elementData = newData;
            System.arraycopy(elementData, index, elementData, index + 1, capacity - index - 1);
            elementData[index] = value;
        }

        size++;
    }

    /**
     * Deletes an element in the specified index position
     *
     * @param index index
     */
    @Override
    public void delete(int index) {
        if (index >= capacity) {
            throw new IllegalArgumentException(index + " >= " + capacity);
        } else if (index < 0) {
            throw new IllegalArgumentException("Illegal index" + index);
        } else if (index == this.size) {
            this.size--;
        } else {
            System.arraycopy(elementData, index + 1, elementData, index, capacity - index - 1);
            this.size--;
        }
    }

    /**
     * Edits an element in the specified index position
     *
     * @param index index
     * @param value string
     */
    @Override
    public void set(int index, String value) {
        elementData[index] = value;
    }

    /**
     * Search element by index
     *
     * @param index index
     * @return found string
     */
    @Override
    public String searchByIndex(int index) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Illegal index " + index);
        } else {
            return elementData[index];
        }
    }

    /**
     * Search index of string by value
     *
     * @param value string
     * @return index of position, if element is not exist return -1
     */
    @Override
    public int searchByValue(String value) {
        for (int i = 0; i < size; i++) {
            if (elementData[i].equals(value)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Return collection in String format
     *
     * @return string
     */
    @Override
    public String toString() {
        String outputString = "";
        for (int i = 0; i < size; i++) {
            outputString += elementData[i] + "\n";
        }

        return outputString;
    }
}
