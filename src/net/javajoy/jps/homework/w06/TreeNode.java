package net.javajoy.jps.homework.w06;


/**
 * Class is node of binary tree.
 * Contents link to left and right child
 */
public class TreeNode {
    private String value;
    private TreeNode left;
    private TreeNode right;


    /**
     * Constructor with param
     *
     * @param value value of String type
     */
    public TreeNode(String value) {
        this.value = value;
    }

    /**
     * Get value of node
     *
     * @return string
     */
    public String getValue() {
        return value;
    }

    /**
     * Set value for node
     *
     * @param value value of String type
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Get left child
     *
     * @return instance of TreeNode class
     */
    public TreeNode getLeft() {
        return left;
    }

    /**
     * Set left child
     *
     * @param left instance of TreeNode
     */
    public void setLeft(TreeNode left) {
        this.left = left;
    }

    /**
     * Get right childe
     *
     * @return instance of TreeNode
     */
    public TreeNode getRight() {
        return right;
    }

    /**
     * Set right child
     *
     * @param right instance of TreeNode
     */
    public void setRight(TreeNode right) {
        this.right = right;
    }
}
