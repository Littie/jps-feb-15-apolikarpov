package net.javajoy.jps.homework.w06;

/**
 * Class Stack for nodes.
 * Contains nodes
 */
public class StackForNodes {
    public static final int DEFAULT_CAPACITY = 10;

    private TreeNode[] stack;
    private int capacity;
    private int size;
    private int top = -1;

    /**
     * Initial constructor
     */
    public StackForNodes() {
        stack = new TreeNode[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
    }

    /**
     * Returns the last element and removes it from the stack
     *
     * @return instance of TreeNode
     */
    public TreeNode pop() {
        size--;
        return stack[top--];
    }

    /**
     * Add node in last position of stack
     *
     * @param node instance of TreeNode
     */
    public void push(TreeNode node) {
        if (size < capacity) {
            stack[size++] = node;
        } else {
            capacity = capacity * 3 / 2 + 1;
            TreeNode[] newData = new TreeNode[capacity];
            System.arraycopy(stack, 0, newData, 0, size);
            stack = newData;
            stack[size++] = node;
        }

        top++;
    }

    /**
     * Returns the last element from the stack, but does not remove it
     *
     * @return instance of TreeNode
     */
    public TreeNode peek() {
        if (top < 0) {
            System.out.println("Stack is empty");
            return null;
        }

        return stack[top];
    }

    /**
     * Get size of stack
     *
     * @return size of stack
     */
    public int getSize() {
        return top + 1;
    }

    /**
     * Check whether the stack is empty
     *
     * @return true if stack is not empty
     * false if stack is empty
     */
    public boolean isEmpty() {
        return this.top < 0;
    }

}
