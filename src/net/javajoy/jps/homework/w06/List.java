package net.javajoy.jps.homework.w06;

import java.util.Iterator;

/**
 * Aleksey checked
 * Linked List
 */
//AN: здесь вроде бы все хорошо. Молодец
public class List implements IStringSequence, Iterable<String> {
    public Node head;

    /**
     * Iterator
     *
     * @return ListIterator()
     */
    @Override
    //AP: Понял
    public Iterator<String> iterator() {
        return new ListIterator();
    }

    /**
     * Inner class-iterator
     */
    private class ListIterator implements Iterator<String> { //AP: Переименовал
        Node node = head;
        Node prevNode;

        /**
         * Return true if if the iterator has more elements
         *
         * @return true if if the iterator has more elements
         */
        @Override
        public boolean hasNext() {
            return node != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return next element
         */
        @Override
        public String next() {
            String value = node.getValue();
            prevNode = node;
            node = node.next;
            return value;
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator
         */
        @Override
        public void remove() {
            prevNode.next = node.next;
        }
    }

    /**
     * Inner class-node
     */
    public static class Node {
        private String value;
        public Node next = null;

        /**
         * Constructor
         *
         * @param value value of node
         * @param next  link to next node
         */
        public Node(String value, Node next) {
            this.value = value;
            this.next = next;
        }

        /**
         * Return value of node
         *
         * @return value
         */
        public String getValue() {
            return value;
        }

        /**
         * Set value for node
         *
         * @param value string
         */
        private void setValue(String value) {
            this.value = value;
        }
    }

    /**
     * Adds an value to the end
     *
     * @param value string
     */
    @Override
    public void add(String value) {
        if (head == null) {
            head = new Node(value, null);
            return;
        }

        Node node = head;

        while (node.next != null) {
            node = node.next;
        }

        node.next = new Node(value, null);
    }

    /**
     * Adds an element in the specified index position
     *
     * @param index index
     * @param value string
     */
    @Override
    public void addAt(int index, String value) {
        checkIndex(index);

        int k = 0;
        Node node = head;
        Node prevNode = head;

        while (index != k) {
            checkNullOfNode(index, node);
            prevNode = node;
            node = node.next;
            k++;
        }

        prevNode.next = new Node(value, node);
    }

    /**
     * Deletes an element in the specified index position
     *
     * @param index index
     */
    @Override
    public void delete(int index) {
        checkIndex(index);

        int k = 0;
        Node node = head;
        Node prevNode = head;

        while (index != k) {
            checkNullOfNode(index, node);
            prevNode = node;
            node = node.next;
            k++;
        }

        prevNode.next = node.next;
    }

    /**
     * Edits an element in the specified index position
     *
     * @param index index
     * @param value string
     */
    @Override
    public void set(int index, String value) {
        checkIndex(index);

        int k = 0;
        Node node = head;

        while (index != k) {
            checkNullOfNode(index, node);
            node = node.next;
            k++;
        }

        node.setValue(value);
    }

    /**
     * Search element by index
     *
     * @param index index
     * @return found string
     */
    @Override
    public String searchByIndex(int index) {
        checkIndex(index);

        int k = 0;
        Node node = head;

        while (index != k) {
            checkNullOfNode(index, node);
            node = node.next;
            k++;
        }

        return node.getValue();
    }

    /**
     * Search index of string by value
     *
     * @param value string
     * @return index of position
     */
    @Override
    public int searchByValue(String value) {
        int k = 0;
        Node node = head;

        while (node.next != null) {
            if (node.getValue().equals(value)) {
                return k;
            } else {
                node = node.next;
                k++;
            }
        }

        return -1;
    }

    /**
     * Return collection in String format
     *
     * @return string
     */
    @Override
    public String toString() {
        String outputString = "";
        Node node = head;

        while (node != null) {
            outputString += node.getValue() + "\n";
            node = node.next;
        }

        return outputString;
    }

    /**
     * Check bounds of index
     *
     * @param index index
     */
    private void checkIndex(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Illegal index " + index);
        }
    }

    /**
     * Check Node for null
     *
     * @param index index
     * @param node  node
     */
    private void checkNullOfNode(int index, Node node) {
        if (node == null) {
            throw new IndexOutOfBoundsException("Index out of bounds " + index);
        }
    }


}
