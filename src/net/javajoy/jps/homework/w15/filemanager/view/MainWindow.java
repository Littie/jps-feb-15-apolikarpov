package net.javajoy.jps.homework.w15.filemanager.view;

import net.javajoy.jps.homework.w15.filemanager.Operation;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {

    private static final int FIELD_WIDTH = 400;
    private static final int FIELD_HEIGHT = 200;

    private JTextField sourceField;
    private JTextField targetField;
    private JLabel fromLabel;
    private JLabel toLabel;
    private JComboBox<String> operationsList;
    private JLabel informationLabel;
    private JButton doButton;
    private Operation operation;

    public MainWindow() {
        init();
    }

    private void init() {
        setPreferredSize(new Dimension(FIELD_WIDTH, FIELD_HEIGHT));
        initComponents();
        addComponents();
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        sourceField = new JTextField();
        targetField = new JTextField();
        fromLabel = new JLabel("Path from:");
        toLabel = new JLabel("Path to:");
        operationsList = new JComboBox<>(Operation.valuesAsArray());
        informationLabel = new JLabel("");
        informationLabel.setVisible(false);
        doButton = new JButton("Do");
    }

    private void addComponents() {
        setLayout(new GridBagLayout());
        getContentPane().add(fromLabel, new GridBagConstraints(0, 0, 1, 1, 0.1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(6, 6, 0, 6), 0, 0));
        getContentPane().add(sourceField, new GridBagConstraints(1, 0, 1, 1, 0.9, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(6, 6, 0, 6), 0, 0));
        getContentPane().add(toLabel, new GridBagConstraints(0, 1, 1, 1, 0.1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(6, 6, 0, 6), 0, 0));
        getContentPane().add(targetField, new GridBagConstraints(1, 1, 1, 1, 0.9, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(6, 6, 0, 6), 0, 0));
        getContentPane().add(operationsList, new GridBagConstraints(0, 2, 2, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(6, 0, 6, 0), 0, 0));
        getContentPane().add(informationLabel, new GridBagConstraints(0, 3, 2, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(doButton, new GridBagConstraints(0, 4, 2, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(6, 6, 6, 6), 0, 0));
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public JComboBox<String> getOperationsList() {
        return operationsList;
    }

    public JButton getDoButton() {
        return doButton;
    }

    public JTextField getSourceField() {
        return sourceField;
    }

    public JTextField getTargetField() {
        return targetField;
    }

    public JLabel getInformationLabel() {
        return informationLabel;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    //------------------------------------------------------------------------------------------------------------------
}
