package net.javajoy.jps.homework.w15.filemanager;

public enum Operation {

    COPY("Copy"),
    MOVE("Move"),
    DELETE("Delete"),
    SIZE("Size");

    private String name;

    Operation(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public static String[] valuesAsArray() {
        int size = values().length;
        String[] strValues = new String[size];
        for (int i = 0; i < size; i++) {
            strValues[i] = values()[i].getName();
        }
        return strValues;
    }

    public static Operation getByName(String name) {
        for (Operation value : values()) {
            if (value.getName().equals(name)) {
                return value;
            }
        }
        return null;
    }

}
