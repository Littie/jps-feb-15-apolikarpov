package net.javajoy.jps.homework.w15.filemanager.controllers;

import net.javajoy.jps.homework.w15.filemanager.Operation;
import net.javajoy.jps.homework.w15.filemanager.view.MainWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OperationListController implements ActionListener {

    private MainWindow mainWindow;

    public OperationListController(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox operationsList = (JComboBox) e.getSource();
        Operation operation = Operation.getByName((String) operationsList.getSelectedItem());
        mainWindow.setOperation(operation);
    }
}
