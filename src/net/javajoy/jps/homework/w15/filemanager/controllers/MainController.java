package net.javajoy.jps.homework.w15.filemanager.controllers;

import net.javajoy.jps.homework.w15.filemanager.view.MainWindow;

public class MainController {

    private MainWindow mainWindow;

    public MainController(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        initControllers();
    }

    public void initControllers() {
        mainWindow.getDoButton().addActionListener(new DoButtonController(mainWindow));
        mainWindow.getOperationsList().addActionListener(new OperationListController(mainWindow));
    }
}
