package net.javajoy.jps.homework.w15.filemanager.controllers;

import net.javajoy.jps.homework.w15.filemanager.Operation;
import net.javajoy.jps.homework.w15.filemanager.view.MainWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class DoButtonController implements ActionListener {

    private MainWindow mainWindow;
    private JTextField sourceField;
    private JTextField targetField;
    private JLabel informationLabel;
    private long size = 0;

    public DoButtonController(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        this.sourceField = mainWindow.getSourceField();
        this.targetField = mainWindow.getTargetField();
        this.informationLabel = mainWindow.getInformationLabel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Operation operation = mainWindow.getOperation();

        if (operation != null) {

            switch (operation) {
                case COPY:
                    operationWithFile(Operation.COPY);
                    break;
                case MOVE:
                    operationWithFile(Operation.MOVE);
                    break;
                case DELETE:
                    delete();
                    break;
                case SIZE:
                    getSize();
                    break;
            }
        }
    }

    // AP: Вынес в один метод
    private void operationWithFile(Operation operation) {
        File sourceFile = new File(sourceField.getText());
        File targetFile = new File(targetField.getText());

        if (!sourceFile.exists()) {
            informationLabel.setText("File or dir not found");
        } else {
            if (operation == Operation.COPY) {
                copyDirs(sourceFile, targetFile);
                informationLabel.setText("Copy successful");
            } else if (operation == Operation.MOVE) {
                moveDirs(sourceFile, targetFile);
                informationLabel.setText("Move successful");
            }
        }

        informationLabel.setVisible(true);
    }

    private void delete() {
        File file = new File(sourceField.getText());

        int returnValue = JOptionPane.showConfirmDialog(mainWindow, "Are you sure?");

        if (returnValue == 0) {
            if (file.isDirectory()) {
                deleteDirs(file);
                informationLabel.setText("Delete successful");
            } else if (!file.exists()) {
                informationLabel.setText("File not found");
            } else if (!file.delete()) {
                informationLabel.setText("Delete failed");
            } else {
                informationLabel.setText("Delete successful");
            }

            informationLabel.setVisible(true);
        }
    }


    private void getSize() {
        File file = new File(sourceField.getText());

        if (file.isDirectory()) {
            getSizeDirs(file);
            informationLabel.setText(Long.toString(size) + " bytes");
        } else if (file.exists()) {
            informationLabel.setText(Long.toString(file.length()) + " bytes");
        } else {
            informationLabel.setText("File not found");
        }

        informationLabel.setVisible(true);

    }

    private void copyDirs(File sourceFile, File targetFile) {
        if (sourceFile.isDirectory()) {
            if (!targetFile.exists()) {
                targetFile.mkdir();

                // Маленький лог
                System.out.println("Directory " + targetFile + " was created");
            }

            for (String newFile : sourceFile.list()) {
                copyDirs(new File(sourceFile, newFile), new File(targetFile, newFile));
            }
        } else {
            try {
                if (targetFile.isDirectory()) {
                    targetFile = new File(targetFile, sourceFile.getName());
                }

                Files.copy(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                // Маленький лог
                System.out.println("File " + sourceFile + " was copied");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void moveDirs(File sourceFile, File targetFile) {
        if (sourceFile.isDirectory()) {
            if (!targetFile.exists()) {
                targetFile.mkdir();

                // Маленький лог
                System.out.println("Directory " + targetFile + " was created");
            }

            for (String newFile : sourceFile.list()) {
                moveDirs(new File(sourceFile, newFile), new File(targetFile, newFile));
            }
        } else {
            try {
                if (targetFile.isDirectory()) {
                    targetFile = new File(targetFile, sourceFile.getName());
                }

                Files.move(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

                // Маленький лог
                System.out.println("File " + sourceFile + " was moved");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        sourceFile.delete();
    }

    private void deleteDirs(File dir) {
        if (!dir.delete()) {
            for (String dirFile : dir.list()) {
                File f = new File(dir, dirFile);
                deleteDirs(f);
            }

            dir.delete();
        }
    }

    // AS: getter должен возвращать значение.
    private void getSizeDirs(File dir) {
        for (String dirFile : dir.list()) {
            File newFile = new File(dir, dirFile);

            if (newFile.isDirectory()) {
                getSizeDirs(newFile);
            }

            size += newFile.length();
        }
    }
}
