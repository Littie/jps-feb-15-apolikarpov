package net.javajoy.jps.homework.w15.filemanager;


import net.javajoy.jps.homework.w15.filemanager.controllers.MainController;
import net.javajoy.jps.homework.w15.filemanager.view.MainWindow;

public class Main {

    public static void main(String[] args) {
        new MainController(new MainWindow());
    }
}

