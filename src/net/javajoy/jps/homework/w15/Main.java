package net.javajoy.jps.homework.w15;

import net.javajoy.jps.homework.w12.controller.MainController;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

/**
 * 1. В разработанной ранее игре «пазл» реализуйте пункты меню «File - Open», «File - Save» и «File - Save As»,
 * используя сериализацию и JFileChooser.
 * <p>
 * 2. Разработайте программу, которая  позволяет вводить (в текстовые поля) имена файлов или директорий,
 * выбирать операции (из выпадающего списка) и выполнять их:
 * - копировать
 * - переместить
 * - удалить
 * - узнать размер
 * <p>
 * 3*. Реализуйте также операции «сравнить» (по именам и дате создания всех вложенных файлов) и «сравнить по содержимому».
 * Используйте FileVisitor.
 */

public class Main {
    public static void main(String[] args) {
        new MainController(new GameField(), Model.getInstance());
    }
}
