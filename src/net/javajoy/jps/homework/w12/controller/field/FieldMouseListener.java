package net.javajoy.jps.homework.w12.controller.field;

import net.javajoy.jps.homework.w12.model.Cell;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FieldMouseListener extends MouseAdapter {
    private Model model = Model.getInstance();
    private GameField gameField;
    private Cell coordinates;

    public FieldMouseListener(GameField gameField, Cell cell) {
        this.gameField = gameField;
        this.coordinates = cell;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            gameField.displayPopupMenu(e.getXOnScreen(), e.getYOnScreen());
        } else if (model.getSourceCell() == null) {
            gameField.resetCellColor(model.getCurrentPosition());
            gameField.changeCellColor(coordinates);
            model.setCurrentPosition(coordinates);
            model.setSourceCell(coordinates);
        } else {
            model.setCurrentPosition(coordinates);
            model.setTargetCell(coordinates);
            model.swap();
            model.resetSourceAndTargetCells();

            gameField.repaintField();

            if (model.isGameOver()) {
                gameField.showCongratulation();
            }
        }
    }
}
