package net.javajoy.jps.homework.w12.controller.field;

import net.javajoy.jps.homework.w12.model.Cell;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class FieldKeyListener extends KeyAdapter {
    private Model model;
    private GameField gameField;

    public FieldKeyListener(GameField gameField, Model model) {
        this.gameField = gameField;
        this.model = model;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (isArrowKey(e)) {
            arrowKeyPressed(e.getKeyCode());
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            spaceKeyPressed();
        }
    }

    private boolean isArrowKey(KeyEvent keyEvent) {
        int c = keyEvent.getKeyCode();
        return c == KeyEvent.VK_LEFT || c == KeyEvent.VK_RIGHT || c == KeyEvent.VK_DOWN || c == KeyEvent.VK_UP;
    }

    private void arrowKeyPressed(int code) {
        int x = model.getCurrentPosition().getX();
        int y = model.getCurrentPosition().getY();
        int horizontalShift = y;
        int verticalShift = x;

        if (code == KeyEvent.VK_LEFT) {
            horizontalShift = y - 1 >= 0 ? y - 1 : -1;
        } else if (code == KeyEvent.VK_RIGHT) {
            horizontalShift = y + 1 <= 3 ? y + 1 : -1;
        } else if (code == KeyEvent.VK_UP) {
            verticalShift = x - 1 >= 0 ? x - 1 : -1;
        } else if (code == KeyEvent.VK_DOWN) {
            verticalShift = x + 1 <= 3 ? x + 1 : -1;
        }

        Cell shift = new Cell(verticalShift, horizontalShift);

        // Это проверка выхода за пределы поля
        if (horizontalShift == -1 || verticalShift == -1) {
            model.setCurrentPosition(model.getCurrentPosition());
        } else {
            if (!model.isSpacePressed()) {
                gameField.resetCellColor(model.getCurrentPosition());
                model.setCurrentPosition(shift);
                gameField.changeCellColor(shift);
            } else {
                model.setCurrentPosition(shift);
                gameField.changeCellColor(shift);
                model.setPreviousSpacePressed(false);
            }
        }
    }

    private void spaceKeyPressed() {
        if (model.getSourceCell() == null) {
            gameField.changeCellColor(model.getCurrentPosition());
            model.setSourceCell(model.getCurrentPosition());
            model.setPreviousSpacePressed(true);
        } else {
            model.setTargetCellAsCurrentPosition();
            model.setPreviousSpacePressed(false);
            model.swap();
            model.resetSourceAndTargetCells();

            gameField.repaintField();
            if (model.isGameOver()) {
                gameField.showCongratulation();
            }
        }
    }
}
