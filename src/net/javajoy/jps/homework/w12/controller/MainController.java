package net.javajoy.jps.homework.w12.controller;

import net.javajoy.jps.homework.w12.controller.field.FieldKeyListener;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

public class MainController {
    private GameField gameField;
    private Model model;

    public MainController(GameField gameField, Model model) {
        this.gameField = gameField;
        this.model = model;
        initControllers();
    }

    private void initControllers() {
        gameField.addKeyListener(new FieldKeyListener(gameField, model));
    }
}
