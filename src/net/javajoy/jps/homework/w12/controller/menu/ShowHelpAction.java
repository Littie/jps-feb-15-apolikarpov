package net.javajoy.jps.homework.w12.controller.menu;

import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ShowHelpAction extends AbstractAction {

    private final GameField gameField;

    public ShowHelpAction(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(gameField,
                "Left arrow key - move left\n" +
                "Right arrow key - move right\n" +
                "Up arrow key - move up\n" +
                "Down arrow key - move down\n" +
                "Space key - select model\n");
    }
}
