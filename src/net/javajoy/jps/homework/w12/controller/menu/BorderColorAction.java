package net.javajoy.jps.homework.w12.controller.menu;

import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class BorderColorAction extends AbstractAction {
    private GameField gameField;

    public BorderColorAction(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        gameField.setBorderColor(JColorChooser.showDialog(null, "Choose a border color", gameField.getBackgroundColor()));
    }
}
