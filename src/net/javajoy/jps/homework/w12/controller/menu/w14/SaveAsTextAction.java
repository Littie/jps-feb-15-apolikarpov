package net.javajoy.jps.homework.w12.controller.menu.w14;

import net.javajoy.jps.homework.w12.dao.SettingsDao;
import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class SaveAsTextAction extends AbstractAction {
    private SettingsDao setting;
    private GameField gameField;

    public SaveAsTextAction(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        setting = new SettingsDao();
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            setting.saveAsText(gameField.getMainPanel().getBackground(), gameField.getBorderColor());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gameField, "Save settings шт file fail " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
