package net.javajoy.jps.homework.w12.controller.menu.w15;

import net.javajoy.jps.homework.w12.dao.SettingsDao;
import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

public class Save extends AbstractAction {
    private static final String DEFAULT_PATH_TO_FILE = "resources/w15/";
    private static final String DEFAULT_FILE_NAME = "default.pzl";

    private SettingsDao setting;
    private GameField gameField;

    public Save(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        setting = new SettingsDao();
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        File file = new File(DEFAULT_PATH_TO_FILE);

        if (!file.exists()) {
            file.mkdirs();
        }

        try {
            setting.saveAsSerializable(new File(DEFAULT_PATH_TO_FILE + DEFAULT_FILE_NAME));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gameField, "Save settings in file fail " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
