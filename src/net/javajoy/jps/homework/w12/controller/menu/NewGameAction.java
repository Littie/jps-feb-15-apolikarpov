package net.javajoy.jps.homework.w12.controller.menu;

import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class NewGameAction extends AbstractAction {
    private Model model = Model.getInstance();
    private GameField gameField;

    public NewGameAction(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        model.fillModel();
        gameField.repaintField();
    }
}
