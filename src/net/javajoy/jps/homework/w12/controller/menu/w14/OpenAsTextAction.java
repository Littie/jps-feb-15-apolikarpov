package net.javajoy.jps.homework.w12.controller.menu.w14; //AN: пакет лучше назвать action-w14

import net.javajoy.jps.homework.w12.dao.SettingsDao;
import net.javajoy.jps.homework.w12.model.Cell;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class OpenAsTextAction extends AbstractAction {
    private SettingsDao setting;
    private GameField gameField;
    private Model model;

    public OpenAsTextAction(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        this.model = Model.getInstance();
        setting = new SettingsDao();
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<Cell> cells = new ArrayList<>();
        String[] buf;

        try {
            List<String[]> data = setting.loadFromText();

            for (int i = 0; i < model.getFieldSize(); i++) {
                Cell cell = new Cell();
                buf = data.get(i);

                cell.setX(Integer.parseInt(buf[0]));
                cell.setY(Integer.parseInt(buf[1]));
                cell.setSeqNum(Integer.parseInt(buf[2]));
                cell.setImagePath(buf[3]);

                cells.add(cell);
            }

            buf = data.get(model.getFieldSize());
            gameField.getMainPanel().setBackground(new Color(Integer.parseInt(buf[0]), Integer.parseInt(buf[1]), Integer.parseInt(buf[2])));

            buf = data.get(model.getFieldSize() + 1);
            gameField.setBorderColor(new Color(Integer.parseInt(buf[0]), Integer.parseInt(buf[1]), Integer.parseInt(buf[2])));

            model.setCells(cells);
            gameField.repaintField();

            checkGameOver();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gameField, "Load settings from file fail " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    //AP: Не соображу, как можно вынести этот кусок кода из всех опенов. Абстрактный класс уже не получится сделать
    private void checkGameOver() {
        if (model.isGameOver()) {
            gameField.showCongratulation();
        }
    }

}
