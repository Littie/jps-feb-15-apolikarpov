package net.javajoy.jps.homework.w12.controller.menu.w15;

import net.javajoy.jps.homework.w12.dao.SettingsDao;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

// AS: Более информативное имя OpenAction, тогда сразу будет понятно, что класс является экшеном
public class Open extends AbstractAction {
    private SettingsDao setting;
    private GameField gameField;
    private Model model;

    public Open(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        this.model = Model.getInstance();
        setting = new SettingsDao();
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser openFileChooser = new JFileChooser("resources/w15/");
        int result = openFileChooser.showDialog(null, "Open");

        if (result == JFileChooser.APPROVE_OPTION) {
            File fileName = openFileChooser.getSelectedFile();

            loadAndProcessingData(fileName);
        }
    }

    private void loadAndProcessingData(File fileName) {
        try {
            model = (Model) setting.loadSerializable(fileName);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gameField, "Load settings from file fail " + ex.getMessage());
            ex.printStackTrace();
        }

        Model.getInstance().setCells(model.getCells()); //AN: model может быть null, будет NullPointerException
        gameField.repaintField();
        checkGameOver();
    }

    private void checkGameOver() {
        if (model.isGameOver()) {
            gameField.showCongratulation();
        }
    }

}
