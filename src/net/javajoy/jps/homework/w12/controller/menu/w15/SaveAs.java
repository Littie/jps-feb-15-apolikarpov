package net.javajoy.jps.homework.w12.controller.menu.w15;

import net.javajoy.jps.homework.w12.dao.SettingsDao;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

public class SaveAs extends AbstractAction {
    private static final String DEFAULT_OPEN_PATH = "resources/w15/";
    private SettingsDao setting;

    public SaveAs(String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        setting = new SettingsDao();
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        File file = new File(DEFAULT_OPEN_PATH);

        if (!file.exists()) {
            file.mkdirs();
        }

        JFileChooser saveAsFileChooser = new JFileChooser(DEFAULT_OPEN_PATH);
        saveAsFileChooser.setDialogTitle("Save as");
        saveAsFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        int result = saveAsFileChooser.showDialog(null, "Save");

        if (result == JFileChooser.APPROVE_OPTION) {
            File fileName = saveAsFileChooser.getSelectedFile();

            try {
                setting.saveAsSerializable(fileName);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
