package net.javajoy.jps.homework.w12.controller.menu.w14;

import net.javajoy.jps.homework.w12.dao.SettingsDao;
import net.javajoy.jps.homework.w12.model.Cell;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OpenAsBinaryAction extends AbstractAction {
    private SettingsDao setting;
    private GameField gameField;
    private Model model;

    public OpenAsBinaryAction(GameField gameField, String text, ImageIcon icon, String dsc, Integer mnemonic) {
        super(text, icon);
        this.gameField = gameField;
        this.model = Model.getInstance();
        setting = new SettingsDao();
        putValue(SHORT_DESCRIPTION, dsc);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<Cell> cells = new ArrayList<>();
        String[] buf;

        try {
            List<String[]> data = setting.loadFromBinary();

            for (int i = 0; i < model.getFieldSize(); i++) {
                Cell cell = new Cell();
                buf = data.get(i);

                cell.setX(Integer.parseInt(buf[0]));
                cell.setY(Integer.parseInt(buf[1]));
                cell.setSeqNum(Integer.parseInt(buf[2]));
                cell.setImagePath(buf[3]);

                cells.add(cell);
            }

            buf = data.get(data.size() - 1);

            gameField.getMainPanel().setBackground(new Color(Integer.parseInt(buf[0]), Integer.parseInt(buf[1]), Integer.parseInt(buf[2])));
            gameField.setBorderColor(new Color(Integer.parseInt(buf[3]), Integer.parseInt(buf[4]), Integer.parseInt(buf[5])));

            model.setCells(cells);
            gameField.repaintField();

            checkGameOver();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gameField, "Load settings from file fail " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    private void checkGameOver() {
        if (model.isGameOver()) {
            gameField.showCongratulation();
        }
    }

}
