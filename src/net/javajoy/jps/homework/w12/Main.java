package net.javajoy.jps.homework.w12;

import net.javajoy.jps.homework.w12.controller.MainController;
import net.javajoy.jps.homework.w12.model.Model;
import net.javajoy.jps.homework.w12.view.GameField;

public class Main {
    public static void main(String[] args) {
        new MainController(new GameField(), Model.getInstance());
    }
}
