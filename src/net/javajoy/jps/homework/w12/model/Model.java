package net.javajoy.jps.homework.w12.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Model implements Serializable {
    public static final int SIZE = 4;

    private static Model instance;
    private List<Cell> cells;
    private Cell src;
    private Cell dst;
    private Cell currentPosition;
    private boolean spacePressed = false;

    private Model() {
        cells = new ArrayList<>(getFieldSize());
        src = new Cell();
        dst = new Cell();
        fillModel();
        currentPosition = cells.get(0);
    }

    public static Model getInstance() {
        if (instance == null) {
            instance = new Model();
        }

        return instance;
    }

    public void fillModel() {
        int num = 0;

        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                cells.add(num, new Cell(x, y, num));
                num++;
            }
        }

        for (int i = 0; i < getFieldSize(); i++) {
            int a = new Random().nextInt(getFieldSize());
            int tmp = cells.get(i).getSeqNum();
            cells.get(i).setSeqNum(cells.get(a).getSeqNum());
            cells.get(a).setSeqNum(tmp);
        }
    }

    public void swap() {
        int tmp = cells.get(searchIndexOfCell(src)).getSeqNum();
        cells.get(searchIndexOfCell(src)).setSeqNum(cells.get(searchIndexOfCell(dst)).getSeqNum());
        cells.get(searchIndexOfCell(dst)).setSeqNum(tmp);
    }

    private int searchIndexOfCell(Cell cell) {
        for (int i = 0; i < getFieldSize(); i++) {
            if (cells.get(i).getX() == cell.getX() && cells.get(i).getY() == cell.getY()) {
                return i;
            }
        }

        return -1;
    }

    //-----------------------------------------Getters and Setters-----------------------------------------------------

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    public Cell getSourceCell() {
        if (src.isReset()) {
            return null;
        }

        return src;
    }

    public void setSourceCell(Cell cell) {
        src = new Cell(cell);
    }

    public void setTargetCell(Cell cell) {
        dst = new Cell(cell);
    }

    public Cell getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Cell cell) {
        currentPosition = new Cell(cell);
    }

    public void setPreviousSpacePressed(boolean spacePressed) {
        this.spacePressed = spacePressed;
    }

    public boolean isSpacePressed() {
        return spacePressed;
    }

    public void resetSourceAndTargetCells() {
        src.reset();
        dst.reset();
    }

    public void setTargetCellAsCurrentPosition() {
        setTargetCell(currentPosition);
    }

    public int getFieldSize() {
        return SIZE * SIZE;
    }

    //-----------------------------------------------------------------------------------------------------------------

    public boolean isGameOver() {
        List<Cell> fields = getCells();

        int count = 0;

        for (int i = 0; i < getFieldSize(); i++) {
            if (count++ != fields.get(i).getSeqNum()) {
                return false;
            }
        }

        return true;
    }
}
