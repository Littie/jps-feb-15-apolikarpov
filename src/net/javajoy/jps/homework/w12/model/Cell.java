package net.javajoy.jps.homework.w12.model;

import java.io.Serializable;

public class Cell implements Serializable {
    public static final String CELL_IMAGE_PATH_PATTERN = "/w12/images/image%d.jpg";

    private int x = -1;
    private int y = -1;
    private int seqNum;
    private String imagePath;

    public Cell() {
    }

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Cell(int x, int y, int seqNum) {
        this.x = x;
        this.y = y;
        this.seqNum = seqNum;
        setImagePath();
    }

    public Cell(Cell cell) {
        this.x = cell.x;
        this.y = cell.y;
        this.seqNum = cell.seqNum;
        setImagePath();
    }

    private void setImagePath() {
        this.imagePath = String.format(CELL_IMAGE_PATH_PATTERN, this.seqNum);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Cell cell = (Cell) obj;

        return seqNum == cell.seqNum &&
                x == cell.x &&
                y == cell.y &&
                !(imagePath != null
                        ? !imagePath.equals(cell.imagePath)
                        : cell.imagePath != null);

    }

    //-------------------------------------------Getters and Setters----------------------------------------------------

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(int seqNum) {
        this.seqNum = seqNum;
        setImagePath();
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void reset() {
        this.x = -1;
        this.y = -1;
    }

    public boolean isReset() {
        return this.x == -1 && this.y == -1;
    }

    //------------------------------------------------------------------------------------------------------------------
}
