package net.javajoy.jps.homework.w12.dao;

import net.javajoy.jps.homework.w12.model.Cell;
import net.javajoy.jps.homework.w12.model.Model;

import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SettingsDao {
    private static final String PATH_TO_BINARY = "resources/w14";
    private static final String BINARY_FILE = "/binary.pzl";
    private static final String PATH_TO_TEXT = "resources/w14";
    private static final String TEXT_FILE = "/text.pzl";

    private Model model;

    public SettingsDao() {
        this.model = Model.getInstance();
    }

    public List<String[]> loadFromBinary() throws IOException {

        final int BUF_SIZE_FOR_CELL = 4;
        final int BUF_SIZE_FOR_COLOR = 6;
        List<String[]> data = new ArrayList<>();

        try (DataInputStream inputData = new DataInputStream(new FileInputStream(PATH_TO_BINARY + BINARY_FILE))) {

            // Здесь загоняем данные только для Cell
            for (int i = 0; i < model.getFieldSize(); i++) {
                String buf[] = new String[BUF_SIZE_FOR_CELL];

                for (int j = 0; j < 3; j++) {
                    buf[j] = Integer.toString(inputData.readInt());
                }

                byte[] path = new byte[inputData.readInt()];
                int n = inputData.read(path);
                buf[buf.length - 1] = new String(path, 0, n);

                data.add(buf);

            }

            String[] buf = new String[BUF_SIZE_FOR_COLOR];

            // Здесь добавляем цвета
            for (int i = 0; i < buf.length; i++) {
                buf[i] = Integer.toString(inputData.readInt());
            }

            data.add(buf);
        }

        return data;
    }

    public List<String[]> loadFromText() throws IOException {
        List<String[]> data = new ArrayList<>(model.getFieldSize());

        try (BufferedReader inputData = new BufferedReader(new FileReader(PATH_TO_TEXT + TEXT_FILE))) {

            String[] buf;
            String line;

            while ((line = inputData.readLine()) != null) {
                buf = line.split("[|]");
                data.add(buf);
            }
        }

        return data;
    }

    public Object loadSerializable(File fileName) throws IOException {
        Object model = null;

        try (ObjectInputStream inputData = new ObjectInputStream(new FileInputStream(fileName))) {
            model = inputData.readObject();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return model;
    }

    public void saveAsBinary(Color backgroundColor, Color borderColor) throws IOException {
        List<Cell> cells = Model.getInstance().getCells();
        File fileToBinary = new File(PATH_TO_BINARY);
        if (!fileToBinary.exists()) {
            fileToBinary.mkdirs();
        }


        try (DataOutputStream outData = new DataOutputStream(new FileOutputStream(PATH_TO_BINARY + BINARY_FILE))) {
            for (Cell cell : cells) {
                outData.writeInt(cell.getX());
                outData.writeInt(cell.getY());
                outData.writeInt(cell.getSeqNum());
                outData.writeInt(cell.getImagePath().getBytes().length);
                outData.write(cell.getImagePath().getBytes());
            }

            outData.writeInt(backgroundColor.getRed());
            outData.writeInt(backgroundColor.getGreen());
            outData.writeInt(backgroundColor.getBlue());

            outData.writeInt(borderColor.getRed());
            outData.writeInt(borderColor.getGreen());
            outData.writeInt(borderColor.getBlue());
        }
    }

    public void saveAsText(Color backgroundColor, Color borderColor) throws IOException {
        List<Cell> cells = Model.getInstance().getCells();
        File fileToText = new File(PATH_TO_TEXT);
        if (!fileToText.exists()) {
            fileToText.mkdirs();
        }


        try (BufferedWriter outData = new BufferedWriter(new FileWriter(PATH_TO_TEXT + TEXT_FILE))) {

            for (Cell cell : cells) {
                outData.write(cell.getX() + "|");
                outData.write(cell.getY() + "|");
                outData.write(cell.getSeqNum() + "|");
                outData.write(cell.getImagePath() + System.lineSeparator());
            }

            outData.write(backgroundColor.getRed() + "|");
            outData.write(backgroundColor.getGreen() + "|");
            outData.write(backgroundColor.getBlue() + System.lineSeparator());

            outData.write(borderColor.getRed() + "|");
            outData.write(borderColor.getGreen() + "|");
            outData.write(borderColor.getBlue() + System.lineSeparator());

        }
    }

    public void saveAsSerializable(File fileName) throws IOException {
        try (ObjectOutputStream outData = new ObjectOutputStream(new FileOutputStream(fileName))) {
            outData.writeObject(model);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
