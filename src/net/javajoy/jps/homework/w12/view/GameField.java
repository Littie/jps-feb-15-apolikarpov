package net.javajoy.jps.homework.w12.view;

import net.javajoy.jps.homework.w12.controller.field.FieldMouseListener;
import net.javajoy.jps.homework.w12.controller.menu.BackgroundColorAction;
import net.javajoy.jps.homework.w12.controller.menu.BorderColorAction;
import net.javajoy.jps.homework.w12.controller.menu.NewGameAction;
import net.javajoy.jps.homework.w12.controller.menu.ShowHelpAction;
import net.javajoy.jps.homework.w12.controller.menu.w14.OpenAsBinaryAction;
import net.javajoy.jps.homework.w12.controller.menu.w14.OpenAsTextAction;
import net.javajoy.jps.homework.w12.controller.menu.w14.SaveAsBinaryAction;
import net.javajoy.jps.homework.w12.controller.menu.w14.SaveAsTextAction;
import net.javajoy.jps.homework.w12.controller.menu.w15.Open;
import net.javajoy.jps.homework.w12.controller.menu.w15.Save;
import net.javajoy.jps.homework.w12.controller.menu.w15.SaveAs;
import net.javajoy.jps.homework.w12.model.Cell;
import net.javajoy.jps.homework.w12.model.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;
import java.util.List;

public class GameField extends JFrame {
    public static final int FIELD_WIDTH = 320;
    public static final int FIELD_HEIGHT = 380;
    public static final int CELL_WIDTH = 80;
    public static final int CELL_HEIGHT = 80;
    public static final int HGAP = 1;
    public static final int VGAP = 1;

    private JPanel mainPanel;
    private JLabel cells[][] = new JLabel[Model.SIZE][Model.SIZE];
    private List<Cell> fields = Model.getInstance().getCells();
    private Color backgroundColor = Color.GRAY;
    private Color borderColor = Color.RED;

    private JMenuBar menuBar = new JMenuBar();
    private JPopupMenu popupMenu = new JPopupMenu();
    private JToolBar toolBar = new JToolBar(JToolBar.HORIZONTAL);


    public GameField() {
        init();
    }

    public void init() {
        setLayout(new BorderLayout());
        setTitle("PUZZLE");
        setPreferredSize(new Dimension(FIELD_WIDTH, FIELD_HEIGHT));
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        addPanels();
        setVisible(true);
    }

    private void addPanels() {
        addMenuBar();
        addPopup();
        addToolBar();
        initMainPanel();
        fillMainPanel();
        add(mainPanel, BorderLayout.CENTER);
        setFocusable(true);
    }

    private void initMainPanel() {
        GridLayout layout = new GridLayout(Model.SIZE, Model.SIZE);
        layout.setHgap(HGAP);
        layout.setVgap(VGAP);
        mainPanel = new JPanel(layout);
        mainPanel.setBackground(backgroundColor);
    }

    private void addMenuBar() {
        JMenu file = new JMenu("File");

        JMenuItem newGame = new JMenuItem(new NewGameAction(this, "New game", new ImageIcon(getClass().getResource("/w13/new.png")), "Start new game", KeyEvent.VK_N));
        JMenuItem backgroundColor = new JMenuItem(new BackgroundColorAction(this, "Background color", new ImageIcon(getClass().getResource("/w13/bkcolor.png")), "Set background color", KeyEvent.VK_C));
        JMenuItem borderColor = new JMenuItem(new BorderColorAction(this, "Border color", new ImageIcon(getClass().getResource("/w13/brcolor.png")), "Set border color", KeyEvent.VK_B));

        file.add(newGame);
        file.add(backgroundColor);
        file.add(borderColor);

        JMenu w14 = new JMenu("W14"); //AP: Конечно. Делал исключительно для удобства.

        //AN: null вместо ImageIcon
        JMenuItem openInBinary = new JMenuItem(new OpenAsBinaryAction(this, "Open in binary", null, "Open in binary", KeyEvent.VK_O));
        JMenuItem openInText = new JMenuItem(new OpenAsTextAction(this, "Open in text", null, "Open in text", KeyEvent.VK_Q));
        JMenuItem saveInBinary = new JMenuItem(new SaveAsBinaryAction(this, "Save in binary", null, "Save file in binary", KeyEvent.VK_S));
        JMenuItem saveInText = new JMenuItem(new SaveAsTextAction(this, "Save in text", null, "Save file in text", KeyEvent.VK_T));

        w14.add(openInBinary);
        w14.add(openInText);
        w14.add(saveInBinary);
        w14.add(saveInText);

        JMenu w15 = new JMenu("W15");

        JMenuItem open = new JMenuItem(new Open(this, "Open", null, "Open file", KeyEvent.VK_P));
        JMenuItem save = new JMenuItem(new Save(this, "Save", null, "Save file", KeyEvent.VK_L));
        JMenuItem saveAs = new JMenuItem(new SaveAs("Save as", null, "Save as file", KeyEvent.VK_K));

        w15.add(open);
        w15.add(save);
        w15.add(saveAs);


        JMenu help = new JMenu("Help");
        JMenuItem f1 = new JMenuItem(new ShowHelpAction(this, "F1", new ImageIcon(""), "Help", KeyEvent.VK_F));
        help.add(f1);

        menuBar.add(file);
        menuBar.add(w14);
        menuBar.add(w15);
        menuBar.add(help);

        setJMenuBar(menuBar);
    }

    private void addPopup() {
        popupMenu.add(new NewGameAction(this, "New game", new ImageIcon(getClass().getResource("/w13/new.png")), "Start new game", KeyEvent.VK_N));
        popupMenu.add(new BackgroundColorAction(this, "Background color", new ImageIcon(getClass().getResource("/w13/bkcolor.png")), "Set background color", KeyEvent.VK_C));
        popupMenu.add(new BorderColorAction(this, "Border color", new ImageIcon(getClass().getResource("/w13/brcolor.png")), "Set border color", KeyEvent.VK_B));
    }

    private void addToolBar() {
        toolBar.add(new NewGameAction(this, "New game", new ImageIcon(getClass().getResource("/w13/new.png")), "Start new game", KeyEvent.VK_N));
        toolBar.add(new BackgroundColorAction(this, "Background color", new ImageIcon(getClass().getResource("/w13/bkcolor.png")), "Set background color", KeyEvent.VK_C));
        toolBar.add(new BorderColorAction(this, "Border color", new ImageIcon(getClass().getResource("/w13/brcolor.png")), "Set border color", KeyEvent.VK_B));
        add(toolBar, BorderLayout.NORTH);
        toolBar.setVisible(true);
    }

    public void fillMainPanel() {
        for (int i = 0; i < Model.SIZE * Model.SIZE; i++) {
            mainPanel.add(createCell(fields.get(i)));
        }

        cells[0][0].setBorder(BorderFactory.createLineBorder(Color.RED));
    }

    private JLabel createCell(Cell cell) {
        int x = cell.getX();
        int y = cell.getY();
        //AP: Изменил возвращаемый imagePath в Cell(), стало чуть короче, не надо реплейсить
        URL url = getClass().getResource(cell.getImagePath());

        cells[x][y] = new JLabel(new ImageIcon(url));

        cells[x][y].setHorizontalAlignment(JLabel.CENTER);
        cells[x][y].setMaximumSize(new Dimension(CELL_WIDTH, CELL_HEIGHT));
        cells[x][y].setComponentPopupMenu(popupMenu);

        cells[x][y].addMouseListener(new FieldMouseListener(this, cell));

        return cells[x][y];
    }

    public void repaintField() {
        int num = 0;
        fields = Model.getInstance().getCells();

        for (int x = 0; x < Model.SIZE; x++) {
            for (int y = 0; y < Model.SIZE; y++) {
                mainPanel.remove(cells[x][y]);
                mainPanel.add(createCell(fields.get(num++)));
            }
        }

        validate();
        repaint();
    }

    public void changeCellColor(Cell cell) {
        cells[cell.getX()][cell.getY()].setBorder(BorderFactory.createLineBorder(borderColor));
    }

    public void resetCellColor(Cell cell) {
        cells[cell.getX()][cell.getY()].setBorder(null);
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBorderColor(Color color) {
        this.borderColor = color;
    }

    public Color getBorderColor() {
        return this.borderColor;
    }

    public void displayPopupMenu(int x, int y) {
        popupMenu.setLocation(x, y);
        popupMenu.setVisible(true);
    }

    public void showCongratulation() {
        JOptionPane.showMessageDialog(this, "Congratulation. You won!");
    }
}
























