package net.javajoy.jps.homework.w05;

/**
 * Class Container contains array of strings.
 */
public class Container {
    public static final String[] EMPTY_ELEMENT_DATA = {};

    private String[] elementData;
    private Condition condition;
    private int capacity;
    private int size;

    /**
     * Constructs an empty list with the specified initial capacity.
     *
     * @param initialCapacity the initial capacity of the list
     * @throws IllegalArgumentException if the specified initial capacity
     *                                  is negative
     */
    public Container(int initialCapacity) {
        if (initialCapacity > 0) {
            this.elementData = new String[initialCapacity];
            this.capacity = elementData.length;
        } else if (initialCapacity == 0) {
            this.elementData = EMPTY_ELEMENT_DATA;
            this.capacity = EMPTY_ELEMENT_DATA.length;
        } else {
            throw new IllegalArgumentException("Illegal capacity" + initialCapacity);
        }
    }


    /**
     * Create iterator for elementData
     *
     * @return new instance of iterator
     */
    public Iterator iterator() {
        return new Iterator();
    }

    /**
     * Add new string to elementData.
     * Element added to the last position of elementData
     *
     * @param string string
     */
    public void add(String string) {
        if (size < capacity) {
            elementData[size++] = string;
        } else {
            String[] tmpContainer = new String[this.capacity * 3 / 2 + 1];
            System.arraycopy(this.elementData, 0, tmpContainer, 0, capacity);
            tmpContainer[size++] = string;
            this.elementData = tmpContainer;
            this.capacity = elementData.length;
        }
    }

    /**
     * Edit string in index position
     *
     * @param index    number of position
     * @param string editing string
     */
    public void set(int index, String string) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Illegal index " + index);
        } else {
            elementData[index] = string;
        }
    }

    /**
     * Delete string in index position
     *
     * @param index number of position
     */
    public void delete(int index) {
        if (index >= capacity) {
            throw new IllegalArgumentException(index + " >= " + capacity);
        } else if (index < 0) {
            throw new IllegalArgumentException("Illegal index" + index);
        } else if (index == this.size) {
            this.size--;
        } else {
            System.arraycopy(elementData, index + 1, elementData, index, capacity - index - 1);
            this.size--;
        }
    }

    /**
     * Set condition in elementData
     *
     * @param condition instance of Condition interface
     */
    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    /**
     * Print all elements from elementData
     */
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < size; i++) {
            out.append(String.format("String %d: %s\n", i, elementData[i]));
        }

        return out.toString();
    }

    /**
     * Get size of container
     *
     * @return size of container
     */
    public int getSize() {
        return size;
    }


    /**
     * Class iterator
     */
    public class Iterator {
        private int index;

        /**
         * Returns the element satisfies the condition
         *
         * @return true, if cursor is within of array and satisfies the condition
         * false, if cursor is outside of array
         */
        public boolean hasNext() {
            for (int i = index; i < size; i++) {
                if (index < size && condition.check(elementData[index])) {
                    return true;
                }
                index++;
            }
            return false;
        }

        /**
         * Print next element if it satisfies the condition
         */
        public String next() {
            if (index < size) {
                return elementData[index++];
            } else {
                return null;
            }
        }
    }
}
