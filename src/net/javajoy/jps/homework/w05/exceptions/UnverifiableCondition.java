package net.javajoy.jps.homework.w05.exceptions;

/**
 * Throw exception when condition can not be verified
 */
public class UnverifiableCondition extends Exception {
    /**
     * Constructor call super
     */
    public UnverifiableCondition() {
        super("Condition can not be verified");
    }
}
