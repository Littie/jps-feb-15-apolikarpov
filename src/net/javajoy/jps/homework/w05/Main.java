package net.javajoy.jps.homework.w05;

/**
 * Checks the work of Container class
 */
public class Main {

    public static final int REQUIRED_LENGTH = 15;
    public static final int UNICODE_CHARACTER_LOWERCASE_M = 109;

    /**
     * Entry point
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        main(5, 6, 5, 2);
    }

    /**
     * Basic test method
     */
    public static void main(int containerCapacity, int countElements, int editElement, int deleteElement) {
        // Creating container
        System.out.println("1. Creating container of the " + containerCapacity + " elements");
        Container container = new Container(containerCapacity);

        // Adding six elements
        System.out.println("2. Adding " + countElements + " elements");
        addDemoElements(container);

        // Print current container
        System.out.println("\n3. Print current container");
        System.out.println(container);

        // Edit elements
        System.out.println("4. Editing element with index " + editElement + " and print container");
        editElements(container, editElement);

        // Delete elements
        System.out.println("5. Deleting element with index " + deleteElement + " and print container");
        deleteElements(container, deleteElement);

        // Checking setConditionLength condition
        System.out.println("\n6. Checking setConditionLength condition and print result");
        setConditionLength(container, REQUIRED_LENGTH);

        // Checking setConditionIsCharacter condition
        System.out.println("\n7. Checking setConditionIsCharacter condition and print result");
        setConditionIsCharset(container, UNICODE_CHARACTER_LOWERCASE_M);
    }


    /**
     * Sets the condition to check the length of the string
     *
     * @param container container of strings
     * @param length    length of string
     */
    public static void setConditionLength(Container container, final int length) {
        Container.Iterator iterator = container.iterator();

        container.setCondition(new Condition() {
            @Override
            public boolean check(String string) {
                return string.length() > length;
            }
        });

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    /**
     * Sets the condition to check for a character in a string
     *
     * @param container container of strings
     * @param character character in decimal format
     */
    public static void setConditionIsCharset(Container container, final int character) {
        Container.Iterator iterator = container.iterator();

        container.setCondition(new Condition() {
            @Override
            public boolean check(String string) {
                int ch = string.indexOf(character);

                if (character < 32 || character > 126) {
                    throw new IllegalArgumentException("Illegal character " + (char) character);
                } else {
                    return ch != -1;
                }
            }
        });

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    /**
     * Add elements in container
     *
     * @param container container of elements
     */
    public static void addDemoElements(Container container) {
        // Adding element to container
        container.add("My name is Artem");
        container.add("It's my life");
        container.add("Never give up");
        container.add("Show must go on");
        container.add("We are the champions");
        container.add("This is sixth element");
    }

    /**
     * Edit element in container
     *
     * @param container container of strings
     */
    public static void editElements(Container container, int index) {
        container.set(index, "YYYY");
        System.out.println(container);
    }

    /**
     * Delete element in container
     *
     * @param container container of strings
     */
    public static void deleteElements(Container container, int index) {
        container.delete(index);
        System.out.println(container);
    }
}
