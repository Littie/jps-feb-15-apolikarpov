package net.javajoy.jps.homework.w05;

public interface Condition {

    /**
     * Method check condition
     *
     * @return true if condition is true
     *         false if condition is false
     */
    boolean check(String string);
}
