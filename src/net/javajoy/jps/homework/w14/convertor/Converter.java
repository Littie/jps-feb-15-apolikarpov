package net.javajoy.jps.homework.w14.convertor;

import java.io.*;

public class Converter {
    public static final String UTF = "utf-8";
    public static final String DOS = "cp866";
    public static final String CP1251 = "cp1251";

    private static final int BUFF_SIZE = 1024;

    private File file;

    public Converter(File file) {
        this.file = file;
    }

    public void convert(String coding) throws IOException {
        switch (coding) {
            case UTF:
                convertTo(coding);
                break;
            case DOS:
                convertTo(coding);
                break;
            case CP1251:
                convertTo(coding);
                break;
            default:
                throw new IllegalArgumentException("Incorrect input charset");
        }

    }

    private void convertTo(String coding) throws IOException {
        char[] buff = new char[BUFF_SIZE];
        int count;

        try (BufferedReader inputFile = new BufferedReader(new FileReader(file.getPath()))) {
            count = inputFile.read(buff);
        }

        try (BufferedWriter outFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), coding))) {
            outFile.write(buff, 0, count);
        }
    }
}
