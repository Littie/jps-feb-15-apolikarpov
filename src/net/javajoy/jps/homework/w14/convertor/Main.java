package net.javajoy.jps.homework.w14.convertor;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        File file = new File("resources/w14/code.txt");
        Converter con = new Converter(file);

        con.convert(Converter.CP1251);
    }
}
