package net.javajoy.jps.homework.w07.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Matrix {
    public static final int MATRIX_MAX_ELEMENT_VALUE = 10;

    private List<List<Integer>> rows = new ArrayList<>();
    private int sizeRow;
    private int sizeColumns;

    /**
     * Constructor with two parameters
     *
     * @param rows    number of rows
     * @param columns number of columns
     */
    public Matrix(int rows, int columns) {
        this.sizeRow = rows;
        this.sizeColumns = columns;
        fillMatrix();
    }

    /**
     * Multiply matrix. Return resulting matrix.
     * if the number of columns of the matrix is not equal to the number of
     * rows matrix two, return null
     *
     * @param matrix second matrix
     * @return resulting matrix
     */
    public Matrix multiply(Matrix matrix) {
        Matrix resultMatrix = new Matrix(sizeRow, sizeColumns);

        if (this.sizeColumns != matrix.sizeRow) {
            return null;
        }

        for (int row = 0; row < rows.size(); row++) {
            List<Integer> columnMatrix = new ArrayList<>();

            for (int column = 0; column < matrix.rows.get(row).size(); column++) {
                int sum = 0;

                for (int k = 0; k < matrix.rows.size(); k++) {
                    sum += this.rows.get(row).get(k) * matrix.rows.get(k).get(column);
                }

                columnMatrix.add(sum);
            }

            resultMatrix.rows.add(columnMatrix);
        }

        return resultMatrix;
    }

    /**
     * Fill matrix random int value from 0 to 10
     */
    private void fillMatrix() {
        Random rnd = new Random();

        for (int i = 0; i < sizeRow; i++) {
            List<Integer> column = new ArrayList<>();

            for (int j = 0; j < sizeColumns; j++) {
                column.add(rnd.nextInt(MATRIX_MAX_ELEMENT_VALUE));
            }

            rows.add(column);
        }
    }

    /**
     * Print matrix
     */
    @Override
    public String toString() {
        String resultString = "";

        for (List<Integer> row : rows) {
            resultString += Arrays.toString(row.toArray()) + "\n";
        }

        return resultString;
    }
}