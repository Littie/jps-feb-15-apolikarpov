package net.javajoy.jps.homework.w07;

import net.javajoy.jps.homework.w07.matrix.Matrix;
import net.javajoy.jps.homework.w07.parser.ExpressionVerifier;
import net.javajoy.jps.homework.w07.tree.TreeNode;

import java.util.ArrayDeque;
import java.util.Deque;

public class Main {

    public static final String DEPTH_FIRST_SEARCH = "DFS";
    public static final String BREADTH_FIRST_SEARCH = "BFS";

    public static void main(String[] args) {
        run();
    }

    public static void run() {
        System.out.println("=========================================================================================");
        System.out.println("==                            TESTING PARSER                                           ==");
        System.out.println("=========================================================================================");
        testParser();

        System.out.println("\n=========================================================================================");
        System.out.println("==                            TESTING DFS                                              ==");
        System.out.println("=========================================================================================");
        testSearch(DEPTH_FIRST_SEARCH);

        System.out.println("\n=========================================================================================");
        System.out.println("==                            TESTING BFS                                              ==");
        System.out.println("=========================================================================================");
        testSearch(BREADTH_FIRST_SEARCH);


        System.out.println("\n=========================================================================================");
        System.out.println("==                            TESTING MATRIX                                           ==");
        System.out.println("=========================================================================================");
        testMatrix();
    }

    /**
     * Method test parser (Task 1)
     */
    public static void testParser() {
        ExpressionVerifier expressionVerifier = new ExpressionVerifier();
        String expression = "if (a > 0) {a = 0} else {a < 0";

        System.out.println("Current expression is \"" + expression + "\" ");

        if (expressionVerifier.verify(expression)) {
            System.out.println("Expression contain all pair bracket");
        } else {
            System.out.println("Expression does not contain all pair bracket");
        }
    }


    /**
     * Matrix test method of search (Task 2)
     * Available two options: BFS - breath-first search
     * and DFS - depth-search first
     *
     * If input incorrect options, then trows IllegalArgumentException
     */
    public static void testSearch(String method) {
        Deque<TreeNode> list = new ArrayDeque<>();

        // Adding root node
        TreeNode root = new TreeNode("1");

        // Adding left child and his two children
        TreeNode left = new TreeNode("2");
        left.setLeft(new TreeNode("4"));
        left.setRight(new TreeNode("5"));
        root.setLeft(left);

        // Adding right child and his two children
        TreeNode right = new TreeNode("3");
        right.setLeft(new TreeNode("6"));
        right.setRight(new TreeNode("7"));
        root.setRight(right);

        System.out.println("Current Tree:");

        System.out.println("          1          ");
        System.out.println("       2     3       ");
        System.out.println("     4   5 6   7     \n");

        list.add(root);

        System.out.print("Result of search: ");

        while (!list.isEmpty()) {
            TreeNode node;

            switch (method) {
                case BREADTH_FIRST_SEARCH:
                    node = list.poll();
                    break;
                case DEPTH_FIRST_SEARCH:
                    node = list.pollLast();
                    break;
                default:
                    throw new IllegalArgumentException("Illegal argument" + method);
            }

            System.out.print(node.getValue() + " ");

            if (node.getLeft() != null) {
                list.add(node.getLeft());
            }

            if (node.getRight() != null) {
                list.add(node.getRight());
            }
        }

    }

    /**
     * Method test multiply matrix one and matrix two (Task 3)
     */
    public static void testMatrix() {
        Matrix leftMatrix = new Matrix(3, 3);
        Matrix rightMatrix = new Matrix(3, 3);

        System.out.println("Matrix one is: \n" + leftMatrix.toString());
        System.out.println("Matrix two is: \n" + leftMatrix.toString());

        System.out.println("Result of multiply matrixOne and MatrixTwo is: \n" + leftMatrix.multiply(rightMatrix));
    }
}
