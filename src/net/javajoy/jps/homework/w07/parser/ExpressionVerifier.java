package net.javajoy.jps.homework.w07.parser;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Parser, which uses Deque for organization stack
 */
public class ExpressionVerifier {
    public static final String KEY = "{}()";

    private Deque<String> stack;

    /**
     * Constructor with one parameter
     */
    public ExpressionVerifier() {
        stack = new ArrayDeque<>();
    }

    /**
     * Parse string
     *
     * @param expression of String type
     */
    public boolean verify(String expression) {
        for (char c : expression.toCharArray()) {
            if (isBracket(c)) {
                if (stack.size() != 0 && isPairBracket(stack.peek(), String.valueOf(c))) {
                    stack.poll();
                } else {
                    stack.add(String.valueOf(c));
                }
            }
        }

        return stack.isEmpty();
    }

    /**
     * Checks whether paired brackets
     *
     * @param openBracket  open brackets
     * @param closeBracket close brackets
     * @return true if brackets are paired
     * false if brackets are not paired
     */
    private static boolean isPairBracket(String openBracket, String closeBracket) {
        return openBracket.equals("(") && closeBracket.equals(")") || openBracket.equals("{") && closeBracket.equals("}");
    }

    /**
     * Check whether character is a bracket
     *
     * @param character character
     * @return true if character is bracket
     * false if character is not bracket
     */
    private static boolean isBracket(char character) {
        return KEY.contains(String.valueOf(character));
    }
}
