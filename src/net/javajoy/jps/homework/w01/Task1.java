package net.javajoy.jps.homework.w01;
//AN: нужно будет придерживать такой структуру пакетов. Помимо homework, будет еще exam

/**
 * 1. Вычислить сумму чисел от 1 до 100.
 * <p/>
 * 2. В ходе вычисления выводить промежуточную сумму каждые 10 элементов, сохранить суммы в массив.
 * <p/>
 * 3. Упорядочить массив промежуточных сумм по убыванию и вывести в консоль
 * Результат вывода:
 * 1. Сумма чисел 1..100
 * 2. Промежуточные суммы
 * 3. Упорядоченный массив
 */
//AN: в целом, все хорошо!!!
public class Task1 {

    public static final int INTERMEDIATE_SUM_INDEX = 10; //AN: желательн выводить значения в константы. 2 плюса: им можно дать смысловые имена, их не нужно искать в самом, так как размещают в блоке с полями класса
    public static final int RANGE = 100;

    public static void main(String[] args) {
        int[] arr = new int[RANGE / INTERMEDIATE_SUM_INDEX];

        printSum(arr);    //AN: можно было бы назвать printIntermediateSums либо printSumOn - и передать в него INTERMEDIATE_SUM_INDEX переменную, для конкретики
        sortArray(arr); //AN: метод лучше назвать sortArray - то есть what to do - to sort (инфинитив, глагол не опрделенной формы). sorted - это уже adjective (прилагательное), и можно перевести как - отсортированный
        printArray(arr);
    }

    /*
     *  Подсчет суммы от 1 до RANGE и вывод
     *  промежуточных результатов
    */
    public static void printSum(int[] array) {
        int sum = 0;
        int k = 0;

        System.out.println("Промежуточные результаты:");

        for (int i = 1; i <= RANGE; i++) {
            sum += i;
            if (i % INTERMEDIATE_SUM_INDEX == 0) {
                System.out.print(sum + " ");
                array[k] = sum;
                k++;
            }
        }

        System.out.println("\nСумма чисел от 1 до " + RANGE + " : " + sum);
    }

    //AN: пользуйтесь форматированием кода: Ctrl+Alt+L
    // Сортировка массива в порядке убывания
    public static void sortArray(int[] array) { //AP: Исправил на public
        System.out.println("Отсортированный в порядке убывания массив:");

        for (int i = 1; i < array.length; i++) {
            for (int j = i; j > 0 && (array[j] > array[j - 1]); j--) {
                int tmp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = tmp;
            }
        }
    }

    // Вывод массива
    public static void printArray(int[] array) {
        for (int element : array) { //AP: изенил на element
            System.out.print(element + " ");
        }
    }
}