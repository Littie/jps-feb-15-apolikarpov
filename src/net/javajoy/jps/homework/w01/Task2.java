package net.javajoy.jps.homework.w01;

/**
 * Создайте  массив  из  всех  нечётных  чисел  от  3  до  35  и  выведите  элементы  массива
 * на  экран  сначала  в  строку,  отделяя  один  элемент  от  другого  пробелом,  а  затем  в
 * столбик  (отделяя  один  элемент  от  другого  началом  новой  строки).  Перед
 * созданием  массива  подумайте, какого  он  будет  размера.
 */

public class Task2 {
    // private static final int SIZE = 17; // AP: Не знаю, как лучше делать, через константу или через метод, в котором можно задать диапазон
    // Метод кажется лучше, т.к. можно указывать разные диапазоны, но константа подходит под условие задачи

    public static void main(String[] args) {
        int[] array = new int[getElementCount(3, 35)];
        createArray(array, 3, 35);
        printArray(array, " "); //AP: Переделал метод, теперь можно выбирать перевод строки
        printArray(array, "\n");
    }

    /**
     * Возвращает количество четных или нечетных элементов заданных в диапазоне
     * от start до end.
     *
     * @param start начало диапазона
     * @param end   конец диапазона
     * @return - количество нечетных элементов
     */
    public static int getElementCount(int start, int end) { //AP: Переименовал название на правильное, подогнал под условие задачи, теперь ищем только нечет
        int count = 0; //AP: Переименовал

        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) count++;
        }

        return count;
    }

    /**
     * Создает массив состоящий из четных или нечетных элементов
     * в диапазоне от start до end.
     *
     * @param array исходный массив
     */
    public static void createArray(int[] array, int start, int end) { //AP: Заменил на "создает" в самом описании
        int k = 0;

        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                array[k] = i;
                k++;
            }
        }
    }

    /**
     * Печатает массив элементов
     *
     * @param array   исходный массив
     * @param newLine способ перевода строки
     */
    public static void printArray(int[] array, String newLine) {
        for (int element : array) {
            System.out.print(element + newLine);
        }
        System.out.println(); //AN: Ok
    }


    //AN: задаче решена. Проблемы: дублирование кода и имена для переменных не всегда верно подобраны, это важно.

    //AP: Переделал. Подогнал максимально под условия задачи. В первом варианте действительно намудрил много

    //AN: Да, теперь все отлично.
}
