package net.javajoy.jps.homework.w01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Пользователь  должен  указать  с  клавиатуры  чётное  положительное  число,  а   программа  должна  создать  массив
 * указанного  размера  из  случайных  целых  чисел  из  [-5;5]  и  вывести  его  на  экран  в  строку.  После
 * этого  программа  должна  определить  и  сообщить  пользователю  о  том,  сумма  модулей  какой  половины
 * массива  больше:  левой  или  правой,  либо  сообщить,  что  эти  суммы  модулей  равны. Если  пользователь  введёт
 * неподходящее  число,  то  программа  должна  требовать повторного  ввода  до  тех  пор,  пока  не  будет  указано
 * корректное  значение. Ввод  с  клавиатуры  можно  заменить  передачей  параметра
 */

public class Task11 {
    private static final int MIN = -5;
    private static final int MAX = 5;

    public static void main(String[] args) throws IOException {
        int[] array = new int[getUserArraySize()]; //AP: Заменил

        createArray(array);
        printArray(array);
        getBigAbsPart(array);
    }

    /**
     * Возвращает цело положительное четное число введенное с клавиатуры
     *
     * @return целое положительное четное число
     */

    public static int getUserArraySize() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num;

        while (true) {
            try {
                num = Integer.parseInt(reader.readLine()); //AN: если вводим букву, то программа завершается
                if (num % 2 == 0 && num > 0) break;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                System.out.println("Введен символ вместо цифры");
            }
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return num;
    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходный массив
     */

    public static void createArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * (MAX - MIN + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }

    /**
     * Сравнивает модули сторон массива
     *
     * @param array исходный массив
     */

    public static void getBigAbsPart(int[] array) {
        int modLeft = 0;
        int modRight = 0;

        for (int i = 0; i < array.length; i++) {
            if (i < array.length / 2) modLeft += Math.abs(array[i]);
            else modRight += Math.abs(array[i]);
        }

        System.out.println(modLeft > modRight ? "Модуль левой половины массива больше модуля правой половины массива" :
                modLeft < modRight ? "Модуль правой половины массива больше модуля левой половины массива" : "Модули " +
                        "обеих половин массива равны");
    }

}


