package net.javajoy.jps.homework.w01;

/**
 * Создайте  два  массива  из  10  целых  случайных  чисел из отрезка  [1;9]  и  третий
 * массив  из  10  действительных  чисел  (с  плавающей  точкой).  Каждый  элемент  с  i - ым
 * индексом  третьего  массива  должен  равняться  отношению  элемента  из  первого массива  с  i - ым
 * индексом  к  элементу  из  второго  массива  с  i -ым  индексом.  Вывести все  три  массива  на
 * экран  (каждый  на  отдельной  строке),  затем  вывести  количество целых  элементов  в  третьем  массиве.
 */

public class Task9 {
    private static final int SIZE = 10;
    private static final int MIN = 1;
    private static final int MAX = 9;


    public static void main(String[] args) {
        int[] arrayOne = new int[SIZE]; //AP: Изменил на camelStyle
        int[] arrayTwo = new int[SIZE];
        double[] array_3;

        createArray(arrayOne);
        createArray(arrayTwo);
        array_3 = getRealArray(arrayOne, arrayTwo);

        printIntArray(arrayOne);
        printIntArray(arrayTwo);
        printDoubleArray(array_3);
        printIntElement(array_3);
    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходный массив
     */

    public static void createArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * (MAX - MIN + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printIntArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printDoubleArray(double[] array) {
        for (double element : array) {
            System.out.printf("%.3f ", element);
        }
        System.out.println();
    }

    /**
     * Возвращает массив, где каждый i - й элемент
     * равен отношению i - го элемента первого массива к
     * i - му элементу второго массива
     *
     * @param array_1 исходный массив 1
     * @param array_2 исходный массив 2
     * @return вещественный массив
     */

    public static double[] getRealArray(int[] array_1, int[] array_2) {
        int size = array_1.length;

        double[] array = new double[size];
        for (int i = 0; i < size; i++) {
            array[i] = (double) array_1[i] / (double) array_2[i];
        }

        return array;
    }

    /**
     * Находит целочисленные элементы в вещественном массиве
     *
     * @param array исходный массив
     */

    public static void printIntElement(double[] array) {
        int k = 0;
        for (double element : array) {
            if ((int) element - element == 0) {
                System.out.println((int) element + " целочисленный элемент");
                k++;
            }
        }

        System.out.println("Количество целочисленных элементов: " + k);
    }
}





















