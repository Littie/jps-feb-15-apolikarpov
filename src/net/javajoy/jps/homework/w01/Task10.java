package net.javajoy.jps.homework.w01;

/**
 * Создайте  массив  из  11  случайных  целых  чисел  из  отрезка  [-1;1],  выведите  массив
 * на  экран  в  строку.  Определите  какой  элемент  встречается в массиве  чаще  всего  и
 * выведите  об  этом  сообщение  на  экран.  Если  два  каких - то  элемента  встречаются
 * одинаковое  количество  раз,  то  не  выводите ничего
 */

public class Task10 {
    private static final int SIZE = 11;
    private static final int MIN = -1;
    private static final int MAX = 1;

    public static void main(String[] args) {
        int[] array = new int[SIZE];

        createArray(array);
        printArray(array);
        detectMaxElements(array);
    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходный массив
     */

    public static void createArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * (MAX - MIN + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }

    /**
     * Выводит количество чаще всего встречающегося элемента массива.
     * Если таких элементов несколько, ничего не выводится
     *
     * @param array - исходный массив
     */

    public static void detectMaxElements(int[] array) {
        int[] elements = new int[3];

        for (int element : array) {
            if (element == -1) elements[0]++;
            else if (element == 0) elements[1]++;
            else elements[2]++;
        }

        int flag;

        flag =
                elements[0] > elements[1] && elements[0] > elements[2] ? -1 :
                        elements[1] > elements[0] && elements[1] > elements[2] ? 0 :
                                elements[2] > elements[0] && elements[2] > elements[1] ? 1 : 10;

        if (flag != 10)
            System.out.println("Число " + flag + " встречается больше всего раз");
    }


}














