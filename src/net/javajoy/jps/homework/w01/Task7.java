package net.javajoy.jps.homework.w01;

/**
 * Создайте  массив  из  20 - ти  первых  чисел  Фибоначчи  и  выведите  его  на экран
 */

public class Task7 {
    private static final int SIZE = 20;

    public static void main(String[] args) {
        int[] array = new int[SIZE];
        addFibonachi(array);
        printArray(array);
    }

    /**
     * Заполняет массив длиной SIZE числами фибоначи
     *
     * @param array исходный массив
     */

    public static void addFibonachi(int[] array) {
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < array.length; i++) {
            array[i] = array[i - 1] + array[i - 2];
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
    }
}
