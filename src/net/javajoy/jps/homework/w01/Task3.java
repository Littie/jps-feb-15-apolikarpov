package net.javajoy.jps.homework.w01;

/**
 * Создайте  массив  из  15  случайных  целых  чисел  из  отрезка  [0;9].  Выведите  массив  на
 * экран.  Подсчитайте  сколько  в  массиве  чётных  элементов  и  выведете  это
 * количество  на  экран  на  отдельной
 */
//AN: это задание лучше получилось чем Task2
public class Task3 {
    private static final int SIZE = 15;

    public static void main(String[] args) {
        int[] array = new int[SIZE];

        fillArray(array);
        printArray(array);
        countEvenElement(array);
    }

    /**
     * Заполняет массив целыми числами случайным образом
     *
     * @param array исходный массив
     */
    public static void fillArray(int[] array) { //AP: Изменил название на fill
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */
    public static void printArray(int[] array) {
        for (int element : array) {
            System.out.print(element + " ");
        }
    }

    /**
     * Выводит количество четных элементов массива
     *
     * @param array исходный массив
     */
    public static void countEvenElement(int[] array) {
        int sum = 0;
        for (int element : array) {
            if (element % 2 == 0) {
                sum++;
            }
        }

        System.out.println("\nСумма четных элементов равна " + sum);
    }
}
