package net.javajoy.jps.homework.w01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Пользователь  вводит  с  клавиатуры  натуральное  число  большее  3,  которое сохраняется  в  переменную  n.
 * Если  пользователь  ввёл  не  подходящее  число,  то программа  должна  просить  пользователя  повторить  ввод.
 * Создать  массив  из n случайных  целых  чисел  из  отрезка  [0;n]  и  вывести  его  на  экран.  Создать  второй
 * массив  только  из  чётных  элементов  первого  массива,  если  они  там есть,  и  вывести  его  на  экран.
 */

//AN: в целом ты большой молодец. Очень хороший старт
public class Task13 {
    private static final int N; //AN: переменные лучше инициализировать через блок инициализации

    static {
        N = enterNum(); //AN: а лучше в теле метода это делать.
    }

    private static final int MIN = 0;
    private static final int MAX = N;

    public static void main(String[] args) {
        int[] array = new int[N];
        int[] evenNum;

        createArray(array);
        printArray(array);
        evenNum = createEvenNumArray(array);
        printArray(evenNum);
    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходный массив
     */

    public static void createArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * (MAX - MIN + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }


    /**
     * Возвращает целое положительное число больше 3
     * введенное с клавиатуры
     *
     * @return число введенное с клавиатуры
     */

    public static int enterNum() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num;

        while (true) {
            try {
                System.out.println("Введите натуральное число большее 3");
                num = Integer.parseInt(reader.readLine());
                if (num > 3) return num;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //AN: нет закрытия потока
    }

    /**
     * Возвращает количество элементов в массиве,
     * переданного в качестве параметра
     *
     * @param array исходный массив
     * @return количество четных элементов
     */

    public static int getCountEvenElementOfArray(int[] array) {
        int count = 0;

        for (int element : array)
            if (element % 2 == 0) count++;

        return count;
    }

    /**
     * Создает массив из четных элементов массива,
     * переданного в качестве параметра
     *
     * @param array исходный массив
     * @return массив четных элементов
     */

    public static int[] createEvenNumArray(int[] array) {
        int[] evens = new int[getCountEvenElementOfArray(array)];
        int k = 0;

        for (int element : array) {
            if (element % 2 == 0) {
                evens[k] = element;
                k++;
            }
        }
        return evens;
    }
}
