package net.javajoy.jps.homework.w01;

/**
 * Программа  должна  создать  массив  из  12  случайных  целых  чисел  из  отрезка  [-10;10]
 * таким  образом,  чтобы  отрицательных  и  положительных  элементов  там  было поровну  и  не  было  нулей.
 * При  этом  порядок  следования  элементов  должен  быть случаен  (т.  е.  не  подходит  вариант,  когда  в  массиве
 * постоянно  выпадает  сначала  6 положительных,  а  потом  6  отрицательных  чисел  или  же  когда  элементы  постоянно
 * чередуются  через  один  и  пр.).  Вывести  полученный  массив
 */

public class Task12 {
    private static final int SIZE = 12;
    private static final int MIN = -10;
    private static final int MAX = 10;

    public static void main(String[] args) {
        int[] array = new int[SIZE];
        createArray(array);
        printArray(array);
    }

    /**
     * Заполняет массив таким образом, что количество положительных
     * и отрицательных элементов равно
     *
     * @param array исходный массив
     */

    public static void createArray(int[] array) {
        int[] count = new int[2];
        int rand;

        for (int i = 0; i < array.length; i++) {
            while (true) {
                rand = MIN + (int) (Math.random() * (MAX - MIN + 1));

                if (count[0] >= SIZE / 2 && rand > 0 || rand == 0) continue;
                if (count[1] >= SIZE && rand < 0) continue;

                if (rand > 0) {
                    array[i] = rand;
                    count[0]++;
                }

                array[i] = rand; //AP: Вынес
                count[1]++;

                break;
            }
        }
        System.out.println("Выход из метода");
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }
}
