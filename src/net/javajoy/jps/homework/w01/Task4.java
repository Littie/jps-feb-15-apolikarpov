package net.javajoy.jps.homework.w01;

/**
 * Создайте  массив  из  8  случайных  целых  чисел  из  отрезка  [1;10].  Выведите  массив  на
 * экран  в  строку.  Замените  каждый  элемент  с  нечётным  индексом  на  ноль.  Снова
 * выведете  массив  на  экран  на  отдельной  ст
 */

public class Task4 {
    private static final int SIZE = 8;
    private static final int MIN = 1;
    private static final int MAX = 10;

    public static void main(String[] args) {
        int[] array = new int[SIZE];
        createArray(array);
        printArray(array);
        replaceElement(array);
        printArray(array);
    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходныый массив
     */
    public static void createArray(int[] array) {
        //Random rnd = new Random(); //AN:
        //rnd.nextInt(10); //AN: снизу все верно. Для твоей инф-ции, вот что еще можно использовать для random в границах

        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * ((MAX - MIN) + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходныый массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }

    /**
     * Заменяет нечетные элементы массива нулями
     *
     * @param array исходныый массив
     */
    public static void replaceElement(int[] array) { //AN: replaceElements
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) array[i] = 0;
        }
    }
}
