package net.javajoy.jps.homework.w01;

/**
 * Создайте  2  массива  из  5  случайных  целых  чисел  из  отрезка  [0;5]  каждый,  выведите
 * массивы  на   экран  в  двух  отдельных  строках.  Посчитайте  среднее  арифметическое
 * элементов  каждого  массива  и  сообщите,  для  какого  из  массивов  это  значение
 * оказалось  больше  (либо  сообщите,  что  их  средние  арифметические  равно
 */

public class Task5 {
    private static final int SIZE = 5;
    private static final int MIN = 0;
    private static final int MAX = 5;

    public static void main(String[] args) {
        int[] arrayOne = new int[SIZE]; //AP: Изменил на camelStyle
        int[] arrayTwo = new int[SIZE];

        fellArray(arrayOne);
        fellArray(arrayTwo);

        printArray(arrayOne);
        printArray(arrayTwo);

        compareAverageOfArrays(arrayOne, arrayTwo);

    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходныый массив
     */
    public static void fellArray(int[] array) { //AP: Изменил название на fill
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * ((MAX - MIN) + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходныый массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }

    /**
     * Находит среднее арефмитическое значений элементов массива
     *
     * @param array исходныый массив
     * @return среднее арефмитическое
     */

    public static double getAverageValue(int[] array) {
        int sum = 0;

        for (int element : array)
            sum += element;

        return (double) (sum / array.length); //AP: Запомнить!!!
    }


    /**
     * Находит большее среднее арефметическое двух заданных массивов
     *
     * @param array_1 исходный массив 1
     * @param array_2 исходный массив 2
     */

    public static void compareAverageOfArrays(int[] array_1, int[] array_2) {
        double av_1 = getAverageValue(array_1);
        double av_2 = getAverageValue(array_2);

        if (av_1 > av_2) System.out.println("Среднее арефметическое первого массива больше чем у второго");
        else if (av_2 > av_1) System.out.println("Среднее арефметическое второго массива больше чем у первого");
        else System.out.println("Среднее арефметическое первого массива равно среднему арефметическому второго");
    }

}
