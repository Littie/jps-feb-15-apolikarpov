package net.javajoy.jps.homework.w01;

/**
 * Создайте  массив  из  4  случайных  целых  чисел  из отрезка  [10;99],  выведите  его  на
 * экран  в  строку.  Определить  и  вывести  на  экран  сообщение  о  том,  является  ли
 * массив  строго  возрастающей  последовательностью
 */

public class Task6 {
    private static final int SIZE = 4;
    private static final int MIN = 10;
    private static final int MAX = 99;

    public static void main(String[] args) {
        int[] array = new int[SIZE];

        fillArray(array);
        printArray(array);
        checkIncreasingSequence(array);
    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходныый массив
     */

    public static void fillArray(int[] array) { //AP: изменил название на fill
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * ((MAX - MIN) + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходныый массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }

    /**
     * Проверяет, является ли массив строго возрастающей последовательностю
     *
     * @param array исходныый массив
     */

    public static void checkIncreasingSequence(int[] array) {
        boolean isSequence = true; //AP: Изменил имя и немного логику

        /*
         * Сортировка методом вставки. При данном методе сортировки,
         * количество итераций в отсортированном массиве равна нулю.
         */
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j > 0 && (array[j] < array[j - 1]); j--) {
                isSequence = false;
            }
        }

        if (isSequence) System.out.println("Массив является строго возрастающей последовательностью");
        else System.out.println("Массив не является строго возрастающей последовательностью");

    }
}
