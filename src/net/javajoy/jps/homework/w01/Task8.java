package net.javajoy.jps.homework.w01;

/**
 * Создайте  массив  из  12  случайных  целых  чисел  из  отрезка  [-20;20].  Определите
 * какой  элемент  является  в  этом  массиве  максимальным  и  сообщите  индекс  его
 * последнего  вхождения  в  массив
 */

public class Task8 {
    private static final int SIZE = 12;
    private static final int MIN = -20;
    private static final int MAX = 20;

    public static void main(String[] args) {
        int[] array = new int[SIZE];

        createArray(array);
        printArray(array);
        System.out.println("Индекс последнего вхождения максимального элемнта " + getIndexOfMaxElement(array));

    }

    /**
     * Заполняет массив целыми числами случайным образом
     * в диапазоне от MIN до MAX
     *
     * @param array исходный массив
     */

    public static void createArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * (MAX - MIN + 1));
        }
    }

    /**
     * Печатает массив переданный в качестве параметра
     *
     * @param array исходный массив
     */

    public static void printArray(int[] array) {
        for (int element : array)
            System.out.print(element + " ");
        System.out.println();
    }

    /**
     * Возвращает индекс последнего вхождения максимального элемента массива
     *
     * @param array исходный массив
     */

    public static int getIndexOfMaxElement(int[] array) { //AP: Исправил, теперь возвращает int
        int max = Integer.MIN_VALUE; //AN: можно просто MIN - 1
        int k = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= max) {
                max = array[i];
                k = i;
            }
        }

        //AP: Сделал возврат значения, а печать вынес в Main метод
        return k;
    }

}















