package net.javajoy.jps.homework.w08;

import java.util.Scanner;

/**
 * Main class
 */
public class Main {
    public static final String PRINT_ALL_COLORS = "All";

    public static void main(String[] args) {
        run();
    }

    /**
     * Main method for testing
     */
    public static void run() {
        Colors colors = new Colors();
        Scanner inputData = new Scanner(System.in);

        while (true) {
            menu();

            String num = inputData.nextLine();
            switch (num) {
                case "0":
                    System.exit(0);
                    break;
                case "1":
                    addColor(colors);
                    break;
                case "2":
                    deleteColor(colors);
                    break;
                case "3":
                    searchColor(colors);
                    break;
                default:
                    System.out.println("Incorrect input data. Please try again");
                    break;
            }
        }
    }

    /**
     * Search color input from console
     *
     * @param colors color name
     */
    private static void searchColor(Colors colors) {
        String colorName = getColorName();

        if (colors.getRGBColor(colorName) != null) {
            System.out.println(colors.getRGBColor(colorName));
        } else if (colorName.equals(PRINT_ALL_COLORS)) {
            System.out.println(colors.getAllColor());
        } else {
            System.out.println("The color is missing. Add it");
            addColor(colors);
        }
    }

    /**
     * Delete color input from console
     *
     * @param colors color name
     */
    private static void deleteColor(Colors colors) {
        String colorName = getColorName();

        if (!colors.delete(colorName)) {
            System.out.println("The color is missing");
        } else {
            System.out.println(colorName + " successfully deleted");
        }
    }

    /**
     * Add color input from console
     *
     * @param colors color name
     */
    private static void addColor(Colors colors) {
        Scanner inputData = new Scanner(System.in);

        String colorName = getColorName();

        System.out.println("Enter RGB in XXX XXX XXX format: ");

        while (inputData.hasNext()) {
            String colorRGB = inputData.nextLine();
            if (checkRGB(colorRGB)) {
                String[] arr = colorRGB.split(" ");
                colors.add(colorName, Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));
                break;
            } else {
                System.out.println("Incorrect input data. Try again");
            }
        }
    }

    /**
     * Return name pf color
     *
     * @return color name
     */
    private static String getColorName() {
        Scanner inputData = new Scanner(System.in);

        System.out.println("Enter color: ");
        return inputData.nextLine();
    }


    /**
     * Check RGB component input from console
     *
     * @param inputString RGB component
     * @return true if input string match RGB components, else return false
     */
    private static boolean checkRGB(String inputString) {
        String pattern = "^([0-9]|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]) " +
                "([0-9]|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]) " +
                "([0-9]|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])$";
        return inputString.matches(pattern);
    }

    /**
     * Output menu console
     */
    public static void menu() {
        System.out.println("\n1. Add color");
        System.out.println("2. Delete color");
        System.out.println("3. Search color - type 'All' to see full list of colors");
        System.out.println("0. Exit");
    }
}