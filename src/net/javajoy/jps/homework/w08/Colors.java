package net.javajoy.jps.homework.w08;

import java.awt.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class colors contains container with pairs key - color, value - RGB
 */
public class Colors {
    private Map<String, Color> colors;

    /**
     * Initial constructor
     */
    public Colors() {
        colors = new TreeMap<>();
    }

    /**
     * Add color in container.
     *
     * @param name color name
     * @param R    0 - 255
     * @param G    0 - 255
     * @param B    0 - 255
     */
    public void add(String name, int R, int G, int B) {
        colors.put(name, new Color(R, G, B));
    }

    /**
     * Delete color from container
     *
     * @param name color name
     * @return true if delete ok or false id delete failed
     */
    public boolean delete(String name) {
        return colors.remove(name) != null;
    }

    /**
     * Print all color from container
     */
    public String getAllColor() {
        String resultString = "";
        for (String color : colors.keySet()) {
            resultString += getRGBColor(color) + "\n";
        }

        if (resultString.isEmpty()) resultString = "There are no colors currently";
        return resultString;
    }

    /**
     * Get RGB component of color
     *
     * @param color color name
     * @return RGB component in String type
     */
    public String getRGBColor(String color) {
        if (colors.containsKey(color)) {
            return "Color: " + color + " [R = " + colors.get(color).getRed() +
                    " G = " + colors.get(color).getGreen() +
                    " B = " + colors.get(color).getBlue() + "]";
        } else {
            return null;
        }
    }

    public int getSize() {
        return colors.size();
    }
}