package net.javajoy.jps.homework.w04;

/**
 * Class extends Matrix class
 *
 * @author Artem Polikarpov
 */

public final class ImmutableMatrix extends Matrix {
    /**
     * Default constructor
     */
    public ImmutableMatrix() {
        super();
    }

    /**
     * Constructor with one param.
     *
     * @param matrix dimensional array of int type
     */
    public ImmutableMatrix(int[][] matrix) {
        super(matrix);
    }

    /**
     * Method adds two matrix.
     * Result is new object of ImmutableMatrix type
     *
     * @param matrix object of ImmutableMatrix type
     * @return new object of IMatrix type if size of matrix is correct
     *         null if size of matrix is incorrect
     */
    @Override
    public IMatrix addition(IMatrix matrix) {
        // Check size of matrix. Allowed add matrix of the same size
        if (CheckMatrix.isAdditionNotAllowed(this, matrix)) {
            System.out.println("Incorrect size of matrix");
            return null;
        }

        return new ImmutableMatrix(OperationMatrix.operationAddition(this, matrix));
    }

    /**
     * Method multiplies two matrix
     *
     * @param matrix dimensional array of int type
     * @return new object of ImmutableMatrix type
     */
    @Override
    public IMatrix multiplication(IMatrix matrix) {
        // Check size of matrix. Allowed multiplication matrix where count rows of matrix A equal count columns of matrix B
        if (CheckMatrix.isMultiplicationNotAllowed(this, matrix)) {
            System.out.println("When multiplying the matrix, the number of columns in the matrix A must be equal to the " +
                    "number of rows in the matrix B");

            return null;
        }

        return new ImmutableMatrix(OperationMatrix.operationMultiplication(this, matrix));
    }

    /**
     * Method return dimensional array
     *
     * @return new dimensional array of int type
     */
    @Override
    public int[][] getMatrix() {
        int[][] matrix = super.getMatrix();
        int[][] result = new int[matrix.length][];

        // Copy dimensional array
        for (int i = 0; i < matrix.length; i++) {
            result[i] = matrix[i].clone();
        }

        return result;
    }
}
