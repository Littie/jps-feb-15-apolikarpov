package net.javajoy.jps.homework.w04;

/**
 * Provides methods for working with matrix
 *
 * @author Artem Polikarpov
 */
public interface IMatrix {

    /**
     * Adding matrix.
     *
     * @param imatrix object which has a field which is dimensional array
     * @return object which has a field which is dimensional array
     */
    IMatrix addition(IMatrix imatrix);

    /**
     * Multiplication matrix
     *
     * @param imatrix object which has a field which is dimensional array
     * @return object which has a field which is dimensional array
     */
    IMatrix multiplication(IMatrix imatrix);

    int[][] getMatrix();
}
