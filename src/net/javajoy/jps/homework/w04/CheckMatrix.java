package net.javajoy.jps.homework.w04;

/**
 * Check matrix for arithmetic operations
 *
 * @author Artem Polikarpov
 */

public class CheckMatrix {
    /**
     * Method checks the condition matrix addition
     *
     * @param firstIMatrix  instance of IMatrix
     * @param secondIMatrix instance of IMatrix
     * @return true - if the matrix can not be added
     * false - if the matrix can be added
     */
    public static boolean isAdditionNotAllowed(IMatrix firstIMatrix, IMatrix secondIMatrix) {
        int[][] firstMatrix = firstIMatrix.getMatrix();
        int[][] secondMatrix = secondIMatrix.getMatrix();

        return firstMatrix.length != secondMatrix.length || firstMatrix[0].length != secondMatrix[0].length;
    }

    /**
     * Method checks the condition matrix multiplication
     *
     * @param firstIMatrix instance of IMatrix
     * @param secondIMatrix instance of IMatrix
     * @return true - if the matrix can not be multiplied
     *         false - if the matrix can be multiplied
     */
    public static boolean isMultiplicationNotAllowed(IMatrix firstIMatrix, IMatrix secondIMatrix) {
        int[][] firstMatrix = firstIMatrix.getMatrix();
        int[][] secondMatrix = secondIMatrix.getMatrix();

        return firstMatrix[0].length != secondMatrix.length;
    }
}
