package net.javajoy.jps.homework.w04;

/**
 * Contents arithmetic operations with matrix
 *
 * @author Artem Polikarpov
 */

public class OperationMatrix {
    /**
     * Operation of addition matrix
     *
     * @param firstIMatrix  instance of IMatrix
     * @param secondIMatrix instance of IMatrix
     * @return result of addition matrix
     */
    public static int[][] operationAddition(IMatrix firstIMatrix, IMatrix secondIMatrix) {
        int[][] firstMatrix = firstIMatrix.getMatrix();
        int[][] secondMatrix = secondIMatrix.getMatrix();

        int[][] resultingMatrix = new int[secondMatrix.length][secondMatrix[0].length];

        for (int row = 0; row < secondMatrix.length; row++) {
            for (int column = 0; column < secondMatrix[0].length; column++) {
                resultingMatrix[row][column] = firstMatrix[row][column] + secondMatrix[row][column];
            }
        }

        return resultingMatrix;
    }

    /**
     * Operation of multiplication matrix
     *
     * @param firstIMatrix  instance of IMatrix
     * @param secondIMatrix instance of IMatrix
     * @return result of multiplication matrix
     */
    public static int[][] operationMultiplication(IMatrix firstIMatrix, IMatrix secondIMatrix) {
        int[][] firstMatrix = firstIMatrix.getMatrix();
        int[][] secondMatrix = secondIMatrix.getMatrix();
        int[][] resultingMatrix = new int[firstMatrix.length][secondMatrix[0].length];
        int sum;

        for (int row = 0; row < firstMatrix.length; row++) {
            for (int column = 0; column < secondMatrix[0].length; column++) {
                sum = 0;
                for (int i = 0; i < secondMatrix.length; i++) {
                    sum += firstMatrix[row][i] * secondMatrix[i][column];
                }
                resultingMatrix[row][column] = sum;
            }
        }

        return resultingMatrix;
    }
}
