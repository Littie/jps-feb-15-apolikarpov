package net.javajoy.jps.homework.w04;

import java.util.Scanner;

/**
 * Class Matrix implements IMatrix interface.
 * <p>
 *
 * @author Artem Polikarpov
 */
public class Matrix implements IMatrix {
    private int[][] matrix;

    /**
     * Default constructor. Fill matrix with console
     */
    public Matrix() {
        this.matrix = fillMatrixWithConsole(enterSizeMatrix());
    }

    /**
     * Constructor with one param.
     *
     * @param matrix matrix of Matrix type
     */
    public Matrix(int[][] matrix) {
        this.matrix = matrix;
    }

    /**
     * Method adds two matrix.
     * Method change this matrix
     *
     * @param matrix object of Matrix type
     * @return this object if size of matrix is correct
     * null if size of matrix is incorrect
     */
    @Override
    public IMatrix addition(IMatrix matrix) {
        // Check size of matrix. Allowed add matrix of the same size
        if (CheckMatrix.isAdditionNotAllowed(this, matrix)) {
            System.out.println("Incorrect size of matrix");
            return null;
        }

        this.matrix = OperationMatrix.operationAddition(this, matrix);

        return this;
    }

    /**
     * Method multiplies two matrix.
     * Method change this matrix
     *
     * @param matrix dimensional array of int type
     * @return this object
     */
    @Override
    public IMatrix multiplication(IMatrix matrix) {
        // Check size of matrix. Allowed multiplication matrix where count rows of matrix A equal count columns of matrix B
        if (CheckMatrix.isMultiplicationNotAllowed(this, matrix)) {
            System.out.println("When multiplying the matrix, the number of columns in the matrix A must be equal to the " +
                    "number of rows in the matrix B");

            return null;
        }

        this.matrix = OperationMatrix.operationMultiplication(this, matrix);

        return this;
    }

    /**
     * Get matrix field
     *
     * @return dimensional array of int type
     */
    public int[][] getMatrix() {
        return this.matrix;
    }

    /**
     * Return specified element of matrix. if the element of the matrix beyond, return -1
     *
     * @param row    number of row
     * @param column number of column
     * @return value of element
     */
    public int getElement(int row, int column) {
        if (row < 0 || row >= this.matrix.length || column < 0 || column >= this.matrix[0].length) {
            return -1;
        } else {
            return this.matrix[row][column];
        }
    }

    /**
     * Print matrix
     */
    public void printMatrix() {
        for (int[] matrix : this.matrix) {
            for (int j = 0; j < this.matrix[0].length; j++) {
                System.out.print(matrix[j] + " ");
            }
            System.out.println("");
        }
    }

    /**
     * Enter and check size of matrix
     * Matrix size in the range from 2 to 100
     *
     * @return dimensional array of int type
     */
    private int[][] enterSizeMatrix() {
        Scanner in = new Scanner(System.in);
        int[][] matrix = new int[0][];

        System.out.println("Enter size in MxN format. Range from 2 to 100");

        /*
         * Enter and check matrix size
         */
        while (in.hasNext()) {
            String readString = in.nextLine();
            if (checkInputData(readString)) {
                String[] arr = readString.split("x");
                matrix = new int[Integer.parseInt(arr[0])][Integer.parseInt(arr[1])];
                break;
            } else {
                System.out.println("Incorrect input format, please try again");
            }
        }

        return matrix;
    }

    /**
     * Fill dimensional array with console
     *
     * @param matrix dimensional array of int type
     * @return filled dimensional array of int type
     */
    private int[][] fillMatrixWithConsole(int[][] matrix) {
        Scanner in = new Scanner(System.in);
        int count = 0;

        /*
         * Enter matrix's elements, check format of matrix and check input data
         */
        System.out.println("Fill matrix. Input elements through the gap. New line after pressed enter key");

        while (count != matrix.length) {
            String readString = in.nextLine();

            if (checkInputArrayRowData(readString, matrix[0].length)) {
                String[] arr = readString.split(" ");

                for (int j = 0; j < matrix[0].length; j++) {
                    matrix[count][j] = Integer.parseInt(arr[j]);
                }

                count++;
            } else {
                System.out.println("Incorrect input format, please try again");
                count = 0;
            }

        }

        return matrix;
    }

    /*
     * Check input data. Input Data is size of matrix
     */
    private static boolean checkInputData(String inputData) {
        String pattern = "^([2-9]|[1-9]\\d|100)x([1-9]|[1-9]\\d|100)$";
        return inputData.matches(pattern);
    }

    /*
     * Check input data. Input data are row's elements of matrix
     */
    private static boolean checkInputArrayRowData(String inputData, int length) {
        String pattern = "([0-9]|[1-9]\\d|100)";
        String result = "";

        for (int i = 0; i < length; i++) {
            result += pattern + " ";
        }

        return inputData.matches(result.trim());
    }
}
