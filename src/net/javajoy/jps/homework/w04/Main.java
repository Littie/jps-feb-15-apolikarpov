package net.javajoy.jps.homework.w04;

/**
 * Main class. Testing methods addition(), multiplication() and getMatrix()
 * of Matrix and ImmutableMatrix class
 */

public class Main {

    /**
     * Start point
     *
     * @param args parameters
     */
    public static void main(String[] args) {
        new Main();
    }


    public Main() {
        testMatrix();
        testImmutableMatrix();
        testGetMethod();
    }

    public static void testMatrix() {
        System.out.println("\n\n[[[ DEMO MATRIX ]]]");
        Matrix matrixOne;
        Matrix matrixTwo;

        // Fill and print matrix matrixOne
        System.out.println("Enter matrix matrixOne");
        matrixOne = new Matrix();
        System.out.println("Matrix matrixOne:");
        matrixOne.printMatrix();

        // Fill and print matrix matrixTwo
        System.out.println("Enter matrix matrixTwo");
        matrixTwo = new Matrix();
        System.out.println("Matrix matrixTwo:");
        matrixTwo.printMatrix();

        // Adding matrixOne and matrixTwo
        matrixOne = (Matrix) matrixOne.addition(matrixTwo);
        System.out.println("Sum of matrix matrixOne и matrixTwo: ");
        matrixOne.printMatrix();

        // Multiplication matrixOne and matrixTwo
        matrixOne = (Matrix) matrixOne.multiplication(matrixTwo);
        System.out.println("Multiplication of matrix matrixOne и matrixTwo: ");
        matrixOne.printMatrix();
    }

    public static void testImmutableMatrix() {
        System.out.println("\n\n[[[ DEMO IMMUTABLE MATRIX ]]]");
        ImmutableMatrix immutableMatrixOne;
        ImmutableMatrix immutableMatrixTwo;
        ImmutableMatrix immutableMatrixThree;

        // Fill and print matrix ImmutableMatrixOne
        System.out.println("Enter matrix immutableMatrixOne");
        immutableMatrixOne = new ImmutableMatrix();
        System.out.println("Matrix ImmutableMatrixOne:");
        immutableMatrixOne.printMatrix();

        // Fill and print matrix ImmutableMatrixTwo
        System.out.println("Enter matrix immutableMatrixTwo");
        immutableMatrixTwo = new ImmutableMatrix();
        System.out.println("Matrix ImmutableMatrixTwo:");
        immutableMatrixTwo.printMatrix();

        // Adding ImmutableMatrixOne and ImmutableMatrixTwo
        immutableMatrixThree = (ImmutableMatrix) immutableMatrixOne.addition(immutableMatrixTwo);
        System.out.println("Sum of matrix ImmutableMatrixOne и ImmutableMatrixTwo: ");
        immutableMatrixThree.printMatrix();

        // Multiplication ImmutableMatrixOne and ImmutableMatrixTwo
        immutableMatrixThree = (ImmutableMatrix) immutableMatrixOne.multiplication(immutableMatrixTwo);
        System.out.println("Multiplication of matrix matrixOne и matrixTwo: ");
        immutableMatrixThree.printMatrix();
    }

    public static void testGetMethod() {
        int[][] innerMatrix;

        System.out.println("Enter matrix immutableMatrix");
        ImmutableMatrix immutableMatrix = new ImmutableMatrix();

        // Get inner matrix
        innerMatrix = immutableMatrix.getMatrix();

        // Change element
        System.out.println("Change element [2][2] of immutableMatrix to 100");
        innerMatrix[2][2] = 100;

        // Print result matrix
        System.out.println("Matrix after change:");
        immutableMatrix.printMatrix();
    }
}
