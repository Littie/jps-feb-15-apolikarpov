package net.javajoy.jps.homework.w03;

import net.javajoy.jps.homework.w02.Time;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        // Создаем по одному экземпляру класса родителя и потомка
        Time firstTime = new Time(10, 0, 30);
        Time secondTime = new SmartTime(23, 15, 20);

        // Демонстрируем работу методов toString
        System.out.println("Метод toString для класса Time: " + firstTime);
        System.out.println("Метод toString для класса SmartTime: " + secondTime);

        // Тестируем метод getTimeOfDay. Определяем по заданному времени - часть дня
        TimeOfDay timeOfDayFirst = SmartTime.getTimeOfDay(firstTime);
        System.out.println("Текущее время: " + firstTime + " - это " + timeOfDayFirst.getTimeOfDay());
        TimeOfDay timeOfDaySecond = SmartTime.getTimeOfDay(secondTime);
        System.out.println("Текущее время: " + secondTime + " - это " + timeOfDaySecond.getTimeOfDay());

        // Клонируем объект secondTime класса Time
        Time cloneTime = firstTime.clone();
        System.out.println("Склонированое время: " + cloneTime);

        // Проверяем клон на идентичность
        if (firstTime.equals(cloneTime)) {
            System.out.println("Время firstTime и cloneTime идентичны");
        } else {
            System.out.println("Время firstTime и cloneTime не идентичны");
        }

    }
}
