package net.javajoy.jps.homework.w03;

/**
 * Задача:
 * <p>
 * В перечислении («утро», «день», «вечер», «ночь») сохраните время начала и конца каждого периода.
 * Реализуйте в этом перечислении конструктор и методы для соответствующих полей.
 * <p>
 * Пояснение:
 * <p>
 * Для удобства вычисления, время начало и конца периода указывается в секундах
 */
public enum TimeOfDay {

    NIGHT(0, 18000, "ночь"),            // Определяем ночь с 00:00:00 до 05:00:00
    MORNING(18001, 36000, "утро"),      // Определяем утро с 05:00:01 до 10:00:00
    DAY(36001, 57600, "день"),          // Определяем день с 10:00:01 до 16:00:00
    EVENING(57601, 86400, "вечер");     // Определяем вечер с 16:00:01 до 24:00:00

    private final int startTime;
    private final int endTime;
    private final String timeOfDay;

    /*
     * Constructor
     */
    TimeOfDay(int startTime, int endTime, String timeOfDay) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.timeOfDay = timeOfDay;
    }

    /*
     * Getter of startTime field
     */
    public int getStartTime() {
        return startTime;
    }

    /*
     * Getter of endTime field
     */
    public int getEndTime() {
        return endTime;
    }

    /**
     * Return string representation of time of day
     *
     * @return time of day
     */
    public String getTimeOfDay() {
        return timeOfDay;
    }
}
