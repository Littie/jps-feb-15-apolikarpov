package net.javajoy.jps.homework.w13.model;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class Names {
    private static Names instance;
    private Set<String> names = new TreeSet<>();

    private Names() {
        fill();
    }

    public static Names getInstance() {
        if (instance == null) { //AN: этот if будет всегда true. Посмотри внимательно в тело if
            instance = new Names();    //AP: Недосмотрел. Поправил.
        }

        return instance;
    }

    private void fill() {
        Collections.addAll(names, getListOfNames());
    }

    private String[] getListOfNames() {
        return new String[]{
                "Андрей", "Артём", "Вася", "Виктор", "Виктория", "Вера", "Вова", "Гриша", "Гена", "Дима",
                "Даша", "Елена", "Евгений", "Жанна", "Злата", "Ирина", "Игорь", "Илона", "Иван", "Катя",
                "Костя", "Лиза", "Лола", "Маша", "Марина", "Максим", "Настя", "Николай", "Ольга", "Петя",
        };
    }

    public String getNameByFirstSymbols(String firstSymbols) {
        if (!firstSymbols.isEmpty()) {

            for (String name : names) {
                if (name.startsWith(firstSymbols)) {
                    return name;
                }
            }
        }

        return null;
    }
}
