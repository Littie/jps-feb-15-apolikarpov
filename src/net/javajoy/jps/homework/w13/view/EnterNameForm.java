package net.javajoy.jps.homework.w13.view;

import javax.swing.*;
import java.awt.*;

public class EnterNameForm extends JFrame {
    public static final int WIDTH = 250;
    public static final int HEIGHT = 150;

    private JLabel nameLabel = new JLabel("Enter name:");
    private JLabel helpLabel = new JLabel("Help:");
    private JTextField nameField = new JTextField();
    private JTextField helpField = new JTextField();

    public EnterNameForm() {
        init();
    }

    private void init() {
        addPanes();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(WIDTH, HEIGHT));
        setVisible(true);
    }

    private void addPanes() {
        setLayout(new GridLayout(4, 1));
        getContentPane().add(nameLabel);
        getContentPane().add(nameField);
        getContentPane().add(helpLabel);
        getContentPane().add(helpField);
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getHelpField() {
        return helpField;
    }

    //------------------------------------------------------------------------------------------------------------------
}
