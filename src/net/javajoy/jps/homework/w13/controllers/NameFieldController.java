package net.javajoy.jps.homework.w13.controllers;

import net.javajoy.jps.homework.w13.model.Names;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;

public class NameFieldController implements CaretListener {
    private Names names = Names.getInstance();
    private JTextField nameField;
    private JTextField helpField;

    public NameFieldController(JTextField nameField, JTextField helpField) {
        this.nameField = nameField;
        this.helpField = helpField;
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        String name = names.getNameByFirstSymbols(nameField.getText());

        if (name != null) {
            helpField.setText(name);
            nameField.setBackground(Color.GREEN);
        } else {
            nameField.setBackground(Color.RED);
        }
    }
}
