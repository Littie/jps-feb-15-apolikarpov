package net.javajoy.jps.homework.w13.controllers.key;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class InsertKey extends AbstractAction {
    private JTextField textField;

    public InsertKey(JTextField textField) {
        this.textField = textField;
        bind(textField);
    }

    private void bind(JTextField textField) {
        InputMap im = textField.getInputMap(JComponent.WHEN_FOCUSED);
        ActionMap am = textField.getActionMap();
        String InsertKey = "insert";
        im.put(KeyStroke.getKeyStroke("INSERT"), InsertKey);
        am.put(InsertKey, this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //AN: вижу, что нашел сам как работать с clipboard. Молодец!
        Transferable transfer = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
        if (transfer != null && transfer.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            try {
                String text = (String) transfer.getTransferData(DataFlavor.stringFlavor);
                textField.setText(text);
            } catch (UnsupportedFlavorException | IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
