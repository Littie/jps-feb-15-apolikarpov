package net.javajoy.jps.homework.w13.controllers.key;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ClearKey extends AbstractAction {
    private JTextField textField; //AN: в каждом Action, приводится component к JTextField. Тогда лучше сразу отдавать JTextField, ведь вся функциональность заточена под него
    //AP: думал над этим. Вспомнил, что ты советуюшь приводить к общему виду, сделал так, но это скорее уже крайность, мне JTextField больше нравится

    public ClearKey(JTextField textField) {
        this.textField = textField;
        bind(textField);
    }

    private void bind(JTextField textField) {
        InputMap im = textField.getInputMap(JComponent.WHEN_FOCUSED);
        ActionMap am = textField.getActionMap();
        String clearKey = "clear";
        im.put(KeyStroke.getKeyStroke("ESCAPE"), clearKey);
        am.put(clearKey, this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        textField.setText("");
    }
}
