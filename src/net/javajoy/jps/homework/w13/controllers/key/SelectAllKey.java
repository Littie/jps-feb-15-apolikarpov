package net.javajoy.jps.homework.w13.controllers.key;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class SelectAllKey extends AbstractAction {
    private JTextField textField;

    public SelectAllKey(JTextField textField) {
        this.textField = textField;
        bind(textField);
    }

    private void bind(JTextField textField) {
        InputMap im = textField.getInputMap(JComponent.WHEN_FOCUSED);
        ActionMap am = textField.getActionMap();
        String selectAllKey = "select all";
        im.put(KeyStroke.getKeyStroke("F3"), selectAllKey);
        am.put(selectAllKey, this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int startPosition = 0;
        int lastPosition = textField.getText().length();

        textField.setCaretPosition(startPosition);
        textField.moveCaretPosition(lastPosition);
    }
}
