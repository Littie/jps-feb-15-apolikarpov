package net.javajoy.jps.homework.w13.controllers.key;

import net.javajoy.jps.homework.w13.model.Names;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class CompleteKey extends AbstractAction {
    private JTextField textField;
    private Names model;

    public CompleteKey(JTextField textField, Names model) {
        this.textField = textField;
        this.model = model;
        bind(textField);
    }

    private void bind(JTextField textField) {
        InputMap im = textField.getInputMap(JComponent.WHEN_FOCUSED);
        ActionMap am = textField.getActionMap();
        String completeKey = "complete";
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.CTRL_MASK, true), completeKey);
        am.put(completeKey, this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String fullName = model.getNameByFirstSymbols(textField.getText());
        textField.setText(fullName);
    }

}
