package net.javajoy.jps.homework.w13.controllers;

import net.javajoy.jps.homework.w13.controllers.key.ClearKey;
import net.javajoy.jps.homework.w13.controllers.key.CompleteKey;
import net.javajoy.jps.homework.w13.controllers.key.InsertKey;
import net.javajoy.jps.homework.w13.controllers.key.SelectAllKey;
import net.javajoy.jps.homework.w13.model.Names;
import net.javajoy.jps.homework.w13.view.EnterNameForm;

import javax.swing.*;

public class MainController {
    private Names model;
    private JTextField nameField;
    private JTextField helpField;

    public MainController(EnterNameForm form, Names model) {
        this.nameField = form.getNameField();
        this.helpField = form.getHelpField();
        this.model = model;
        initControllers();
    }

    private void initControllers() {
        nameField.addCaretListener(new NameFieldController(nameField, helpField));
        nameField.setAction(new ClearKey(nameField));
        nameField.setAction(new CompleteKey(nameField, model));
        nameField.setAction(new InsertKey(nameField));
        nameField.setAction(new SelectAllKey(nameField));
    }
}
