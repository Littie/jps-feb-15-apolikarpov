package net.javajoy.jps.homework.w13.calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class DayOfKyivCity {
    public static final int YEAR;
    public static final int MONTH = 4;
    public static final int DAY = 31;

    static {
        YEAR = inputYear();
    }

    public static void main(String[] args) {
        GregorianCalendar calendar = new GregorianCalendar(YEAR, MONTH, DAY);

        if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
            calendar.set(Calendar.DAY_OF_MONTH, DAY - calendar.get(Calendar.DAY_OF_WEEK));

        System.out.printf("Day of Kyiv: %d may%n", calendar.get(Calendar.DAY_OF_MONTH));

    }

    private static int inputYear() {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter year in xxxx format: ");

        while (true) {
            String year = in.nextLine();

            if (checkInputData(year)) {
                return Integer.parseInt(year);
            } else {
                System.out.println("Incorrect input format");
            }
        }
    }

    private static boolean checkInputData(String formatString) {
        String pattern = "[0-9][0-9][0-9][0-9]";
        return formatString.matches(pattern);
    }
}
