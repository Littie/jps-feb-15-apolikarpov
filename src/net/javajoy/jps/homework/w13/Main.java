package net.javajoy.jps.homework.w13;

import net.javajoy.jps.homework.w13.controllers.MainController;
import net.javajoy.jps.homework.w13.model.Names;
import net.javajoy.jps.homework.w13.view.EnterNameForm;

//AN: превосходная работа! Очень все грамотно сделано. Пару моментов только подправить
public class Main {
    public static void main(String[] args) {
        new MainController(new EnterNameForm(), Names.getInstance());
    }
}
