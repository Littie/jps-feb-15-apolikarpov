package net.javajoy.jps.homework.w17.om;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class Good implements Serializable {
    private static final long serialVersionUID = -1936963839402900492L;

    private ImageIcon imageIcon;
    private String name;
    private String category;
    private float price;
    private Color color;
    private boolean available;

    public Good(ImageIcon imageIcon, String name, String category, float price, Color color, boolean available) {
        this.imageIcon = imageIcon;
        this.name = name;
        this.category = category;
        this.price = price;
        this.color = color;
        this.available = available;
    }
    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public ImageIcon getImageIcon() {
        return imageIcon;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public float getPrice() {
        return price;
    }

    public Color getColor() {
        return color;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setImageIcon(ImageIcon imageIcon) {
        this.imageIcon = imageIcon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    //------------------------------------------------------------------------------------------------------------------
}
