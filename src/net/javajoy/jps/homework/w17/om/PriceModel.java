package net.javajoy.jps.homework.w17.om;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class PriceModel implements Serializable {
    private static final long serialVersionUID = 1227046165144441419l;

    private static PriceModel instance;
    private List<Good> goods;
    private Set<String> categories;

    private PriceModel() {
        goods = new ArrayList<>();
        categories = new TreeSet<>();
        fillCategories();
    }

    public static PriceModel getInstance() {
        if (instance == null) {
            instance = new PriceModel();
        }

        return instance;
    }

    public void addGood(Good good) {
        goods.add(good);
    }

    public void removeGood(int index) {
        if (index >= 0 && index < goods.size()) {
            goods.remove(index);
        }
    }

    private void fillCategories() {
        categories.add("Category 1");
        categories.add("Category 2");
        categories.add("Category 3");
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public List<Good> getGoods() {
        return goods;
    }

    public static void setInstance(PriceModel model) {
        instance = model;
    }

    public void updateGoodAt(int index, Title title, Object data) {
        Good goodForUpdate = goods.get(index);

        switch (title) {
            case IMAGE:
                goodForUpdate.setImageIcon((ImageIcon) data);
                break;
            case NAME:
                goodForUpdate.setName((String) data);
                break;
            case CATEGORY:
                goodForUpdate.setCategory((String) data);
                break;
            case PRICE:
                goodForUpdate.setPrice((Float) data);
                break;
            case COLOR:
                goodForUpdate.setColor((Color) data);
                break;
            case AVAILABLE:
                goodForUpdate.setAvailable((Boolean) data);
                break;
        }
    }

    public String[] getCategoriesAsArray() {
        return categories.toArray(new String[categories.size()]);
    }

    //------------------------------------------------------------------------------------------------------------------
}
