package net.javajoy.jps.homework.w17.om;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PriceTableModel extends AbstractTableModel {
    private List<List<Object>> data;

    public PriceTableModel() {
        this.data = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return Title.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data.get(rowIndex).get(columnIndex);
    }

    @Override
    public String getColumnName(int column) {
        return Title.values()[column].getName();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return ImageIcon.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return Float.class;
            case 4:
                return Color.class;
            case 5:
                return Boolean.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex != 0;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        data.get(rowIndex).set(columnIndex, aValue);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public void insertRow(List<Object> row) {
        ImageIcon icon = (ImageIcon) row.get(0);
        String name = (String) row.get(1);
        String category = (String) row.get(2);
        Float price = (Float) row.get(3);
        Color color = (Color) row.get(4);
        Boolean available = (Boolean) row.get(5);

        data.add(row);
        PriceModel.getInstance().addGood(new Good(icon, name, category, price, color, available));

        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }

    public void removeRow(int selectedRow) {
        if (selectedRow < 0 || selectedRow > data.size() - 1) {
            return;
        }

        data.remove(selectedRow);
        PriceModel.getInstance().removeGood(selectedRow);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }

    public void setData(List<List<Object>> data) {
        this.data = data;
        fireTableChanged(new TableModelEvent(this));
    }
}
