package net.javajoy.jps.homework.w17.om;

public enum Title {

    IMAGE("Image"),
    NAME("Name"),
    CATEGORY("Category"),
    PRICE("Price"),
    COLOR("Color"),
    AVAILABLE("Available");

    private String name;

    Title(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
