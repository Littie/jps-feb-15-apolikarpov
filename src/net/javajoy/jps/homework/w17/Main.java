package net.javajoy.jps.homework.w17;

import net.javajoy.jps.homework.w17.controller.MainWindowController;
import net.javajoy.jps.homework.w17.view.MainWindow;

public class Main {
    public static void main(String[] args) {
        new MainWindowController(new MainWindow());
    }
}
