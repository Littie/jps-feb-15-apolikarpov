package net.javajoy.jps.homework.w17.view;

import net.javajoy.jps.homework.w17.om.PriceModel;
import net.javajoy.jps.homework.w17.om.PriceTableModel;
import net.javajoy.jps.homework.w17.om.Title;
import net.javajoy.jps.homework.w17.view.renderer.ColorCellRenderer;
import net.javajoy.jps.homework.w17.view.renderer.ImageCellRenderer;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    private static final int FIELD_WIDTH = 1000;
    private static final int FIELD_HEIGHT = 400;
    private static final int ROW_HEIGHT = 25;

    private JTable priceTable;
    private JButton addButton;
    private JButton deleteButton;
    private JButton savePriceButton;
    private JButton loadPriceButton;
    private JScrollPane scrollPane;

    public MainWindow() {
        init();
    }

    private void init() {
        setPreferredSize(new Dimension(FIELD_WIDTH, FIELD_HEIGHT));
        initComponents();
        addComponents();
        setResizable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        addButton = new JButton("Add");
        deleteButton = new JButton("Delete");
        savePriceButton = new JButton("Save Table");
        loadPriceButton = new JButton("Load Table");
        priceTable = initTable();
        setRenderer();
        setEditor();
        scrollPane = new JScrollPane(priceTable);
    }

    private JTable initTable() {
        JTable table = new JTable(new PriceTableModel());

        table.setRowHeight(ROW_HEIGHT);
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        return table;
    }

    private void setRenderer() {
        priceTable.getColumnModel().getColumn(Title.COLOR.ordinal()).setCellRenderer(new ColorCellRenderer());
        priceTable.getColumnModel().getColumn(Title.IMAGE.ordinal()).setCellRenderer(new ImageCellRenderer());
    }

    private void setEditor() {
        priceTable.getColumnModel().getColumn(Title.CATEGORY.ordinal()).setCellEditor(
                new DefaultCellEditor(new JComboBox<>(PriceModel.getInstance().getCategoriesAsArray())));
    }

    private void addComponents() {
        setLayout(new GridBagLayout());

        getContentPane().add(addButton,
                new GridBagConstraints(0, 0, 1, 1, 0.01, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(deleteButton,
                new GridBagConstraints(0, 1, 1, 1, 0.01, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(savePriceButton,
                new GridBagConstraints(0, 2, 1, 1, 0.01, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(loadPriceButton,
                new GridBagConstraints(0, 3, 1, 1, 0.01, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(scrollPane,
                new GridBagConstraints(1, 0, 1, 4, 0.99, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 0), 0, 0));
    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getDeleteButton() {
        return this.deleteButton;
    }

    public JButton getSavePriceButton() {
        return savePriceButton;
    }

    public JButton getLoadPriceButton() {
        return loadPriceButton;
    }

    public JTable getPriceTable() {
        return priceTable;
    }

    public PriceTableModel getTableModel() {
        return (PriceTableModel) priceTable.getModel();
    }

    //------------------------------------------------------------------------------------------------------------------
}
