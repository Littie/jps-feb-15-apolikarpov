package net.javajoy.jps.homework.w17.view;

import net.javajoy.jps.homework.w17.om.PriceModel;
import net.javajoy.jps.homework.w17.om.Title;

import javax.swing.*;
import java.awt.*;

import static java.awt.GridBagConstraints.*;

public class AddWindow extends JFrame {
    private static final int FIELD_WIDTH = 400;
    private static final int FIELD_HEIGHT = 250;

    private JLabel imageLabel;
    private JLabel nameLabel;
    private JLabel categoryLabel;
    private JLabel priceLabel;
    private JLabel colorLabel;
    private JLabel availableLabel;

    private JTextField imageField;
    private JTextField nameField;
    private JComboBox<String> category;
    private JTextField priceField;
    private JLabel setColorLabel;
    private JCheckBox available;

    private JButton okButton;
    private JButton cancelButton;
    private JButton setImageButton;
    private JButton setColorButton;

    private String[] categories = PriceModel.getInstance().getCategoriesAsArray();
    private String selectedCategory;
    private boolean isAvailable = false;

    private MainWindow mainWindow;

    public AddWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        init();
    }

    private void init() {
        setPreferredSize(new Dimension(FIELD_WIDTH, FIELD_HEIGHT));
        initComponents();
        addComponents();
        setResizable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        imageLabel = new JLabel(Title.IMAGE.getName());
        nameLabel = new JLabel(Title.NAME.getName());
        categoryLabel = new JLabel(Title.CATEGORY.getName());
        priceLabel = new JLabel(Title.PRICE.getName());
        colorLabel = new JLabel(Title.COLOR.getName());
        availableLabel = new JLabel(Title.AVAILABLE.getName());

        imageField = new JTextField();
        nameField = new JTextField();
        category = new JComboBox<>(categories);
        priceField = new JTextField("0");
        setColorLabel = new JLabel("");
        available = new JCheckBox("");

        okButton = new JButton("Ok");
        cancelButton = new JButton("Cancel");
        setImageButton = new JButton("...");
        setColorButton = new JButton("...");
    }

    private void addComponents() {
        setLayout(new GridBagLayout());

        getContentPane().add(imageLabel,
                new GridBagConstraints(0, 0, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(imageField,
                new GridBagConstraints(1, 0, 1, 1, 0.8, 1, WEST, HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(setImageButton,
                new GridBagConstraints(2, 0, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(nameLabel,
                new GridBagConstraints(0, 1, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(nameField,
                new GridBagConstraints(1, 1, 1, 1, 0.8, 1, WEST, HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(categoryLabel,
                new GridBagConstraints(0, 2, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(category,
                new GridBagConstraints(1, 2, 1, 1, 0.8, 1, WEST, HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(priceLabel,
                new GridBagConstraints(0, 3, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(priceField,
                new GridBagConstraints(1, 3, 1, 1, 0.8, 1, WEST, HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(colorLabel,
                new GridBagConstraints(0, 4, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(setColorLabel,
                new GridBagConstraints(1, 4, 1, 1, 0.8, 1, WEST, HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(setColorButton,
                new GridBagConstraints(2, 4, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(availableLabel,
                new GridBagConstraints(0, 5, 1, 1, 0.1, 1, WEST, NONE, new Insets(0, 5, 0, 0), 0, 0));
        getContentPane().add(available,
                new GridBagConstraints(1, 5, 1, 1, 0.8, 1, WEST, BOTH, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(okButton,
                new GridBagConstraints(0, 6, 1, 1, 1, 1, EAST, NONE, new Insets(0, 0, 0, 0), 0, 0));
        getContentPane().add(cancelButton,
                new GridBagConstraints(1, 6, 1, 1, 1, 1, CENTER, NONE, new Insets(0, 0, 0, 0), 0, 0));

    }

    //---------------------------------------- Getters and Setters -----------------------------------------------------

    public JComboBox<String> getCategory() {
        return category;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public JButton getSetImageButton() {
        return setImageButton;
    }

    public JTextField getImageField() {
        return imageField;
    }

    public void setImageFieldText(String text) {
        this.imageField.setText(text);
    }

    public JButton getSetColorButton() {
        return setColorButton;
    }

    public void setSetColorLabel(Color color) {
        this.setColorLabel.setOpaque(true);
        this.setColorLabel.setForeground(color);
        this.setColorLabel.setBackground(color);
        this.setColorLabel.setText(Integer.toString(color.getRGB()));
    }

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getPriceField() {
        return priceField;
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public JLabel getSetColorLabel() {
        return setColorLabel;
    }

    public JCheckBox getAvailable() {
        return available;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public MainWindow getMainWindow() {
        return mainWindow;
    }

    //------------------------------------------------------------------------------------------------------------------
}
