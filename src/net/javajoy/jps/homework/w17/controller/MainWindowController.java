package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.controller.action.window.main.AddButtonAction;
import net.javajoy.jps.homework.w17.controller.action.window.main.DeleteButtonAction;
import net.javajoy.jps.homework.w17.controller.action.window.main.LoadPriceButtonAction;
import net.javajoy.jps.homework.w17.controller.action.window.main.SavePriceButtonAction;
import net.javajoy.jps.homework.w17.view.MainWindow;

public class MainWindowController {
    private MainWindow mainWindow;


    public MainWindowController(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        initControllers();
    }

    private void initControllers() {
        mainWindow.getAddButton().addActionListener(new AddButtonAction(mainWindow));
        mainWindow.getDeleteButton().addActionListener(new DeleteButtonAction(mainWindow));
        mainWindow.getSavePriceButton().addActionListener(new SavePriceButtonAction());
        mainWindow.getLoadPriceButton().addActionListener(new LoadPriceButtonAction(mainWindow));
        mainWindow.getPriceTable().getModel().addTableModelListener(new PriceModelListener());
    }
}
