package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.controller.action.window.add.*;
import net.javajoy.jps.homework.w17.view.AddWindow;

public class AddController {
    private AddWindow addWindow;

    public AddController(AddWindow addWindow) {
        this.addWindow = addWindow;
        initControllers();
    }

    private void initControllers() {
        addWindow.getCancelButton().addActionListener(new CancelButtonAction(addWindow));
        addWindow.getSetImageButton().addActionListener(new SetImageButtonAction(addWindow));
        addWindow.getSetColorButton().addActionListener(new SetColorButtonAction(addWindow));
        addWindow.getCategory().addActionListener(new CategoryAction(addWindow));
        addWindow.getAvailable().addItemListener(new AvailableAction(addWindow));
        addWindow.getOkButton().addActionListener(new OkButtonAction(addWindow, addWindow.getMainWindow()));
    }
}
