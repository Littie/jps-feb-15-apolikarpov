package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.om.PriceModel;
import net.javajoy.jps.homework.w17.om.PriceTableModel;
import net.javajoy.jps.homework.w17.om.Title;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class PriceModelListener implements TableModelListener {
    @Override
    public void tableChanged(TableModelEvent e) {
        int row = e.getFirstRow();
        int col = e.getColumn();
        PriceTableModel model = (PriceTableModel) e.getSource();

        //AP: Все, понял.
        if (col == TableModelEvent.ALL_COLUMNS) {
            return;
        }

        Title columnName = Title.valueOf(model.getColumnName(col).toUpperCase());
        Object newValue = model.getValueAt(row, col);
        PriceModel.getInstance().updateGoodAt(row, columnName, newValue);
    }
}
