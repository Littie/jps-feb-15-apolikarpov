package net.javajoy.jps.homework.w17.controller.action.window.add;

import net.javajoy.jps.homework.w17.view.AddWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SetColorButtonAction implements ActionListener {
    private AddWindow addWindow;

    public SetColorButtonAction(AddWindow addWindow) {
        this.addWindow = addWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        addWindow.setSetColorLabel(JColorChooser.showDialog(addWindow, "Select color", Color.BLACK));
    }
}
