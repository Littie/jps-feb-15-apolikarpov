package net.javajoy.jps.homework.w17.controller.action.window.add;

import net.javajoy.jps.homework.w17.view.AddWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CancelButtonAction implements ActionListener {
    private AddWindow addWindow;

    public CancelButtonAction(AddWindow addWindow) {
        this.addWindow = addWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        addWindow.dispose();
    }
}
