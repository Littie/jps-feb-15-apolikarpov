package net.javajoy.jps.homework.w17.controller.action.window.main;

import net.javajoy.jps.homework.w17.dao.FileSystemDao;
import net.javajoy.jps.homework.w17.om.Good;
import net.javajoy.jps.homework.w17.om.PriceModel;
import net.javajoy.jps.homework.w17.view.MainWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoadPriceButtonAction implements ActionListener {
    private static final String OPEN_FILE_CHOOSER_PATH = "resources/w17/";

    private MainWindow mainWindow;
    private FileSystemDao fileSystemDao;
    private PriceModel priceModel;

    public LoadPriceButtonAction(MainWindow mainWindow) {
        this.fileSystemDao = new FileSystemDao();
        this.mainWindow = mainWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser openFileChooser = new JFileChooser(OPEN_FILE_CHOOSER_PATH);
        int result = openFileChooser.showDialog(null, "Open");

        if (result == JFileChooser.APPROVE_OPTION) {
            File fileName = openFileChooser.getSelectedFile();

            try {
                PriceModel.setInstance(fileSystemDao.loadData(fileName));
                this.priceModel = PriceModel.getInstance();
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(mainWindow, "Error open file");
                e1.printStackTrace();
            }

            mainWindow.getTableModel().setData(update(priceModel.getGoods()));

        }
    }

    private List<List<Object>> update(List<Good> goods) {
        List<List<Object>> data = new ArrayList<>();

        for (Good good : goods) {
            List<Object> row = new ArrayList<>();

            row.add(good.getImageIcon());
            row.add(good.getName());
            row.add(good.getCategory());
            row.add(good.getPrice());
            row.add(good.getColor());
            row.add(good.isAvailable());

            data.add(row);
        }

        return data;
    }
}
