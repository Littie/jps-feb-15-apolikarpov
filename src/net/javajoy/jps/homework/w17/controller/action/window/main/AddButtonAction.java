package net.javajoy.jps.homework.w17.controller.action.window.main;

import net.javajoy.jps.homework.w17.controller.AddController;
import net.javajoy.jps.homework.w17.view.AddWindow;
import net.javajoy.jps.homework.w17.view.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddButtonAction implements ActionListener {
    private MainWindow mainWindow;

    public AddButtonAction(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new AddController(new AddWindow(mainWindow));
    }
}
