package net.javajoy.jps.homework.w17.controller.action.window.main;

import net.javajoy.jps.homework.w17.om.PriceTableModel;
import net.javajoy.jps.homework.w17.view.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeleteButtonAction implements ActionListener {
    private MainWindow mainWindow;

    public DeleteButtonAction(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int selectedRow = mainWindow.getPriceTable().getSelectedRow();

        ((PriceTableModel) mainWindow.getPriceTable().getModel()).removeRow(selectedRow);
    }
}
