package net.javajoy.jps.homework.w17.controller.action.window.main;

import net.javajoy.jps.homework.w17.dao.FileSystemDao;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class SavePriceButtonAction implements ActionListener {
    private static final String DEFAULT_OPEN_PATH = "resources/w17/";
    private static final String EXTENSION = ".dat";

    private FileSystemDao setting;

    public SavePriceButtonAction() {
        setting = new FileSystemDao();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        File file = new File(DEFAULT_OPEN_PATH);

        if (!file.exists()) {
            file.mkdirs();
        }

        JFileChooser saveChooser = new JFileChooser(DEFAULT_OPEN_PATH);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(EXTENSION + " files", "dat");
        saveChooser.setFileFilter(filter);
        saveChooser.setDialogTitle("Save as");
        saveChooser.setDialogType(JFileChooser.SAVE_DIALOG);

        int result = saveChooser.showDialog(null, "Save");
        if (result == JFileChooser.APPROVE_OPTION) {
            File fileName = saveChooser.getSelectedFile();
            setting.saveAs(fileName);
        }
    }
}
