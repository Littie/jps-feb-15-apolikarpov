package net.javajoy.jps.homework.w17.controller.action.window.add;

import net.javajoy.jps.homework.w17.view.AddWindow;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class AvailableAction implements ItemListener {
    private AddWindow addWindow;

    public AvailableAction(AddWindow addWindow) {
        this.addWindow = addWindow;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            addWindow.setAvailable(true);
        } else {
            addWindow.setAvailable(false);
        }
    }
}
