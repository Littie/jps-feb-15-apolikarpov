package net.javajoy.jps.homework.w17.controller.action.window.add;

import net.javajoy.jps.homework.w17.view.AddWindow;
import net.javajoy.jps.homework.w17.view.MainWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OkButtonAction implements ActionListener {
    private AddWindow addWindow;
    private MainWindow mainWindow;

    public OkButtonAction(AddWindow addWindow, MainWindow mainWindow) {
        this.addWindow = addWindow;
        this.mainWindow = mainWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<Object> data = new ArrayList<>();
        data.add(getImageIcon());
        data.add(addWindow.getNameField().getText());
        data.add(addWindow.getSelectedCategory());
        data.add(getPrice());
        data.add(getColor());
        data.add(addWindow.isAvailable());

        mainWindow.getTableModel().insertRow(data);

        addWindow.dispose();
    }

    private ImageIcon getImageIcon() {
        ImageIcon imageIcon = new ImageIcon();
        String path = addWindow.getImageField().getText();

        if ((new File(path)).exists() && (path.endsWith(".png") || path.endsWith(".jpg"))) {
            imageIcon = new ImageIcon(addWindow.getImageField().getText());
        } else {
            return imageIcon;
        }

        return imageIcon;
    }

    private Color getColor() {
        Color color = Color.BLACK;
        if (!addWindow.getSetColorLabel().getText().equals("")) {
            color = new Color(Integer.parseInt(addWindow.getSetColorLabel().getText()));
        }
        return color;
    }

    private float getPrice() {
        float price = 0;
        if (!addWindow.getPriceField().getText().equals("")) {
            price = Float.parseFloat(addWindow.getPriceField().getText());
        }
        return price;
    }
}
