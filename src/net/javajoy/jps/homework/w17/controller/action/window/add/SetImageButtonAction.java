package net.javajoy.jps.homework.w17.controller.action.window.add;

import net.javajoy.jps.homework.w17.view.AddWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class SetImageButtonAction implements ActionListener {
    private AddWindow addWindow;

    public SetImageButtonAction(AddWindow addWindow) {
        this.addWindow = addWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser imageChooser = new JFileChooser();
        int returnResult = imageChooser.showDialog(addWindow, "Open");

        if (returnResult == JFileChooser.APPROVE_OPTION) {
            File file = imageChooser.getSelectedFile();
            addWindow.setImageFieldText(file.toString());
        }
    }
}
