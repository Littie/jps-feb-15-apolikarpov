package net.javajoy.jps.homework.w17.controller.action.window.add;

import net.javajoy.jps.homework.w17.view.AddWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CategoryAction implements ActionListener {
    private AddWindow addWindow;

    public CategoryAction(AddWindow addWindow) {
        this.addWindow = addWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox category = (JComboBox) e.getSource();
        addWindow.setSelectedCategory((String) category.getSelectedItem());
    }
}
