package net.javajoy.jps.homework.w17.dao;

import net.javajoy.jps.homework.w17.om.PriceModel;

import java.io.*;

public class FileSystemDao {

    public void saveAs(File fileName) {
        try (ObjectOutputStream outData = new ObjectOutputStream(new FileOutputStream(fileName))) {
            outData.writeObject(PriceModel.getInstance());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public PriceModel loadData(File fileName) throws IOException {
        Object model = null;

        try (ObjectInputStream inputData = new ObjectInputStream(new FileInputStream(fileName))) {
            model = inputData.readObject();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return model != null ? (PriceModel) model : null;
    }
}
